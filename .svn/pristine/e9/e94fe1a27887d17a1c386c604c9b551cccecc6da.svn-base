import {
    Component,
    OnInit,
    ViewChild
} from '@angular/core';
import {
    BreadcrumbService
} from '../layout/breadcrumb/breadcrumb.service';
import * as operators from '../../../operators.variables';
import {
    TranslateService
} from '@ngx-translate/core';
import {
    AdminLayoutComponent
} from '../layout/admin-layout/admin-layout.component';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import {
    UserService
} from '../../../service/user.service';
import {
    ToastrService
} from 'ngx-toastr';
import {
    DocumentService
} from '../../../service/document.service';
import {
    FileUploader
} from 'ng2-file-upload';
import {
    SafeHtml,
    DomSanitizer
} from '@angular/platform-browser';
import {
    AdministrationService
} from '../../../service/Administration.service';
import {
    SignatureComponent
} from '../../generic-components/signature-pad/signature-pad.component';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
    public languageOptions = [];
    public selectedLangauage = '';
    public isDelegate = true;
    addDelegateForm: FormGroup;
    userList: any[];
    public delegationList = [];
    public showfromfile = true;
    public showsignature = false;
    // public SignaturePad;
    public filevalue: any;
    public showpinui: any;
    public showsavebutton = false;
    public currentSignatureShow = true;
    public base64data;
    public signatureUrl: any;
    public responseAddOns = [];
    public signatureChanged = 'File';
    public selectedColor: any;
    public pdfUrl = 'assets/images/pdf-icon.png';
    public imageUrl = 'assets/images/png-icon.png';
    enclosures: FileUploader = new FileUploader({
        isHTML5: true,
        allowedFileType: ['doc', 'ppt', 'image', 'pdf']
        //  allowedMimeType: ['image/png']
    });
    public themeArray = [];
    public disablePast;
    public disablePastdateSelect;
    // public signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    //   'minWidth': 1,
    //   'canvasWidth': 600,
    //   'canvasHeight': 300
    //   };
    @ViewChild('signature') SignaturePad: SignatureComponent;
    public signUrl: string;
    public passwordform: FormGroup;
    public showColorChange = false;
    public showLanguageChange = false;
    public annotationList;
    public defaultAnnotation;
    public showAnnotationChange = false;
    public searchList;
    public selectedSearch;
    public showSearchChange = false;
    public dateList;
    public selecteDate;
    public showDateChange = false;
    public ddmmyy = true;
    public showDocumentViewChanged = false;
    public deafultDocumentView = 'annotations';
    public documentView = [];

    constructor(private breadcrumbService: BreadcrumbService,
        private translate: TranslateService, private app: AdminLayoutComponent,
        private fb: FormBuilder, public us: UserService, private as: AdministrationService,
        private tr: ToastrService, private ds: DocumentService, private _sanitizer: DomSanitizer) {
        if (localStorage.getItem('Date Format') === 'DD/MM/YYYY') {
            this.ddmmyy = true;
        } else {
            this.ddmmyy = false;
        }
        this.ddmmyy = true;
        this.breadcrumbService.setItems([{
            label: this.translate.instant('Settings')
        }]);
        this.disablePast = new Date();
        this.disablePastdateSelect = new Date();
        let array;
        array = [];
        for (const amt of operators.languageOptions) {
            array.push(amt.value);
        }
        setTimeout(() => {
            translate.addLangs(array);
        }, 0);

        this.annotationList = [{
            label: 'Comment',
            value: 'COMMENT'
        }, {
            label: 'Image',
            value: 'IMAGE'
        }];
        this.documentView = [{
            label: 'Annotation',
            value: 'annotations'
        }, {
            label: 'Properties',
            value: 'properties'
        }];
        this.searchList = [
            {
                label: 'Document',
                value: 'DOCUMENTS'
            }, {
                label: 'Worktype',
                value: 'WORKTYPES'
            }
        ];
        this.dateList = [
            {
                label: 'DD/MM/YYYY',
                value: 'DD/MM/YYYY'
            }, {
                label: 'MM/DD/YYYY',
                value: 'MM/DD/YYYY'
            }
        ];
    }

    ngOnInit() {
        this.themeArray = [];
        this.themeArray = [{
            label: 'Blue Grey - Green',
            value: 'bluegrey'
        }, {
            label: 'Indigo - Pink',
            value: 'indigo'
        }, {
            label: 'Pink - Amber',
            value: 'pink'
        }, {
            label: 'Purple - Pink',
            value: 'purple'
        }, {
            label: 'Deep Purple - Orange',
            value: 'deeppurple'
        }, {
            label: 'Light Blue - Blue Grey',
            value: 'lightblue'
        }, {
            label: 'Cyan - Amber',
            value: 'cyan'
        }];

        // (<HTMLInputElement>document.getElementById(elementId)).value;
        const theme = (<HTMLInputElement>document.getElementById('theme-css')).getAttribute('href');
        if (theme.includes('theme-bluegrey')) {
            this.selectedColor = 'bluegrey';
        } else if (theme.includes('theme-indigo')) {
            this.selectedColor = 'indigo';
        } else if (theme.includes('theme-pink')) {
            this.selectedColor = 'pink';
        } else if (theme.includes('theme-purple')) {
            this.selectedColor = 'purple';
        } else if (theme.includes('theme-deeppurple')) {
            this.selectedColor = 'deeppurple';
        } else if (theme.includes('theme-lightblue')) {
            this.selectedColor = 'lightblue';
        } else if (theme.includes('theme-cyan')) {
            this.selectedColor = 'cyan';
        }
        // this.translate.stream('operators.languageOptions').subscribe(val => {
        //   for
        // });
        this.addDelegateForm = this.fb.group({
            id: [''],
            delegateId: [''],
            delegateName: ['', Validators.required],
            fromDate: [null, Validators.required],
            toDate: [null, Validators.required],
            fromDateDisplay: [null],
            toDateDisplay: [null],
            status: new FormControl('ACTIVE')
        });
        this.languageOptions = operators.languageOptions;
        this.us.getUsers().subscribe(data => this.getUsers(data));
        this.us.getUserDelegations().subscribe(data => this.getDelegation(data));
        //   this.selectedLangauage = localStorage.getItem('Default Language');
        //   this.app.dirType = localStorage.getItem('direction');
        //   if ( this.selectedLangauage === 'ar') {
        //       this.app.isRTL = true;
        //   } else {
        //       this.app.isRTL = false;
        //   }
        //   const value = localStorage.getItem('Default Language');
        //   this.translate.use(value);
        if (this.us.getCurrentUser().signature !== undefined) {
            this.ds.downloadDocument(this.us.getCurrentUser().signature).subscribe(data => this.showSignatureMethod(data));
        }
        this.as.getAddOns().subscribe(data => this.addOnsRes(data), error => { });

        this.passwordform = this.fb.group({
            oldpassword: [null, Validators.compose([Validators.required])],
            newpassword: [null, Validators.compose([Validators.required])],
            confirmpassword: [null, Validators.compose([Validators.required])]
        });

        this.defaultAnnotation = localStorage.getItem('Default Annotation');
        this.selectedSearch = localStorage.getItem('Default Search');
        this.selecteDate = localStorage.getItem('Date Format');
        this.selectedLangauage = localStorage.getItem('Default Language');
        setTimeout(() => {
            this.deafultDocumentView = localStorage.getItem('Default Document View');
        }, 0);
    }
    addOnsRes(data) {
        this.responseAddOns = JSON.parse(data._body);
    }
    getUsers(data) {
        this.userList = [];
        let value = [];
        value = JSON.parse(data._body);
        for (let aks = 0; aks < value.length; aks++) {
            const amt = {
                label: value[aks].fulName,
                value: value[aks].EmpNo
            };
            this.userList.push(amt);
        }
    }
    getDelegation(data) {
        this.delegationList = JSON.parse(data._body);
    }
    delegationSubmit(delegation) {
        this.isDelegate = true;
        const del = {
            id: this.addDelegateForm.controls.id.value,
            delegateId: this.addDelegateForm.controls.delegateId.value,
            fromDate: this.addDelegateForm.controls.fromDate.value,
            toDate: this.addDelegateForm.controls.toDate.value,
            delegatedBy: this.us.getCurrentUser().EmpNo
        };
        this.us.saveDelegation(del).subscribe(data => {
            this.saveDeligationUser(data);
            this.tr.success('', this.translate.instant('Delegated successfully'));
        });
    }
    saveDeligationUser(data) {
        this.addDelegateForm.reset();
        this.addDelegateForm.patchValue({
            'delegateName': ''
        });
        this.delegationList = [];
        this.us.getUserDelegations().subscribe(datares => this.getDelegation(datares));
    }
    valueChanged(event) {
        this.addDelegateForm.patchValue({
            delegateId: event.value
        });
    }
    languageChanged(event) {
        this.selectedLangauage = event.value;
        if (this.selectedLangauage !== localStorage.getItem('Default Language')) {
            this.showLanguageChange = true;
        } else {
            this.showLanguageChange = false;
        }

    }

    setLanguageChange() {
        const setting = [{
            empNo: this.us.getCurrentUser().EmpNo,
            key: 'Default Language',
            val: this.selectedLangauage
        }];
        if (this.selectedLangauage === 'ar') {
            setting.push({
                empNo: this.us.getCurrentUser().EmpNo,
                key: 'direction',
                val: 'rtl'
            });
        } else {
            setting.push({
                empNo: this.us.getCurrentUser().EmpNo,
                key: 'direction',
                val: 'ltr'
            });
        }
        // if (this.selectedLangauage === 'ar') {
        //     this.app.isRTL = true;
        //     this.app.dirType = 'rtl';
        //     localStorage.setItem('direction', 'rtl');
        //     this.translate.use(localStorage.getItem('language'));
        //     this.selectedLangauage = localStorage.getItem('language');
        //     // window.location.reload();
        // } else {
        //     this.app.isRTL = false;
        //     this.app.dirType = 'ltr';
        //     localStorage.setItem('direction', 'ltr');
        //     this.translate.use(localStorage.getItem('language'));
        //     this.selectedLangauage = localStorage.getItem('language');
        //     // window.location.reload();
        // }
        this.us.updateUserSettings(setting).subscribe(data => this.getUserDeatil(data));
    }
    fromdatechanged(event) {
        const date = new Date(event),
            mnth = ('0' + (date.getMonth() + 1)).slice(-2),
            day = ('0' + date.getDate()).slice(-2);
        let frmdate;
        if (this.ddmmyy) {
            frmdate = [day, mnth, date.getFullYear()].join('/');
        } else {
            frmdate = [mnth, day, date.getFullYear()].join('/');
        }
        console.log(event);
        this.disablePastdateSelect = event;
        this.addDelegateForm.patchValue({
            fromDate: frmdate,
            toDate: ''
        });

    }
    toDateChanged(event) {
        const date = new Date(event),
            mnth = ('0' + (date.getMonth() + 1)).slice(-2),
            day = ('0' + date.getDate()).slice(-2);
        let tdate;
        if (this.ddmmyy) {
            tdate = [day, mnth, date.getFullYear()].join('/');
        } else {
            tdate = [mnth, day, date.getFullYear()].join('/');
        }
        this.addDelegateForm.patchValue({
            toDate: tdate
        });
    }
    editDeligation(details) {
        this.tr.info('', this.translate.instant('Edit the delegated user'));
        this.isDelegate = false;
        this.addDelegateForm.reset();
        let fromdateMonth = details.fromDate.split('/')[1];
        if (fromdateMonth.split('')[0] === '0') {
            fromdateMonth = fromdateMonth.replace('0', '');
        }
        let todateMonth = details.toDate.split('/')[1];
        if (todateMonth.split('')[0] === '0') {
            todateMonth = todateMonth.replace('0', '');
        }
        this.addDelegateForm.patchValue({
            id: details.id,
            delegateName: details.delegateId,
            delegateId: details.delegateId,
            toDate: details.toDate,
            fromDate: details.fromDate,
            fromDateDisplay: {
                date: {
                    year: details.fromDate.split('/')[2],
                    month: fromdateMonth,
                    day: details.fromDate.split('/')[0]
                }
            },
            toDateDisplay: {
                date: {
                    year: details.toDate.split('/')[2],
                    month: todateMonth,
                    day: details.toDate.split('/')[0]
                }
            },
        });
        //  this.myDatePickerOptionsto = {
        //   dateFormat: 'dd/mm/yyyy',
        //    disableUntil: { year: details.fromDate.split('/')[2] , month: fromdateMonth , day: details.fromDate.split('/')[0]}
        // };
    }
    revokedelegation(item) {
        this.us.revokeDelegation(item.id).subscribe(data => {
            this.tr.success('', this.translate.instant('Deleted successfully'));
            this.us.getUserDelegations().subscribe(resdata => this.getDelegation(resdata));
        });
    }
    optionforfile() {
        this.signatureChanged = 'File';
        this.showfromfile = true;
        this.showsignature = false;
        //  this.SignaturePad.clear();
    }
    fileupload(event) {
        // this.addDocument(this.enclosureAttachment, this.enclosures, 'ENCLOSURE');
        this.filevalue = [];
        const value = this.enclosures.queue;
        if (value.length > 1) {
            value.splice(0, 1);
        }
        for (let i = 0; i < value.length; i++) {
            if (value[i].file.type !== 'image/png') {
                value[i].remove();
                this.tr.error('', 'Attach Only PNG Images');
            } else {
                const docInfo = {
                    docclass: 'ProductivitiSignature',
                    props: [{
                        'name': 'Document Title',
                        'symName': 'DocumentTitle',
                        'dtype': 'STRING',
                        'mvalues': [value[i]._file.name],
                        'mtype': 'N',
                        'len': 255,
                        'rOnly': 'false',
                        'hidden': 'false',
                        'req': 'false'
                    }],
                    accessPolicies: []
                };
                const formData = new FormData();
                formData.append('DocInfo', JSON.stringify(docInfo));
                formData.append('file' + i, value[i]._file);
                // document.getElementById('spinner').style.display = 'block';
                this.ds.addDocument(formData).
                    subscribe(data => {
                        this.store(data);
                        this.showsavebutton = true;
                    });
            }
        }
    }
    store(data) {
        this.filevalue = data._body;
    }
    saveFileAttachments(data) {
        this.us.saveSignature(data).subscribe(resdata => {
            this.tr.success('', 'Signature Saved');
            this.showsavebutton = false;
            this.saveFromFile(this.filevalue);
        });
        this.showpinui = '';
        this.enclosures.queue = [];
        data = '';
    }
    saveFromFile(data) {
        const userObject = JSON.parse(localStorage.getItem('user'));
        userObject.signature = this.filevalue;
        localStorage.removeItem('user');
        localStorage.setItem('user', JSON.stringify(userObject));
        this.filevalue = '';
        this.signatureShow();
    }
    signatureShow() {
        //  this.currentSignatureShow = false;
        if (this.us.getCurrentUser().signature !== undefined) {
            this.ds.downloadDocument(this.us.getCurrentUser().signature).subscribe(data => this.showSignatureMethod(data));
            this.currentSignatureShow = true;
            this.showfromfile = true;
            this.showsignature = false;
        } else {
            this.currentSignatureShow = false;
            this.showfromfile = false;
            this.showsignature = false;
        }
    }
    showSignatureMethod(data) {
        let base64data;
        const reader = new FileReader();
        reader.readAsDataURL(data._body);
        reader.onloadend = function () {
            base64data = reader.result;
        };
        this.base64data = base64data;
        const objectURL = URL.createObjectURL(data._body);
        this.signatureUrl = this._sanitizer.bypassSecurityTrustResourceUrl(objectURL);
        this.signatureChanged = 'File';

    }
    generatepin() {
        this.us.generatePin().subscribe(data => this.showpin(data));
    }
    showpin(data) {
        this.showpinui = JSON.parse(data._body);
    }
    optionforsignature(value) {
        this.signatureChanged = 'Signature';
        this.showsavebutton = false;
        this.showsignature = true;
        this.showfromfile = false;
        this.enclosures.queue = [];
        //    var base64Image = new Buffer( this.signatureUrl, 'binary' ).toString('base64');
        if (this.SignaturePad !== undefined) {
            this.SignaturePad.fromDataURL(this.base64data);
        }
        // ;
    }
    drawComplete() {
        this.signUrl = this.SignaturePad.toDataURL();
    }
    drawStart() {
        // will be notified of szimek/signature_pad's onBegin event
    }
    savesignature() {
        this.signUrl = this.SignaturePad.toDataURL();
        const byteCharacters = atob(this.signUrl.replace('data:image/png;base64,', ''));
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const blob = new Blob([byteArray], {
            type: 'image/png'
        });
        let string: String;
        string = this.us.getCurrentUser().EmpNo + '-digitalSignature.png';
        const docInfo = {
            docclass: 'ProductivitiSignature',
            props: [{
                'name': 'Document Title',
                'symName': 'DocumentTitle',
                'dtype': 'STRING',
                'mvalues': [string],
                'mtype': 'N',
                'len': 255,
                'rOnly': 'false',
                'hidden': 'false',
                'req': 'false'
            }],
            accessPolicies: []
        };
        const formData = new FormData();
        formData.append('DocInfo', JSON.stringify(docInfo));
        // formData.append('file', scannedPdfName);
        formData.append('document', blob, this.us.getCurrentUser().EmpNo + '-digitalSignature.png');
        if (this.SignaturePad !== undefined) {
            this.ds.addDocument(formData).subscribe(data => {
                this.savefor(data);
                this.SignaturePad.fromDataURL(this.signUrl);
            });
        }
    }

    checkingOut(data) {
        this.signUrl = this.SignaturePad.toDataURL();
        const byteCharacters = atob(this.signUrl.replace('data:image/png;base64,', ''));
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const blob = new Blob([byteArray], {
            type: 'image/png'
        });
        // saveAs(blob, this.activityinfo.refNo);
        let string: String;
        string = this.us.getCurrentUser().EmpNo + '-digitalSignature.png';
        const docInfo = {
            id: data._body,
            docclass: 'ProductivitiSignature',
            props: [{
                'name': 'Document Title',
                'symName': 'DocumentTitle',
                'dtype': 'STRING',
                'mvalues': [string],
                'mtype': 'N',
                'len': 255,
                'rOnly': 'false',
                'hidden': 'false',
                'req': 'false'
            }],
            accessPolicies: []
        };
        const formData = new FormData();
        formData.append('DocInfo', JSON.stringify(docInfo));
        // formData.append('file', scannedPdfName);
        formData.append('document', blob, this.us.getCurrentUser().EmpNo + '-digitalSignature.png');
        if (this.SignaturePad !== undefined) {
            this.ds.checkIn(formData).subscribe(datares => {
                this.savefor(datares);
            });
        }
    }
    viewsignature(id) {
        this.ds.viewDocument(id).subscribe(res => res);
    }
    save(data) {
        if (data !== '') {
            this.us.saveSignature(data).subscribe(resdata => {
                this.tr.success('', 'Signature Saved');
                this.showsavebutton = false;
            });
            this.showpinui = '';
            this.filevalue = '';
            this.enclosures.queue = [];
            data = '';
            this.signatureShow();
        }
    }
    savefor(data) {
        const userObject = JSON.parse(localStorage.getItem('user'));
        userObject.signature = data._body;
        localStorage.removeItem('user');
        localStorage.setItem('user', JSON.stringify(userObject));
        this.save(data._body);
        this.filevalue = '';
    }
    addOnDownload(link) {
        window.open(link, '_blank');
    }
    colorChanged(event) {
        this.selectedColor = event.value;
        if (localStorage.getItem('Default Theme') !== this.selectedColor) {
            this.showColorChange = true;
        } else {
            this.showColorChange = false;
        }
    }

    setColorChange() {
        let setting;
        for (let index = 0; index < this.us.getCurrentUser().settings.length; index++) {
            if (this.us.getCurrentUser().settings[index].key === 'Default Theme') {
                setting = [{
                    empNo: this.us.getCurrentUser().EmpNo,
                    key: 'Default Theme',
                    val: this.selectedColor,
                    id: this.us.getCurrentUser().settings[index].id
                }];
                break;
            }
        }
        localStorage.setItem('Default Theme', this.selectedColor);
        this.us.updateUserSettings(setting).subscribe(data => this.getUserDeatil(data));
    }

    changedLayout(layout) {
        const layoutLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('layout-css');
        layoutLink.href = 'assets/layout/css/layout-' + layout + '.css';
    }

    passwordFormSubmit() {
        if (this.passwordform.controls.newpassword.value === this.passwordform.controls.confirmpassword.value) {
            const passwordChange = {
                empNo: this.us.getCurrentUser().EmpNo,
                oldPassword: btoa(this.passwordform.controls.oldpassword.value),
                password: btoa(this.passwordform.controls.newpassword.value)
            };
            this.us.changeUserPassword(passwordChange).subscribe(data => this.changeUserPassword(data));
        } else {
            this.tr.warning('', 'New password and confrim password should be same');
        }
    }

    changeUserPassword(data) {
        this.tr.success('', 'Password updated');
        this.passwordform.reset();
    }

    annotationChanged(event) {
        this.defaultAnnotation = event.value;
        if (localStorage.getItem('Default Annotation') !== this.defaultAnnotation) {
            this.showAnnotationChange = true;
        } else {
            this.showAnnotationChange = false;
        }
    }
    documentViewChanged(event) {
        this.deafultDocumentView = event.value;
        if (localStorage.getItem('Default Document View') !== this.deafultDocumentView) {
            this.showDocumentViewChanged = true;
        } else {
            this.showDocumentViewChanged = false;
        }
    }
    setAnnotationChange() {
        let setting;
        for (let index = 0; index < this.us.getCurrentUser().settings.length; index++) {
            if (this.us.getCurrentUser().settings[index].key === 'Default Annotation') {
                setting = [{
                    empNo: this.us.getCurrentUser().EmpNo,
                    key: 'Default Annotation',
                    val: this.defaultAnnotation,
                    id: this.us.getCurrentUser().settings[index].id
                }];
                break;
            } else {
                setting = [{
                    empNo: this.us.getCurrentUser().EmpNo,
                    key: 'Default Annotation',
                    val: this.defaultAnnotation
                }];
            }
        }
        localStorage.setItem('Default Annotation', this.defaultAnnotation);
        this.us.updateUserSettings(setting).subscribe(data => this.getUserDeatil(data));
    }
    setdocumentViewChanged() {
        let setting;
        for (let index = 0; index < this.us.getCurrentUser().settings.length; index++) {
            if (this.us.getCurrentUser().settings[index].key === 'Default Document View') {
                setting = [{
                    empNo: this.us.getCurrentUser().EmpNo,
                    key: 'Default Document View',
                    val: this.deafultDocumentView,
                    id: this.us.getCurrentUser().settings[index].id
                }];
                break;
            } else {
                setting = [{
                    empNo: this.us.getCurrentUser().EmpNo,
                    key: 'Default Document View',
                    val: this.deafultDocumentView,
                }];
            }
        }
        localStorage.removeItem('Default Document View');
        setTimeout(() => {
            localStorage.setItem('Default Document View', this.deafultDocumentView);
        }, 0);
        this.us.updateUserSettings(setting).subscribe(data => this.getUserDeatil(data));
    }

    searchChanged(event) {
        this.selectedSearch = event.value;
        if (localStorage.getItem('Default Search') !== this.selectedSearch) {
            this.showSearchChange = true;
        } else {
            this.showSearchChange = false;
        }
    }

    setSearchChange() {
        const setting = [{
            empNo: this.us.getCurrentUser().EmpNo,
            key: 'Default Search',
            val: this.selectedSearch
        }];
        this.us.updateUserSettings(setting).subscribe(data => this.getUserDeatil(data));
    }

    dateChanged(event) {
        this.selecteDate = event.value;
        if (localStorage.getItem('Date Format') !== this.selecteDate) {
            this.showDateChange = true;
        } else {
            this.showDateChange = false;
        }
    }

    setDateChange() {
        const setting = [{
            empNo: this.us.getCurrentUser().EmpNo,
            key: 'Date Format',
            val: this.selecteDate
        }];
        this.us.updateUserSettings(setting).subscribe(data => { this.getUserDeatil(data); });
    }

    getUserDeatil(data) {
        this.us.getUserDetails('12').subscribe(result => this.setUserDetails(result));
    }

    setUserDetails(data) {
        if (data._body !== '') {
            const user = JSON.parse(data._body);
            localStorage.setItem('user', JSON.stringify(user));
            if (user.settings !== undefined) {
                for (let index = 0; index < user.settings.length; index++) {
                    if (user.settings[index].key === 'Default Document View') {
                        localStorage.setItem(user.settings[index].key, user.settings[index].val);
                        break;
                    }
                }
                for (let other = 0; other < user.settings.length; other++) {
                    if (user.settings[other].key !== 'Default Document View') {
                        localStorage.setItem(user.settings[other].key, user.settings[other].val);
                    }
                }
            }
            setTimeout(() => {
                window.location.reload();
            }, 100);
        }
    }
    // fileChanegd(event) {
    //     console.log(event.target.files);
    // }
}
