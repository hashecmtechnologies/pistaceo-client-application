"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
require("rxjs/add/operator/map");
const logger_1 = require("./logger");
/**
 * Keep an map of indentation => array of indentations based on the level.
 * This is to optimize calculating the prefix based on the indentation itself. Since most logs
 * come from similar levels, and with similar indentation strings, this will be shared by all
 * loggers. Also, string concatenation is expensive so performing concats for every log entries
 * is expensive; this alleviates it.
 */
const indentationMap = {};
class IndentLogger extends logger_1.Logger {
    constructor(name, parent = null, indentation = '  ') {
        super(name, parent);
        indentationMap[indentation] = indentationMap[indentation] || [''];
        const map = indentationMap[indentation];
        this._observable = this._observable.map(entry => {
            const l = entry.path.length;
            if (l >= map.length) {
                let current = map[map.length - 1];
                while (l >= map.length) {
                    current += indentation;
                    map.push(current);
                }
            }
            entry.message = map[l] + entry.message.split(/\n/).join('\n' + map[l]);
            return entry;
        });
    }
}
exports.IndentLogger = IndentLogger;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZW50LmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9oYW5zbC9Tb3VyY2VzL2hhbnNsL2RldmtpdC8iLCJzb3VyY2VzIjpbInBhY2thZ2VzL2FuZ3VsYXJfZGV2a2l0L2NvcmUvc3JjL2xvZ2dlci9pbmRlbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7O0dBTUc7QUFDSCxpQ0FBK0I7QUFDL0IscUNBQWtDO0FBR2xDOzs7Ozs7R0FNRztBQUNILE1BQU0sY0FBYyxHQUEwQyxFQUFFLENBQUM7QUFHakUsa0JBQTBCLFNBQVEsZUFBTTtJQUN0QyxZQUFZLElBQVksRUFBRSxTQUF3QixJQUFJLEVBQUUsV0FBVyxHQUFHLElBQUk7UUFDeEUsS0FBSyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztRQUVwQixjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsY0FBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDbEUsTUFBTSxHQUFHLEdBQUcsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRXhDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDOUMsTUFBTSxDQUFDLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDNUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNwQixJQUFJLE9BQU8sR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO29CQUN2QixPQUFPLElBQUksV0FBVyxDQUFDO29CQUN2QixHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNwQixDQUFDO1lBQ0gsQ0FBQztZQUVELEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFdkUsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNmLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztDQUNGO0FBdEJELG9DQXNCQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBHb29nbGUgSW5jLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICpcbiAqIFVzZSBvZiB0aGlzIHNvdXJjZSBjb2RlIGlzIGdvdmVybmVkIGJ5IGFuIE1JVC1zdHlsZSBsaWNlbnNlIHRoYXQgY2FuIGJlXG4gKiBmb3VuZCBpbiB0aGUgTElDRU5TRSBmaWxlIGF0IGh0dHBzOi8vYW5ndWxhci5pby9saWNlbnNlXG4gKi9cbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvbWFwJztcbmltcG9ydCB7IExvZ2dlciB9IGZyb20gJy4vbG9nZ2VyJztcblxuXG4vKipcbiAqIEtlZXAgYW4gbWFwIG9mIGluZGVudGF0aW9uID0+IGFycmF5IG9mIGluZGVudGF0aW9ucyBiYXNlZCBvbiB0aGUgbGV2ZWwuXG4gKiBUaGlzIGlzIHRvIG9wdGltaXplIGNhbGN1bGF0aW5nIHRoZSBwcmVmaXggYmFzZWQgb24gdGhlIGluZGVudGF0aW9uIGl0c2VsZi4gU2luY2UgbW9zdCBsb2dzXG4gKiBjb21lIGZyb20gc2ltaWxhciBsZXZlbHMsIGFuZCB3aXRoIHNpbWlsYXIgaW5kZW50YXRpb24gc3RyaW5ncywgdGhpcyB3aWxsIGJlIHNoYXJlZCBieSBhbGxcbiAqIGxvZ2dlcnMuIEFsc28sIHN0cmluZyBjb25jYXRlbmF0aW9uIGlzIGV4cGVuc2l2ZSBzbyBwZXJmb3JtaW5nIGNvbmNhdHMgZm9yIGV2ZXJ5IGxvZyBlbnRyaWVzXG4gKiBpcyBleHBlbnNpdmU7IHRoaXMgYWxsZXZpYXRlcyBpdC5cbiAqL1xuY29uc3QgaW5kZW50YXRpb25NYXA6IHtbaW5kZW50YXRpb25UeXBlOiBzdHJpbmddOiBzdHJpbmdbXX0gPSB7fTtcblxuXG5leHBvcnQgY2xhc3MgSW5kZW50TG9nZ2VyIGV4dGVuZHMgTG9nZ2VyIHtcbiAgY29uc3RydWN0b3IobmFtZTogc3RyaW5nLCBwYXJlbnQ6IExvZ2dlciB8IG51bGwgPSBudWxsLCBpbmRlbnRhdGlvbiA9ICcgICcpIHtcbiAgICBzdXBlcihuYW1lLCBwYXJlbnQpO1xuXG4gICAgaW5kZW50YXRpb25NYXBbaW5kZW50YXRpb25dID0gaW5kZW50YXRpb25NYXBbaW5kZW50YXRpb25dIHx8IFsnJ107XG4gICAgY29uc3QgbWFwID0gaW5kZW50YXRpb25NYXBbaW5kZW50YXRpb25dO1xuXG4gICAgdGhpcy5fb2JzZXJ2YWJsZSA9IHRoaXMuX29ic2VydmFibGUubWFwKGVudHJ5ID0+IHtcbiAgICAgIGNvbnN0IGwgPSBlbnRyeS5wYXRoLmxlbmd0aDtcbiAgICAgIGlmIChsID49IG1hcC5sZW5ndGgpIHtcbiAgICAgICAgbGV0IGN1cnJlbnQgPSBtYXBbbWFwLmxlbmd0aCAtIDFdO1xuICAgICAgICB3aGlsZSAobCA+PSBtYXAubGVuZ3RoKSB7XG4gICAgICAgICAgY3VycmVudCArPSBpbmRlbnRhdGlvbjtcbiAgICAgICAgICBtYXAucHVzaChjdXJyZW50KTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBlbnRyeS5tZXNzYWdlID0gbWFwW2xdICsgZW50cnkubWVzc2FnZS5zcGxpdCgvXFxuLykuam9pbignXFxuJyArIG1hcFtsXSk7XG5cbiAgICAgIHJldHVybiBlbnRyeTtcbiAgICB9KTtcbiAgfVxufVxuIl19