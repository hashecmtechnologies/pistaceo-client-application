import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { ContentService } from '../../../service/content.service';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { DocumentProperty } from '../../../models/documentproperty.model';
import { WorkService } from '../../../service/work.service';
import { Router, ActivatedRoute } from '@angular/router';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../service/user.service';
import { TranslateService } from '@ngx-translate/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { DocumentService } from '../../../service/document.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-doc-property',
  templateUrl: './doc-property.component.html',
  styleUrls: ['./doc-property.component.css']
})
export class DocPropertyComponent implements OnChanges  {

  @Input() documentId;
  @Input() type;
  @Output() refresh = new EventEmitter();
  @Output() documentAnnotate = new EventEmitter();
  @Output() docLinkEmitter = new EventEmitter();
  @Input() reloadComponent;
  @Output() showPrintIs = new EventEmitter();
  @Output() showPerticularAnnotation = new EventEmitter();
  public timestamp;
  public propertyEdit = false;
  public docProperty: DocumentProperty;
  public docPropertyprops;
  public docPropertyName;
  public docisReserved;
  public editProp: FormGroup;
  public folderFileName = [];
  public documentPermission;
  public listOfDocumentAnnotation = [];
  public versionsList = [];
  public sharedDownloadlink;
  public permissionModelEditForm: FormGroup;
  public permissionModelForm: FormGroup;
  public docId;
  public curPage;
  public getrolecategory = [];
  public getroleList;
  public setroleList;
  public permissionRoleSelected;
  public deletePermissionsDetail;
  public permissionModelDropdown = [];
  public listOfDcumentAnnotation = [];
  public attachmentfile;
  public folderPermissionModelDropdown = [];
  public checkOutProp: FormGroup;
  public checkOutdocPropertyprops = [];
  public attachmentfileName = null;
  public showDocumentHistory = [];
  public selectedFolderId = null;
  public userSelectedNode = null;
  public unFileDocument;
  public documentActionsList = [];
  public documentLinkList = [];
  public selectedUnlink;
  public changePage;
  public annotationCount = 0;
  public ddmmyy = true;
  constructor(private cs: ContentService, private fb: FormBuilder, private ws: WorkService, public ngxSmartModalService: NgxSmartModalService,
    private router: Router, private activatedRouter: ActivatedRoute, private tr: ToastrService,
    public us: UserService, private translate: TranslateService ) {
      if (localStorage.getItem('Date Format') === 'DD/MM/YYYY') {
        this.ddmmyy = true;
    }else {
        this.ddmmyy = false;
    }
    this.ddmmyy = true;
     }

  ngOnChanges() {
    this.sharedDownloadlink =  this.cs.downloadSharedDocument(this.documentId);
    this.cs.getDocumentActions(this.documentId).subscribe(data => this.getDocumentActions(data));
    this.cs.getDocument(this.documentId).subscribe(data => this.getPropertyDetail(data, this.documentId));
    this.editProp = this.fb.group({});
    this.checkOutProp  = this.fb.group({});
    this.permissionModelDropdown.push({label: this.translate.instant('Full Control (Delete)'), value: 127});
    this.permissionModelDropdown.push({label: this.translate.instant('Owner (Permissions)'), value: 63});
    this.permissionModelDropdown.push({label: this.translate.instant('Author (Versioning)'), value: 31});
    this.permissionModelDropdown.push({label: this.translate.instant('Author (Modify)'), value: 15});
    // this.permissionModelDropdown.push({label: this.translate.instant('Author (Download)'), value: 7});
    // this.permissionModelDropdown.push({label: this.translate.instant('Author (Annotate)'), value: 3});
    this.permissionModelDropdown.push({label: this.translate.instant('Viewer'), value: 1});

    this.folderPermissionModelDropdown.push({label: this.translate.instant('View'), value: 1});
    this.folderPermissionModelDropdown.push({label: this.translate.instant('Modify'), value: 3});
    this.folderPermissionModelDropdown.push({label: this.translate.instant('Create Subfolder'), value: 7});
    this.folderPermissionModelDropdown.push({label: this.translate.instant('File document'), value: 15});
    this.folderPermissionModelDropdown.push({label: this.translate.instant('Unfile document'), value: 31});
    this.folderPermissionModelDropdown.push({label: this.translate.instant('Owner(Permissions)'), value: 63});
    this.folderPermissionModelDropdown.push({label: this.translate.instant('Delete'), value: 127});
    this.permissionModelForm = this.fb.group({
      accessLevel: [null, Validators.compose([Validators.required])],
      granteeName: [null, Validators.compose([Validators.required])],
      permissionSource: [1, Validators.compose([Validators.required])],
      roleId: [null, Validators.compose([Validators.required])],
      accessMask:  [1, Validators.compose([Validators.required])],
    });

    this.permissionModelEditForm = this.fb.group({
      accessLevel: [null, Validators.compose([Validators.required])],
      granteeName: [null, Validators.compose([Validators.required])],
      permissionSource: [1, Validators.compose([Validators.required])],
      roleId: [null, Validators.compose([Validators.required])],
      accessMask:  [1, Validators.compose([Validators.required])],
      aclId: [0]
    });
    this.activatedRouter.queryParams.subscribe(parms => {
      this.selectedFolderId = parms['folderId'];
    });
    this.folderFileName = [];
  }

  checkOut() {
    this.cs.checkOut(this.documentId).subscribe(data => {this.checkingOut(this.documentId);  });
  }

  checkingOut(data) {
    this.tr.success('', 'Document Checked-out');
    const date = new Date();
    let emit = {
      type: 'CHECKOUT',
      docID: this.documentId,
      date: date
    };
    this.refresh.emit(emit);
    this.ngOnChanges();
  }

  checkIn() {
    this.cs.getDocument(this.docProperty.id)
      .subscribe(data => this.getPropertyDetailOnly(data , this.docProperty.id));
  }

  getPropertyDetailOnly(data , docId) {
    if (data._body  !== '' ) {
      this.docProperty = JSON.parse(data._body);
      this.checkOutdocPropertyprops = this.docProperty.props;
      this.docPropertyName = this.docProperty.className;
      this.docisReserved = this.docProperty.isReserved;
            for (const props of this.docProperty.props){
              if (props.dtype === 'DATE') {
                let dateprops: any;
                if (props.mvalues[0] !== undefined) {
                  const dateValue = props.mvalues[0];
                  dateprops = dateValue.split(' ')[0];
                }else {
                  dateprops = null;
                }
                if ( props.req === 'TRUE' || props.req === 'true') {
                  const control: FormControl = new FormControl(dateprops, Validators.required);
                  this.checkOutProp.addControl(props.symName, control);
                }else {
                  const control: FormControl = new FormControl(dateprops);
                  this.checkOutProp.addControl(props.symName, control);
                }
              }if(props.symName === 'documenttitle') {
                if ( props.req === 'TRUE' || props.req === 'true' ) {
                  let control: FormControl ;
                  if (props.mvalues[0] !== undefined) {
                    control = new FormControl(null, Validators.required);
                  } else {
                    control = new FormControl(null, Validators.required);
                  }
                  this.checkOutProp.addControl(props.symName, control);
                }else {
                  let control: FormControl ;
                  if (props.mvalues[0] !== undefined) {
                    control = new FormControl(null, Validators.required);
                  } else {
                    control = new FormControl(null, Validators.required);
                  }
                  this.checkOutProp.addControl(props.symName, control);
                }
              } else {
                if ( props.req === 'TRUE' || props.req === 'true' ) {
                  let control: FormControl ;
                  if (props.mvalues[0] !== undefined) {
                    control = new FormControl(props.mvalues[0], Validators.required);
                  } else {
                    control = new FormControl(null, Validators.required);
                  }
                  this.checkOutProp.addControl(props.symName, control);
                }else {
                  let control: FormControl ;
                  if (props.mvalues[0] !== undefined) {
                    control = new FormControl(props.mvalues[0]);
                  } else {
                    control = new FormControl(null);
                  }
                  this.checkOutProp.addControl(props.symName, control);
                }
              }
            }
    }
  }

  getPropertyDetail(data , docId) {
    if (data._body  !== '' ) {
      this.docProperty = JSON.parse(data._body);
      this.docPropertyprops = this.docProperty.props;
      this.docPropertyName = this.docProperty.className;
      this.docisReserved = this.docProperty.isReserved;
            for (const props of this.docProperty.props){
              if (props.dtype === 'DATE') {
                if ( props.req === 'TRUE' || props.req === 'true' ) {
                  let control: FormControl ;
                  // if (props.mvalues[0] !== undefined) {
                  //   control = new FormControl(null, Validators.required);
                  // } else {
                  //   control = new FormControl(null, Validators.required);
                  // }
                  // this.editProp.addControl(props.symName, control);
                  let dateprops: any;
                  if (props.mvalues[0] !== undefined) {
                    const dateValue = props.mvalues[0];
                    dateprops = dateValue.split(' ')[0];
                    control = new FormControl(dateprops, Validators.required);
                  }else {
                    dateprops = null;
                    control = new FormControl(null, Validators.required);
                  }
                  this.editProp.addControl(props.symName, control);

                }else {
                  let control: FormControl ;
                  let dateprops: any;
                  if (props.mvalues[0] !== undefined) {
                    const dateValue = props.mvalues[0];
                    dateprops = dateValue.split(' ')[0];
                    control = new FormControl(dateprops, Validators.required);
                  } else {
                    control = new FormControl(null, Validators.required);
                  }
                  this.editProp.addControl(props.symName, control);
                }
              }else {
                if ( props.req === 'TRUE' || props.req === 'true') {
                  let control: FormControl ;
                  if (props.mvalues[0] !== undefined) {
                    control = new FormControl(props.mvalues[0], Validators.required);
                  } else {
                    control = new FormControl(null, Validators.required);
                  }
                  this.editProp.addControl(props.symName, control);
                }else {
                  let control: FormControl ;
                  if (props.mvalues[0] !== undefined) {
                    control = new FormControl(props.mvalues[0]);
                  } else {
                    control = new FormControl(null);
                  }
                  this.editProp.addControl(props.symName, control);
                }
              }
            }
    this.propertyEdit = false;
    }
    this.cs.getDocumentFolders(docId).subscribe(datares => this.getDocumentFolderName(datares));
    this.cs.getDocumentVersions(docId).subscribe(datares => this.getDocumentVersions(datares));
    this.cs.getDocumentPermissions(docId).subscribe(datares => this.getDocumentPermissions(datares));
    this.cs.getAnnotations(docId).subscribe(datares => this.getListOfAnnoattion(datares));
    this.cs.getDocumentHistory(docId).subscribe(datares => this.getDocumentHistory(datares));
    this.cs.getDocumentLinks(docId).subscribe(datares => this.getDocumentLinks(datares));
  }

  getDocumentHistory(data) {
    if (JSON.parse(data._body).length !== 0) {
      this.showDocumentHistory = JSON.parse(data._body);
    }
  }

  getDocumentFolderName(data) {
    if (data._body !== '') {
      this.folderFileName = JSON.parse(data._body);
    }
  }

  getDocumentVersions(data) {
    if (data._body !== '') {
      this.versionsList = JSON.parse(data._body);
    }
  }

  getDocumentPermissions(data) {
    this.documentPermission = JSON.parse(data._body);
  }

  getListOfAnnoattion(data) {
    const resData = JSON.parse(data._body);
    if (resData.length === 0 ) {
    }else {
      this.listOfDcumentAnnotation = resData;
      this.annotationCount = this.listOfDcumentAnnotation.length;
    }
  }

  docWorkflow() {
    this.ws.initiateTaskFlow(this.documentId).subscribe(data => this.initiateTaskFlow(data));
  }

  initiateTaskFlow(data) {
    this.router.navigate(['draft/activity'], {queryParams: {'readworkitem': 'readworkitem' , 'workId': data._body}});
  }

  downloadDocument() {
    this.cs.downloadDocument(this.documentId).subscribe(data => this.downloadCurrentDocument(data) );
  }

  downloadCurrentDocument(data) {
    let filename = '';
      const disposition = data.headers.get('Content-Disposition');
      const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
          filename = matches[1].replace(/['"]/g, '');
        }
        saveAs(data._body, filename);
  }

  propertyEditClick() {
    this.propertyEdit = true;
  }

  submitedit(event, docclass) {
    const params = {'id': this.documentId , 'docclass': this.docProperty.docclass, 'props': []};
    for (const inputField of [].slice.call(event.target)) {
      if (inputField.getAttribute('type') !== 'file') {
          for (const prop of  this.docPropertyprops) {
              if (inputField.id !== undefined && inputField.id === prop.symName) {
                const property = {'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.value]};
                params.props.push(property);
              }
          }
      }
   }
   for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
         if (inputField.getAttribute('type') !== 'file') {
              for (const prop of this.docPropertyprops) {
                   if (inputField.id !== undefined && inputField.id === prop.symName) {
                     const property = {'symName': prop.symName, 'dtype': prop.dtype, 'mvalues':
                          [inputField.children['0'].children['0'].value] };
                        params.props.push(property);
                }
              }
         }
   }
   this.cs.updateProperties(params).subscribe(res => {
    this.cs.getDocument(this.documentId).subscribe(data => this.getPropertyDetail(data , this.documentId )); });
  }

  downloadThisDocument(id) {
    this.cs.downloadThisDocument(id).subscribe(data => this.downloadCurrentDocument(data) );
  }

  editPermissions(permission) {
    this.permissionModelEditForm.patchValue({
      accessLevel:  permission.accessLevel,
      accessMask: permission.accessMask,
      granteeName: permission.granteeName,
      permissionSource: permission.permissionSource,
      roleId: permission.roleId,
      aclId: permission.aclId
    });
    this.setroleList = {'value': permission.roleId,
    'name':  permission.granteeName
   };
   this.permissionRoleSelected = {'value': permission.roleId,
   'name':  permission.granteeName
  };
   this.permissionModelEditForm.controls.granteeName.disable();
  }

  documnetAnnoation(event) {
    this.docId = event.docId;
    this.curPage = event.pageNo;
    this.documentAnnotate.emit(event);
    // this.router.navigate(['documentview/viewdocument'] ,
    // {queryParams: {'documentType': 'annotate',
    // 'docId': this.documentId,
    // 'currentPage': curPage,
    // 'annotationType': localStorage.getItem('Default Annotation')}});
  }

  documentClicked(event) {
    let isReadonly;
    this.docId = event.docId;
    this.curPage = event.pagenumber;
    this.documentAnnotate.emit(event);
  }

  deletePermissions(permission) {
    const setAccessMask = permission;
    setAccessMask.accessMask = 0;
    const docSend = {
      id: this.documentId,
      permissions: [setAccessMask],
  };
  this.cs.updateDocumentSecurity(docSend).subscribe(data => this.updateDocumentSecurity(data) );
  }

    updateDocumentSecurity(data) {
      this.permissionModelForm.reset();
      this.ngxSmartModalService.getModal('permissionConformationModel').close();
      this.cs.getDocumentPermissions(this.docProperty.id).subscribe(datares => this.getDocumentPermissions(datares));
  }

  permissionFormSubmit() {
    if (this.permissionRoleSelected !== undefined && this.permissionModelForm.controls.accessMask.value !== null) {
     this.ngxSmartModalService.getModal('documentpermissionModel').close();
      const permission = {
        accessMask: this.permissionModelForm.controls.accessMask.value,
        granteeName: this.permissionRoleSelected.name,
        permissionSource: 0,
        roleId: '',
      };
      let roleId = this.permissionRoleSelected.value;
      if (roleId > 9000000) {
        roleId = roleId - 9000000;
        roleId = roleId * -1;
      }
      permission.roleId = roleId;
      const docSend = {
        id: this.documentId,
        permissions: [permission],
      };
      this.cs.updateDocumentSecurity(docSend).subscribe(data => this.updateDocumentSecurity(data) );
    } else {
      this.tr.error('', 'Please select the role');
    }
  }

  permissionEditFormSubmit() {
    if (this.permissionRoleSelected !== undefined && this.permissionModelEditForm.controls.accessMask.value !== null) {
     this.ngxSmartModalService.getModal('documentpermissionEditModel').close();
      const permission = {
        accessMask: this.permissionModelEditForm.controls.accessMask.value,
        granteeName: this.permissionRoleSelected.name,
        permissionSource: 0,
        roleId: '',
        aclId: this.permissionModelEditForm.value.aclId
      };
      let roleId = this.permissionRoleSelected.value;
      if (roleId > 9000000) {
        roleId = roleId - 9000000;
        roleId = roleId * -1;
      }
      permission.roleId = roleId;
      const docSend = {
        id: this.documentId,
        permissions: [permission],
      };
      this.cs.updateDocumentSecurity(docSend).subscribe(data => this.updateDocumentSecurity(data) );
    } else {
      this.tr.error('', 'Please select the role');
    }
  }

  roleSingleSelect(event) {
    this.permissionRoleSelected = event;
  }

  autoCompleteKeyUp(event) {
    let stringTyped = '';
    stringTyped = event.query;
    if (stringTyped.length > 2) {
      this.us.searchRoles(stringTyped).subscribe(data => this.getAllRoles(data) );
    }
  }

  getAllRoles(data) {
    this.getroleList = [];
    const getrolecategory  = JSON.parse(data._body);
    for (let index = 0; index < getrolecategory.length; index++) {
      this.getroleList.push({
        'value': getrolecategory[index].id,
        'name':  getrolecategory[index].name
       });
    }
  }

  checkInSubmit(event) {
    if (this.attachmentfile) {
      const docInfo = {
      id: this.docProperty.id,
      creator: this.docProperty.creator,
      addOn: this.docProperty.addOn,
      modOn: this.docProperty.modOn,
      docclass: this.docProperty.docclass,
      props: [],
      accessPolicies: []
      };
      for (const inputField of [].slice.call(event.target)) {
        if (inputField.getAttribute('type') !== 'file') {
            for (const prop of this.docProperty.props) {
                if (inputField.id !== undefined && inputField.id === prop.symName) {
                  const property = {'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.value], mtype: prop.mtype,
                  len: prop.len, rOnly: prop.rOnly, hidden: prop.hidden , req: prop.req, ltype: prop.ltype};
                  docInfo.props.push(property);
              }
          }
        }
      }

     for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
      if (inputField.getAttribute('type') !== 'file') {
        for (const datepick of this.docProperty.props) {
              if (inputField.id !== undefined && inputField.id === datepick.symName) {
                const property = {'symName': datepick.symName, 'dtype': datepick.dtype,
                'mvalues': [ inputField.children['0'].children['0'].value],
                len: datepick.len, rOnly: datepick.rOnly, hidden: datepick.hidden , req: datepick.req, ltype: datepick.ltype};
                docInfo.props.push(property);
              }
          }
      }
    }

    const formData = new FormData();
      formData.append('DocInfo', JSON.stringify(docInfo));
      formData.append('file', this.attachmentfile);
      this.ngxSmartModalService.getModal('checkInModel').close();
   this.cs.checkIn(formData).subscribe(data => { this.getCheckedIn(); } );
}else {
  this.tr.info('', 'Please attach a document');
}
  }

  getCheckedIn() {
    this.ngxSmartModalService.getModal('checkInModel').close();
    this.tr.success('', 'Document checked-in');
    const date = new Date();
    let emit = {
      type: 'CHECKIN',
      docID: this.documentId,
      date: date
    }
    this.refresh.emit(emit);
    this.ngOnChanges();
  }

  fileChanegd(event) {
    this.attachmentfile = null;
    this.attachmentfile = event.target.files[0];
    this.attachmentfileName = event.target.files[0].name;
    for (let i = 0; i < this.checkOutdocPropertyprops.length; i++) {
      if (this.checkOutdocPropertyprops[i].symName === 'documenttitle') {
        this.checkOutProp.controls[this.checkOutdocPropertyprops[i].symName].patchValue(event.target.files[0].name);
    }
    }
  }

  addToFolder() {
    this.userSelectedNode = null;
    // this.cs.fileInFolder()
  }

  eventEmitterFolder(event) {
    this.userSelectedNode = event;
  }

  moveFolderModelClick() {
    if (this.selectedFolderId !== this.userSelectedNode.data) {
      this.cs.fileInFolder(this.userSelectedNode.data , this.documentId ).subscribe(data => {this.moveToFolder(data); });
    }else {
      this.tr.error('', 'Same folder');
    }
  }

  moveToFolder(data) {
    this.tr.success('', 'Document moved');
    this.ngxSmartModalService.getModal('moveFolderModelGeneric').close();
    this.userSelectedNode = null;
    this.ngOnChanges();
  }
  unfileTheCurrentDocument(document) {
    this.unFileDocument = document;
  }

  unfileDocumentConformed() {
    this.cs.unfileFromFolder(this.unFileDocument.id , this.documentId).subscribe(data => this.fileinfolder(data));
  }

  fileinfolder(data) {
    this.tr.success('', 'Document unfiled');
    this.ngxSmartModalService.getModal('unfileDocumentGeneric').close();
    this.ngOnChanges();
  }


  deleteDocumentconfirmed() {
    this.cs.deleteDocument(this.documentId).subscribe(data => this.documentDelete(), error => this.ngxSmartModalService.getModal('deleteDocumentGeneric').close());
  }

  documentDelete() {
    this.ngxSmartModalService.getModal('deleteDocumentGeneric').close();
    this.tr.success('','Document deleted');
    const date = new Date();
    let emit = {
      type: 'DELETE',
      docID: this.documentId,
      date: date
    }
    this.refresh.emit(emit);
  }

  getDocumentActions(data) {
    this.documentActionsList = JSON.parse(data._body);
  }

  getDocumentLinks(datares) {
    this.documentLinkList = JSON.parse(datares._body);
  }

  unlinkDocument() {
    this.cs.unlinkDocument(this.documentId , this.selectedUnlink.id).subscribe(data => this.removeLink(data));
  }

  removeLink(data) {
    this.ngxSmartModalService.getModal('unlinkModel').close();
    this.tr.success('', 'Document unlinked');
    this.selectedUnlink = null;
    this.cs.getDocumentLinks(this.documentId).subscribe(datares => this.getDocumentLinks(datares));

  }

  closeModel(model) {
    const modal = document.getElementById(model);
    modal.style.display = 'none';
  }

  reindexDocument() {
    this.cs.indexDocument(this.documentId).subscribe(data => this.tr.info('', 'Document reindexed'));
  }

  changePageOfDocument(click) {
    const date = new Date();
    this.changePage = [ click , date.getTime];
  }
 docLinkClicked(docLink) {
  this.docLinkEmitter.emit(docLink);
 }

  deleteAnnoation(id) {
    this.cs.deleteAnnotation(id).subscribe(data => {this.deleteAnnotation(data);});
  }

  deleteAnnotation(data) {
    this.cs.getAnnotations(this.docId).subscribe( datares => this.getListOfAnnoattion(datares));
    this.tr.success('', 'Annotation deleted');
  }
  showPrint(event) {
    if (event) {
      // this
      // .ds
      // .downloadPrintDocument(event)
      // .subscribe(data => this.downloadPrintDocument(data));
      this.showPrintIs .emit(event);
    }
  }
  downloadPrintDocument(data) {
    // this.pdfUrl = data._body;
    // const file3 = new Blob([data._body], {type: 'application/pdf'});
    // this.pdfUrl = this
    //     ._sanitizer
    //     .bypassSecurityTrustResourceUrl(window.URL.createObjectURL(file3));
    // this.printPreviewTab = true;
    // this
    //     .changeDetectorRef
    //     .detectChanges();
    // // this.activeIndexNumber = 3;
    // this.activeIndexNumber = this.docProps.length + 3;


}
annotaionEmmitted(event) {
  this.showPerticularAnnotation.emit(event);
}
}
