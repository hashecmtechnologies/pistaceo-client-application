import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { SigninComponent } from './components/app-components/signin/signin.component';
import { AdminLayoutComponent } from './components/app-components/layout/admin-layout/admin-layout.component';
import { DashboardComponent } from './components/app-components/dashboard/dashboard.component';
import { CreateComponent } from './components/app-components/create/create.component';
import { InboxComponent } from './components/app-components/inbox/inbox.component';
import { SentComponent } from './components/app-components/sent/sent.component';
import { DraftComponent } from './components/app-components/draft/draft.component';
import { AnnotationComponent } from './components/app-components/annotation/annotation.component';
import { ActivityComponent } from './components/app-components/activity/activity.component';
import { DocumentViewComponent } from './components/app-components/document-view/document-view.component';
import { SearchComponent } from './components/app-components/search/search.component';
import { RegisterComponent } from './components/app-components/register/register.component';
import { AdvancedSearchComponent } from './components/app-components/advanced-search/advanced-search.component';
import { ArchiveComponent } from './components/app-components/archive/archive.component';
import { SettingsComponent } from './components/app-components/settings/settings.component';
import { ReportsComponent } from './components/app-components/reports/reports.component';
import { CreateFormNamesComponent } from './components/app-components/create-form-names/create-form-names.component';
import { AuthGuard } from './service/auth.guard';
import { UserHistoryComponent } from './components/app-components/user-history/user-history.component';
import { DataComponent } from './components/app-components/data/data.component';


export const routes: Routes = [
  { path: '', component: SigninComponent },
  {
    path: '',
    component: AdminLayoutComponent, canActivate: [AuthGuard],
    //  canDeactivate:[CanDeactivateGuard],
    children: [
      // {path: 'admin', loadChildren: './components/app components/admin/admin.module#AdminModule', canActivateChild: [AuthGuard]},
      { path: 'home', component: DashboardComponent },
      { path: 'create', component: CreateComponent },
      { path: 'createFormNames', component: CreateFormNamesComponent },
      { path: 'inbox', component: InboxComponent },
      { path: 'inbox/activity', component: ActivityComponent },
      { path: 'inbox/documentview', component: AnnotationComponent },

      { path: 'sent', component: SentComponent },
      { path: 'sent/activity', component: ActivityComponent },
      { path: 'sent/documentview', component: AnnotationComponent },

      { path: 'draft', component: DraftComponent },
      { path: 'draft/activity', component: ActivityComponent },
      { path: 'draft/documentview', component: AnnotationComponent },

      { path: 'folderview', component: DocumentViewComponent },
      { path: 'documentview/viewdocument', component: AnnotationComponent },

      { path: 'search', component: SearchComponent },

      { path: 'register', component: RegisterComponent },
      { path: 'register/activity', component: ActivityComponent },
      { path: 'register/documentview', component: AnnotationComponent },

      { path: 'advancedsearch', component: AdvancedSearchComponent },

      { path: 'archive', component: ArchiveComponent },
      { path: 'archive/activity', component: ActivityComponent },
      { path: 'archive/documentview', component: AnnotationComponent },

      { path: 'settings', component: SettingsComponent },
      { path: 'reports', component: ReportsComponent },
      { path: 'user-history', component: UserHistoryComponent },
      { path: 'data', component: DataComponent }
    ]
  }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
