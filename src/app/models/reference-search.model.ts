export class MemoSearch {
    public correspondenceType;
    public referenceId: string;
    public from: string;
    public subject: string;
    public referenceIdOper = 'starts With';
    public correspondenceTypeOper = 'is equal to';
    public fromOper = 'starts With';
    public createdDateOper = 'is equal to';
    public subjectOper = 'Like';
}
