export interface SubLevelOrg {
    id;
    parent;
    name;
    hid;
    orgCode;
    headEmpNo;
    headRoleId;
    }
