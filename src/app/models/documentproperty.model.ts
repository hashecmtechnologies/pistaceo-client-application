import { Props } from './documentprops.model';

export class DocumentProperty {
    public id: string;
    public creator: string;
    public modifier:  string;
    public className: string;
    public addOn: Date;
    public modOn: Date;
    public size:  string;
    public format:  string;
    public verNo:  string;
    public docclass:  string;
    public vsid:  string;
    public props: Props[];
    public isReserved: Boolean;
    public accessPolicies: any[];
}
