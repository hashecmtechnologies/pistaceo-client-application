export class Props {
    public name:  string;
    public symName:  string;
    public dtype:  string;
    public mvalues: any[];
    public mtype:  string;
    public len: number;
    public rOnly:  string;
    public hidden:  string;
    public req:  string;
    public ltype: number;
    public lookups: any[];
}
