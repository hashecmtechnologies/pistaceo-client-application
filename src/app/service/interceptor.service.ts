
import { Http, Request, RequestOptions, RequestOptionsArgs, Response, XHRBackend, Headers } from '@angular/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
// operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import { UserService } from './user.service';
import { environment } from '../../environments/environment.prod';
import * as global from '../global.variables';

@Injectable()
export class HttpInterceptor extends Http {
    public flag = 0;
    public base_url: String;
    private header: Headers;
    constructor(
        backend: XHRBackend,
        options: RequestOptions,
        public http: Http, private router: Router, private tr: ToastrService
    ) {
        super(backend, options);
        this.flag = 0;
    }

    public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return super.request(url, options)
            .catch(this.handleError);
    }
    // ngOnInit() {
    //     this.flag = 0;
    // }
    public handleError = (error: Response) => {
        const errorCaught = JSON.parse((<any>error)._body);
        if (errorCaught.code === 1001 && this.flag === 0) {
            this.flag = 1;
            // this.errorCall(); http://ecmdemo1:8080/pistaceo/resources/UserService/authenticateUser?
            if (environment.production) {
                this.base_url = global.AppURLSettings.SANDBOX_URL;
            } else {
                this.base_url = global.AppURLSettings.DEV_URL;
            }
            this.header = new Headers();
            const url = `${this.base_url}UserService/authenticateUser?`;
            const headers = new Headers();
            return this.http.get(url, { headers: headers }).map(
                data => {
                    this.storeToken(data);
                }).catch((err: any) => Observable.throw(this.errorHandler1(err)));
        } else {
            this.tr.error(errorCaught.message, errorCaught.status);
        }
        return Observable.throw(error);
    }
    errorHandler1(e) {
        const error = JSON.parse((<any>e)._body);
        console.log(error);
        if (error.code === 9999) {
            alert('Token is expired you need to login again');
            this.router.navigateByUrl('/');
            localStorage.clear();
            const sysDateTime = new Date();
            const fulldatetime = sysDateTime.getTime();
            const url = `${this.base_url}UserService/logoutUser?sysdatetime=${fulldatetime}`;
            return this.http.get(url, { headers: this.header }).map(
                res => this.reload(res)
            );

        }
    }
    reload(res) {
        // setTimeout(() => {
        window.location.reload();
        // }, 100);
    }
    storeToken(data) {
        localStorage.setItem('token', data._body);
        this.header.append('token', localStorage.getItem('token'));
        setTimeout(() => {
            window.location.reload();
        }, 0);
    }
}
