import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';

import * as global from '../global.variables';
import { HttpInterceptor } from './interceptor.service';
import { UserService } from './user.service';
import { environment } from '../../environments/environment';

@Injectable()
export class DataService {
    private header: Headers;
    private base_url: String;
    constructor(private http: HttpInterceptor, private us: UserService) {
        if (environment.production) {
            this.base_url = global.AppURLSettings.SANDBOX_URL;
        } else {
            this.base_url = global.AppURLSettings.DEV_URL;
        }
        this.header = new Headers();
        this.header.append('token', localStorage.getItem('token'));
    }
    getDataSchemaList() {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}DataService/getDataSchemaList?sysdatetime=${fulldatetime}`;
        return this.http.get(url, { headers: this.header }).map(
            res => res
        );
    }
    getDataSet(set) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}DataService/getDataSet?sysdatetime=${fulldatetime}`;
        return this.http.post(url, set, { headers: this.header }).map(res => res);
    }
    saveData(set) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}DataService/saveData?sysdatetime=${fulldatetime}`;
        return this.http.post(url, set, { headers: this.header }).map(res => res);
    }

}


