import {Injectable} from '@angular/core';
import {Http,  Response,  Headers, ResponseContentType} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import * as global from '../global.variables';
import {UserService} from './user.service';
import { HttpInterceptor } from './interceptor.service';
import { environment } from '../../environments/environment';

@Injectable()
export class ReportService {
  private base_url:  string;
  private header: Headers;
  constructor(private http:  HttpInterceptor) {
    this.header = new Headers();
    if (environment.production) {
      this.base_url = global.AppURLSettings.SANDBOX_URL;
    }else {
      this.base_url = global.AppURLSettings.DEV_URL;
    }
    this.header.append('token',  localStorage.getItem('token'));

  }
  getReports(): Observable<any> {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
      const url = `${this.base_url}ReportService/getReports?sysdatetime=${fulldatetime}`;
      return this.http.get(url, {headers: this.header}).map(
          res => res
      );
  }
  executePDFReport(repid) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ReportService/executePDFReport?sysdatetime=${fulldatetime}`;
    return this.http.post(url, repid, {headers: this.header, responseType: ResponseContentType.Blob }).map(
        res => res
    );
  }
  executeHTMLReport(repid) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ReportService/executeHTMLReport?sysdatetime=${fulldatetime}`;
    return this.http.post(url, repid, {headers: this.header, responseType: ResponseContentType.Blob }).map(
        res => res
    );
  }
}
