import {Injectable} from '@angular/core';
import {Http,  Response,  Headers, ResponseContentType} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import * as global from '../global.variables';
import { HttpInterceptor } from './interceptor.service';
import { environment } from '../../environments/environment';

@Injectable()
export class ConfigurationService {
  private base_url:  string;
  private header: Headers;
  constructor(private http:  HttpInterceptor) {
    this.header = new Headers();
    if (environment.production) {
      this.base_url = global.AppURLSettings.SANDBOX_URL;
    }else {
      this.base_url = global.AppURLSettings.DEV_URL;
    }
    this.header.append('token',  localStorage.getItem('token'));
  }

  getConfiguration(key) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ConfigurationService/getConfiguration?key=${key}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
        res => res
    );
  }

  getDeploymentType() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ConfigurationService/getDeploymentType?sysdatetime=${fulldatetime}`;
    return this.http.get(url).map(
        res => res
    );
  }

  getLogo() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ConfigurationService/getLogo?sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
        res => res
    );
  }

}
