import { Component, OnInit, Input, Output, ChangeDetectorRef, OnChanges, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { WorkService } from '../../../service/work.service';
import { UserService } from '../../../service/user.service';
import { DocumentService } from '../../../service/document.service';
import { MenuItem } from 'primeng/primeng';
import { TranslateService } from '../../../../../node_modules/@ngx-translate/core';
import { ToastrService } from '../../../../../node_modules/ngx-toastr';
import { MenuItemsComponent } from '../../app-components/layout/menu-items/menu-items.component';
import { AdminLayoutComponent } from '../../app-components/layout/admin-layout/admin-layout.component';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnChanges {

  @Input() workitem;
  @Input() typeFormat;
  @Input() refresh;
  public items: MenuItem[];
  public listOfTask;
  public totalPages: any;
  public pageSize: any;
  public curPage = 1;
  public selectedAll: any;
  public actionbararray = [];
  public references = [];
  public showActions = false;
  public arrays = [];
  public inboxUnread: any;
  public draftsUnread: any;
  public activityComment = '';
  public events: any;
  constructor(private router: Router, private ws: WorkService, private us: UserService, private spinerService: Ng4LoadingSpinnerService,
    private ds: DocumentService, private ref: ChangeDetectorRef, private translate: TranslateService,
    private tr: ToastrService, private app: AdminLayoutComponent, public ngxSmartModalService: NgxSmartModalService) { // private menu: MenuItemsComponent
    this.selectedAll = false;
    const browserLang: string = translate.getBrowserLang();
    translate.use('en');
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    this.items = [
    ];
  }
  actionClicked(event) {
    this.ngxSmartModalService.getModal('commentModel').open();
    this.events = event;
    this.activityComment = '';
  }
  elaborateActivity(data, temparray, references, subref, count, errorArray) {
    const value = JSON.parse(data._body);
    let flag = 0;
    for (let test = 0; test < value.type.docTypes.length; test++) {
      if (value.type.docTypes[test].req === 1 || subref.roleId === -5) {
        errorArray.push(value);
        flag = 1;
        break;
      } else {
        flag = 0;
      }
    }
    if (flag === 0) {
      const activitySubmit = {
        id: references.id,
        typeId: references.typeid,
        modifiedBy: this.us.getCurrentUser().EmpNo,
        modifierRole: this.us.getCurrentUser().roles['0'].id,
        finishedBy: this.us.getCurrentUser().EmpNo,
        finishedByName: this.us.getCurrentUser().fulName,
        finisherRole: this.us.getCurrentUser().roles['0'].id,
        responseId: subref.id,
        comments: this.activityComment,
        attachments: [],
        routes: [{
          activityType: subref.routeToId,
          activityTypeName: subref.routeToName,
          roleId: subref.roleId,
          roleName: subref.name // roleName
        }]
      };
      temparray.push(activitySubmit);
    } else {

    }
    if (count < this.references.length) {

    } else {
      //  this.spinerService.show();
      // console.log(temparray);
      if (errorArray.length > 0) {
        for (let a = 0; a < errorArray.length; a++) {
          this.tr.warning('', 'Activity with reference number' + errorArray[a].refNo + 'missing madatory fields');
        }
        // console.log('error');
        // console.log(errorArray);
        // console.log('send');
        // console.log(temparray);
      }
      if (temparray.length > 0) {
        this.ws.finishMultipleActivities(temparray).subscribe(result => { this.tr.success('', 'Finished Successfully'); this.refreshEvent(); this.selectedAll = false; this.showActions = false; this.spinerService.hide(); });
      }
    }

  }
  refreshEvent() {
    if (this.typeFormat === 'Inbox') {
      this.ws.getInboxItems(this.us.getCurrentUser().EmpNo, 1).subscribe(data => this.refreshedData(data));
    } else if (this.typeFormat === 'Sent') {
      this.ws.getSentItems(this.us.getCurrentUser().EmpNo, 1).subscribe(data => this.refreshedData(data));
    } else if (this.typeFormat === 'Archive') {
      this.ws.getArchiveItems(this.us.getCurrentUser().EmpNo, 1).subscribe(data => this.refreshedData(data));
    } else if (this.typeFormat === 'Draft') {
      this.ws.getDraftItems(this.us.getCurrentUser().EmpNo, 1).subscribe(data => this.refreshedData(data));
    } else {

    }
  }
  downloadEvent() {
    if (this.typeFormat === 'Inbox') {
      this.ws.getInboxItemsAsExcel().subscribe(data => this.excelDownloaded(data));
    } else if (this.typeFormat === 'Sent') {
      this.ws.getSentItemsAsExcel().subscribe(data => this.excelDownloaded(data));
    } else if (this.typeFormat === 'Archive') {
      this.ws.getArchiveItemsAsExcel().subscribe(data => this.excelDownloaded(data));
    } else if (this.typeFormat === 'Draft') {
      this.ws.getDraftItemsAsExcel().subscribe(data => this.excelDownloaded(data));
    } else {

    }
  }
  excelDownloaded(data) {
    let filename = '';
    const disposition = data.headers.get('Content-Disposition');
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(disposition);
    if (matches != null && matches[1]) {
      filename = matches[1].replace(/['"]/g, '');
    }
    saveAs(data._body, filename);
  }
  refreshedData(data) {
    const value = JSON.parse(data._body);
    this.listOfTask = value.activities;
    if (this.typeFormat === 'Inbox') {
      let count = 0;
      for (let index = 0; index < this.listOfTask.length; index++) {
        if (this.listOfTask[index].status === 'NEW') {
          count++;
        }
      }
      this.inboxUnread = count;
      // this.menu.updateInboxBadge('Inbox', count);
      this.app.updateValue('Inbox', count);
    }
    if (this.typeFormat === 'Draft') {
      let draftscount = 0;
      draftscount = this.listOfTask.length;
      this.draftsUnread = draftscount;
      this.app.updateValue('Drafts', draftscount);
    }
  }
  ngOnChanges() {
    if (this.workitem !== undefined) {
      this.listOfTask = this.workitem.activities;
      this.curPage = this.workitem.curPage;
      this.pageSize = this.workitem.pageSize;
      this.totalPages = this.workitem.totalCount;
    }

  }
  pagination(event) {
    let page;
    page = event.page + 1;
    if (this.typeFormat === 'Inbox') {
      this.ws.getInboxItems(this.us.getCurrentUser().EmpNo, page).subscribe(data => this.refreshedData(data));
    } else if (this.typeFormat === 'Sent') {
      this.ws.getSentItems(this.us.getCurrentUser().EmpNo, page).subscribe(data => this.refreshedData(data));
    } else if (this.typeFormat === 'Archive') {
      this.ws.getArchiveItems(this.us.getCurrentUser().EmpNo, page).subscribe(data => this.refreshedData(data));
    } else if (this.typeFormat === 'Draft') {
      this.ws.getDraftItems(this.us.getCurrentUser().EmpNo, page).subscribe(data => this.refreshedData(data));
    } else {

    }

  }
  onTopbarRootItemClick(event: Event, item: Element) {
    // if (this.activeTopbarItem === item) {
    //     this.activeTopbarItem = null; } else {
    //     this.activeTopbarItem = item; }

    // event.preventDefault();
  }

  readMail(work) {
    if (this.typeFormat === 'Sent') {
      this.router.navigate(['sent/activity'], {
        queryParams:
          { 'readworkitem': 'readonly', 'workId': work.id, 'workAssignedTo': work.assignedTo, 'workAssignedToName': work.assignedToName, 'typeFormat': this.typeFormat }
      });
    } else if (this.typeFormat === 'Archive') {
      this.router.navigate(['/archive/activity'], { queryParams: { 'readworkitem': 'readOnly', 'workId': work.id, 'typeFormat': this.typeFormat, } });
    } else if (this.typeFormat === 'Draft') {
      this.router.navigate(['/draft/activity'], { queryParams: { 'readworkitem': 'readworkitem', 'workId': work.id, 'typeFormat': 'Drafts' } });
    } else {
      if (work.status === 'NEW') {
        this.ws.markActivityAsRead(this.us.getCurrentUser().EmpNo, work.id, this.us.getCurrentUser().roles['0'].id).subscribe(data => data);
      }
      this.router.navigate(['/inbox/activity'], { queryParams: { 'readworkitem': 'readworkitem', 'workId': work.id, 'typeFormat': this.typeFormat } });
    }
    // }else {
    //   if (work.status === 'NEW') {
    //     this.ws.markActivityAsRead(this.us.getCurrentUser().EmpNo,
    //       work.id, this.us.getCurrentUser().roles['0'].id ).subscribe(data => data);
    //   }
    //   this.router.navigate(['inbox/activity'], { queryParams:
    //     { 'readworkitem': 'readworkitem' , 'workId': work.id }});
    // }
  }
  checkIfAllSelected(event, id, typeid) {
    this.selectedAll = this.listOfTask.every(function (item: any) {
      return item.selected === true;
    });
    if (event === true) {
      this.ws.getActivityResponses(id).subscribe(data => { this.temparray(data, id, typeid); }); // this.temparray(data, id, typeid)
    } else if (event === false) {
      for (let j = 0; j < this.arrays.length; j++) {
        let res;
        res = this.arrays[j];
        for (let k = 0; k < res.length; k++) {
          if (res[k] === id) {
            this.arrays.splice(j, 1);
            this.findelement(this.arrays);
            break;
          }
        }
      }
      for (let i = 0; i < this.references.length; i++) {
        if (this.references[i].id === id) {
          this.references.splice(i, 1);
          // if (this.references.length === 1) {
          //   this.pushElementsToItems();
          // }
          // this.checkedreferences.emit(this.references);
          break;
        }
        //       }
      }
    }
  }
  temparray(data, id, typeid) {
    let value;
    value = JSON.parse(data._body);
    let temparay;
    temparay = [];
    let arrs;
    arrs = {
      'id': id,
      'res': value,
      'typeid': typeid
    };
    this.references.push(arrs);
    // if (value.length === 0) {
    //   temparay.push('temp', id);
    // }else {
    for (let i = 0; i < value.length; i++) {
      temparay.push('temp', value[i].name, id);
    }
    // }
    //   value.forEach(function(childObj) {
    //    temparay.push('temp', childObj.name, id);
    //  });
    this.arrays.push(temparay);
    this.findelement(this.arrays);
  }

  pushElementsToItems(resArr) {
    this.items = [];
    for (let c = 0; c < resArr.length; c++) {
      const element = {
        label: this.translate.instant(resArr[c]),
        command: (event) => {
          this.actionClicked(event);
        }
      };
      this.items.push(element);
      this.showActions = true;
    }
  }
  findelement(arrs) {
    const resArr = [];
    this.actionbararray = [];
    if (arrs.length > 0) {
      for (var i = arrs[0].length - 1; i > 0; i--) {
        for (var j = arrs.length - 1; j > 0; j--) {
          if (arrs[j].indexOf(arrs[0][i]) === -1) {
            break;
          }
        }
        if (j === 0) {
          resArr.push(arrs[0][i]);
        }
      }
      this.actionbararray = resArr.concat();
      this.selectSome(resArr);
      return resArr;
    } else {
      this.items = [];
      this.showActions = false;
    }
  }
  selectAll(event) {
    for (let i = 0; i < this.listOfTask.length; i++) {
      this.listOfTask[i].selected = this.selectedAll;
    }
    if (event === true) {
      for (let i = 0; i < this.listOfTask.length; i++) {
        this.ws.getActivityResponses(this.listOfTask[i].id).subscribe(data => { this.temparray(data, this.listOfTask[i].id, this.listOfTask[i].typeId); });
      }
    } else {
      this.arrays = [];
      this.references = [];
      this.findelement(this.arrays);
    }
  }
  selectSome(resArr) {
    this.items = [];
    let amt;
    amt = [];
    for (let a = 0; a < resArr.length; a++) {
      let value;
      value = (typeof resArr[a]);
      if (value !== 'number' && resArr[a] !== 'temp') {
        amt.push(resArr[a]);
      }
    }
    if (amt.length > 0) {
      this.pushElementsToItems(amt);
    } else {
      this.showActions = false;
      this.items = [];
    }
  }
  closeComment() {
    this.ngxSmartModalService.getModal('commentModel').close();
  }
  addComment() {
    if (this.activityComment) {
      this.ngxSmartModalService.getModal('commentModel').close();
      let temparray;
      temparray = [];
      let count = 0;
      let errorArray;
      errorArray = [];
      for (let amt = 0; amt < this.references.length; amt++) {
        for (let aks = 0; aks < this.references[amt].res.length; aks++) {
          if (this.references[amt].res[aks].name === this.events.item.label) {
            this.ws.getActivityForAction(this.references[amt].id).subscribe(data => {
              const value = this.elaborateActivity(data, temparray, this.references[amt], this.references[amt].res[aks], ++count, errorArray);
            }, error => { });

          }
        }
      }

    } else {
      this.tr.error('', 'Comment is mandatory');
    }
  }
}

