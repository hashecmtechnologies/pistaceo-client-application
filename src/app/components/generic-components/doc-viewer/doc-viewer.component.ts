import { Component, OnInit, Input, OnChanges, AfterViewInit, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { DocumentService } from '../../../service/document.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UserService } from '../../../service/user.service';
import { ContentService } from '../../../service/content.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { saveAs } from 'file-saver';
import { DomSanitizer } from '@angular/platform-browser';
import { BreadcrumbService } from '../../app-components/layout/breadcrumb/breadcrumb.service';
@Component({
  selector: 'app-doc-viewer',
  templateUrl: './doc-viewer.component.html',
  styleUrls: ['./doc-viewer.component.css']
})
export class DocViewerComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() type;
  @Input() docId;
  @Input() pageNumber;
  @Input() timestamp;
  @Input() documentPermission;
  @Output() documentClicked = new EventEmitter();
  @Input() changePage;
  @Output() printTrue = new EventEmitter();
  @Output() annotionValue = new EventEmitter();
  @Input() primaryType;

  public previewImage;
  public imageToShow = '';
  public curPage = 0;
  public totalPages;
  public displayImage = false;
  public annotationCount = 0;
  public listOfDcumentAnnotation = [];
  public pdfUrl: any;
  public toolBar = false;
  public showVideoURL = null;
  constructor(private ds: DocumentService, private router: Router,
    private _location: Location, public cs: ContentService, private ngxSmartModalService: NgxSmartModalService,
    private us: UserService, private route: ActivatedRoute, private tr: ToastrService, private _sanitizer: DomSanitizer,
    private changeDetectorRef: ChangeDetectorRef, private breadcrumbService: BreadcrumbService) {
  }

  ngOnInit() {

  }

  ngOnChanges() {
    if (this.changePage === undefined) {
      this.imageToShow = '';
      if (this.docId !== undefined && this.docId !== '') {
        if (this.breadcrumbService.getItem() === 'Inbox' || this.breadcrumbService.getItem() === 'Sent' || this.breadcrumbService.getItem() === 'Draft') {
          this.toolBar = false;
          this.ds.getAnnotations(this.docId).subscribe(data => this.getListOfAnnoattionCount(data));
        } else {
          this.toolBar = true;
          this.cs.getAnnotations(this.docId).subscribe(data => this.getListOfAnnoattionCount(data));
        }

        if (this.primaryType !== 'video/mp4' && this.primaryType !== 'audio/mpeg') {
          if (this.type === 'document') {
            this.cs.getPreviewPage(this.docId, this.pageNumber).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());
          } else {
            this.ds.getPreviewPage(this.docId, this.pageNumber).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());
          }
        } else {
          this.showVideoURL = null;
          if (this.primaryType !== 'audio/mpeg') {
            this.ds.downloadURLDocument(this.docId).subscribe(data => { this.downloadedVideo(data, 'video/mp4'); }, error => { });
          } else {
            this.ds.downloadURLDocument(this.docId).subscribe(data => { this.downloadedAudio(data, 'audio/mpeg'); }, error => { });
          }
        }
      }
    } else {
      this.showVideoURL = null;
      if (this.changePage[0] === 'FIRST') {
        if (this.type === 'document') {
          this.cs.getPreviewPage(this.docId, 1).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());
        } else {
          this.ds.getPreviewPage(this.docId, 1).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());
        }
      } else if (this.changePage[0] === 'PREVIOUS') {
        let previous: number;
        previous = this.curPage - 1;
        if (this.type === 'document') {
          this.cs.getPreviewPage(this.docId, previous).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());
        } else {
          this.ds.getPreviewPage(this.docId, previous).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());
        }
      } else if (this.changePage[0] === 'NEXT') {
        if (this.type === 'document') {
          if (this.curPage < this.totalPages && this.curPage >= 0) {
            this.cs.getPreviewPage(this.docId, +this.curPage + 1).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());
          } else {
            this.cs.getPreviewPage(this.docId, this.curPage).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());

          }
        } else {
          if (this.curPage < this.totalPages && this.curPage >= 0) {
            this.ds.getPreviewPage(this.docId, +this.curPage + 1).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());
          } else {
            this.ds.getPreviewPage(this.docId, this.curPage).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());

          }
        }
      } else if (this.changePage[0] === 'LAST') {
        if (this.type === 'document') {
          this.cs.getPreviewPage(this.docId, this.totalPages).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());
        } else {
          this.ds.getPreviewPage(this.docId, this.totalPages).subscribe(data => this.getPreviewImage(data), error => this.noPreviewFound());
        }
      }
    }
    if (this.docId !== undefined) {
      if (this.type === 'document') {
        this.cs.getDocumentActions(this.docId).subscribe(data => this.documentPermissionMethod(data));
      } else {
        this.cs.getDocumentActions(this.docId).subscribe(data => this.documentPermissionMethod(data));
      }

    }
  }
  downloadedVideo(data, type) {
    this.displayImage = true;
    const value = (data._body);
    this.imageToShow = null;
    const blob = new Blob([value], { type: type });
    const url = window.URL.createObjectURL(blob);
    this.showVideoURL = this._sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  downloadedAudio(data, type) {
    this.displayImage = true;
    const value = (data._body);
    this.imageToShow = null;
    const blob = new Blob([value], { type: type });
    const url = window.URL.createObjectURL(blob);
    this.showVideoURL = this._sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  getListOfAnnoattionCount(data) {
    const count = JSON.parse(data._body);
    this.annotationCount = count.length;
  }
  documentPermissionMethod(resdata) {
    this.documentPermission = JSON.parse(resdata._body);
  }

  ngAfterViewInit(): void {

  }

  getPreviewImage(data) {
    this.previewImage = JSON.parse(data._body);
    this.imageToShow = 'data:image/png;base64,' + this.previewImage.image;
    this.curPage = this.previewImage.pageNo;
    this.totalPages = this.previewImage.pageCount;
    this.displayImage = true;
  }

  paginate(pageNo) {
    if (pageNo <= this.totalPages && pageNo >= 0) {
      this.displayImage = false;
      this.cs.getPreviewPage(this.docId, pageNo).subscribe(data => this.getPreviewImage(data));
    }
  }

  paginatePage(pageNo) {
    if (pageNo.page + 1 <= this.totalPages && pageNo.page >= 0) {
      this.displayImage = false;
      this.cs.getPreviewPage(this.docId, pageNo.page + 1).subscribe(data => this.getPreviewImage(data));
    }
  }


  annotationClicked() {
    let isReadonly;
    if (this.type === 'document') {
      if (this.documentPermission.includes('VIEW')) {
        if (this.curPage !== -1) {
          localStorage.setItem('backClick', this.router.url);
          this.route.queryParams.subscribe(param => {
            isReadonly = param['readworkitem'];
          });
          if (this.router.url.includes('inbox')) {
            if (isReadonly === 'readonly') {
              this.documentClicked.emit({
                'pagenumber': this.curPage,
                'docId': this.docId,
                'type': 'readonly'
              });
            } else {
              this.documentClicked.emit({
                'pagenumber': this.curPage,
                'docId': this.docId,
                'type': 'annotate'
              });
            }
          } else if (this.router.url.includes('sent')) {
            this.documentClicked.emit({
              'pagenumber': this.curPage,
              'docId': this.docId,
              'type': 'readonly'
            });
          } else if (this.router.url.includes('draft')) {
            if (isReadonly === 'readonly') {
              this.documentClicked.emit({
                'pagenumber': this.curPage,
                'docId': this.docId,
                'type': 'readonly'
              });
            } else {
              this.documentClicked.emit({
                'pagenumber': this.curPage,
                'docId': this.docId,
                'type': 'annotate'
              });
            }
          } else if (this.router.url.includes('register')) {
            this.documentClicked.emit({
              'pagenumber': this.curPage,
              'docId': this.docId,
              'type': 'readonly'
            });
          } else if (this.router.url.includes('archive')) {
            this.documentClicked.emit({
              'pagenumber': this.curPage,
              'docId': this.docId,
              'type': 'readonly'
            });
          } else {
            this.documentClicked.emit({
              'pagenumber': this.curPage,
              'docId': this.docId,
              'type': 'annotate'
            });
          }
        } else {
          this.tr.warning('', 'Document preview is not avaliable');
        }
      } else {
        this.tr.error('', 'Don\'t have permission to view this document');
      }
    } else {
      if (this.curPage !== -1) {
        localStorage.setItem('backClick', this.router.url);
        this.route.queryParams.subscribe(param => {
          isReadonly = param['readworkitem'];
        });
        if (this.router.url.includes('inbox')) {
          if (isReadonly === 'readonly') {
            this.documentClicked.emit({
              'pagenumber': this.curPage,
              'docId': this.docId,
              'type': 'readonly'
            });
          } else {
            this.documentClicked.emit({
              'pagenumber': this.curPage,
              'docId': this.docId,
              'type': 'annotate'
            });
          }
        } else if (this.router.url.includes('sent')) {
          this.documentClicked.emit({
            'pagenumber': this.curPage,
            'docId': this.docId,
            'type': 'readonly'
          });
        } else if (this.router.url.includes('draft')) {
          if (isReadonly === 'readonly') {
            this.documentClicked.emit({
              'pagenumber': this.curPage,
              'docId': this.docId,
              'type': 'readonly'
            });
          } else {
            this.documentClicked.emit({
              'pagenumber': this.curPage,
              'docId': this.docId,
              'type': 'annotate'
            });
          }
        } else if (this.router.url.includes('register')) {
          this.documentClicked.emit({
            'pagenumber': this.curPage,
            'docId': this.docId,
            'type': 'readonly'
          });
        } else if (this.router.url.includes('archive')) {
          this.documentClicked.emit({
            'pagenumber': this.curPage,
            'docId': this.docId,
            'type': 'readonly'
          });
        } else {
          this.documentClicked.emit({
            'pagenumber': this.curPage,
            'docId': this.docId,
            'type': 'annotate'
          });
        }
      }
    }


  }
  noPreviewFound() {
    this.displayImage = true;
    this.imageToShow = null;
  }

  listDocumentAnnotation() {
    this
      .cs
      .getAnnotations(this.docId)
      .subscribe(data => this.getListOfAnnoattion(data));
  }

  getListOfAnnoattion(data) {
    const resData = JSON.parse(data._body);
    if (resData.length === 0) {
      this
        .tr
        .info('', 'No annotation found on this document');
      this.annotationCount = 0;
      this.ngxSmartModalService.getModal('annotationListModel').close();
    } else {
      this.listOfDcumentAnnotation = JSON.parse(data._body);
      this.annotationCount = this.listOfDcumentAnnotation.length;
      this.ngxSmartModalService.getModal('annotationListModel').open();
    }
    const count = JSON.parse(data._body);
    this.annotationCount = count.length;
  }

  downloadDocument() {
    this.cs.downloadDocument(this.docId).subscribe(data => this.downloadCurrentDocument(data));

  }

  downloadWithAnnotation() {
    this.cs.downloadAnnotatedDocument(this.docId).subscribe(data => this.downloadCurrentDocument(data));

  }

  downloadCurrentDocument(data) {
    let filename = '';
    const disposition = data
      .headers
      .get('Content-Disposition');
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(disposition);
    if (matches != null && matches[1]) {
      filename = matches[1].replace(/['"]/g, '');
    }
    saveAs(data._body, filename);
  }
  deleteAnnoation(id) {
    this.cs.deleteAnnotation(id).subscribe(data => { this.deleteAnnotation(data); });
  }

  deleteAnnotation(data) {
    this
      .cs
      .getAnnotations(this.docId)
      .subscribe(datares => this.getListOfAnnoattion(datares));
    this.tr.success('', 'Annotation deleted');
  }

  // documnetAnnoation(pageNo, type, event){
  //   this.pageNumber = pageNo;
  //   this.type = type;
  //   this.ngOnChanges();
  // }
  printPrimaryDocument() {
    this.printTrue.emit(this.docId);
  }
  documnetAnnoation(pageNo, type, $event) {
    const value = {
      pageNo: pageNo,
      type: type,
      docId: this.docId
    };
    this.annotionValue.emit(value);
  }

}
