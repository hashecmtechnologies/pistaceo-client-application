import { Component, OnInit, OnChanges, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { ContentService } from '../../../service/content.service';
import { TreeNode, Tree } from 'primeng/primeng';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-folder-tree',
  templateUrl: './folder-tree.component.html',
  styleUrls: ['./folder-tree.component.css']
})
export class FolderTreeComponent implements OnChanges {

  @Input() timestamp;
  @ViewChild('expandingTree') expandingTree: Tree;
  @Input() selectedFolderId;
  @Output() emitSelectedFolder = new EventEmitter();
  @Input() type;
  public files: TreeNode[];
  public selectedNode;
  constructor(private cs: ContentService, private ts: ToastrService) { }

  ngOnChanges() {
    this.cs.getAppRepository().subscribe(res => this.updateRepositoryValue(res));
  }

  updateRepositoryValue(res) {
    const resp = JSON.parse(res._body);
    this.files = [{
      label: resp.name,
      collapsedIcon: 'ui-icon-folder',
      expandedIcon: 'ui-icon-folder-open',
      expanded: true,
      children: []
    }];
    this.cs.getTopFolders().subscribe(folderdata => this.getTopFolders(folderdata) );
  }

  getTopFolders(folderdata) {
    const childrenNode = [];
    for (const orgData of JSON.parse(folderdata._body)) {
      const childNode = {
        data: orgData.id ,
        label : orgData.name,
        collapsedIcon: 'ui-icon-folder',
        expandedIcon: 'ui-icon-folder-open',
        expanded: true,
        children: []
      };
      this.files[0].children.push(childNode);
    }
  }
  
  onNodeClicked(node:TreeNode) {
    this.selectedNode = node;
    this.emitSelectedFolder.emit(this.selectedNode);
    this.cs.getSubfolders(node.data).subscribe(resdata => this.expandRecursive(resdata,node , this.files[0].children));
  }

  private expandRecursive(data , node , obj) {
    for (const k in obj) {
      if (typeof obj[k] === 'object' && obj[k] !== null) {
          if(this.selectedFolderId === obj[k].data && this.type === 'FOLDER') {
            // this.ts.warning('Same folder');
          }else {
              if (obj[k].data === this.selectedNode.data && node.parent !== undefined) {
                obj[k].children = [];
                for (const childNodeDetails of JSON.parse(data._body)) {
                  const childNode = {
                    data: childNodeDetails.id ,
                    label : childNodeDetails.name,
                    collapsedIcon: 'ui-icon-folder',
                    expandedIcon: 'ui-icon-folder-open',
                    expanded: true,
                    children: []
                  };
                  obj[k].children.push(childNode);
                }
                break;
              }
        this.expandRecursive(data, obj[k], obj[k].children);
        }
      }
    }
}
}
