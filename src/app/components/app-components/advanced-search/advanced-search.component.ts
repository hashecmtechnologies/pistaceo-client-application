import { Component, OnInit, ViewChild } from '@angular/core';
import { saveAs } from 'file-saver';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DocumentService } from '../../../service/document.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../service/user.service';
import { SchemaService } from '../../../service/schema.service';
import { ContentService } from '../../../service/content.service';
import { WorkService } from '../../../service/work.service';
import * as operators from '../../../operators.variables';
import { MemoSearch } from '../../../models/reference-search.model';
import { TranslateService } from '../../../../../node_modules/@ngx-translate/core';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { Accordion } from 'primeng/primeng';
import { isNullOrUndefined } from 'util';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-advanced-search',
  templateUrl: './advanced-search.component.html',
  styleUrls: ['./advanced-search.component.css']
})
export class AdvancedSearchComponent implements OnInit {

  public correspondenceindex = 0;
  dateValue: boolean;
  isCorrespondenceCollapse = false;
  isCorrespondenceResult = true;
  isDocumentCollapse = true;
  isDocumentResult = false;
  isWorkCollapse = false;
  isWorkResult = true;
  public workTypeDropdown = [];
  public listOfTaskActivities = [];
  public historydata = [];
  public closeResult: any;
  public attachmentres = [];
  memoclass = [{ id: 1, name: 'All' }, { id: 2, name: 'Incoming' }, { id: 3, name: 'Outgoing' }];
  SearchMemo;
  urlid = 'Correspondence';
  searchResultDiv = true;
  documentClass;
  documentClassProp: any[];
  documentSearchResult;
  searchedDocument;
  createdDateSearch;
  resultMemo;
  searchCorrespondenceResultDiv = true;
  // memoSearch: FormGroup;
  documentsearch: FormGroup;
  errorMessage: String;
  activeIdString: any;
  advancedSearch: any;
  stringDropdown: any[];
  numberDropdown: any[];
  corrspondenceType: any[];
  dateDropdown: any[];
  between = false;
  memoSearch: MemoSearch;
  workTypeSearch: FormGroup;
  public workTypeProps: any;
  public workTypeSearchResult = [];
  public workSearchResultDiv = true;
  public selectedTab = 0;
  public documentClassToshow = [];
  public docClassSymName: any;
  public docClassName: any;
  public search;
  public ddmmyy = true;
  public createdRole = [];
  public roleDropItems: any;
  public dateRange: any;
  public classPropName: any;
  public tabIsDisabled = false;
  public workflowSearch: any;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private cs: ContentService, public ngxSmartModalService: NgxSmartModalService,
    private ds: DocumentService, private us: UserService, private ss: SchemaService, private ws: WorkService, private tr: ToastrService,
    private router: Router, private translate: TranslateService, private breadcrumbService: BreadcrumbService) {
    if (localStorage.getItem('Date Format') === 'DD/MM/YYYY') {
      this.ddmmyy = true;
    } else {
      this.ddmmyy = false;
    }
    this.ddmmyy = true;
    this.memoSearch = new MemoSearch();
    this.memoSearch.correspondenceTypeOper = 'Equals To';
    this.memoSearch.correspondenceType = 'All';
    this.memoSearch.subjectOper = 'Contains';
    this.memoSearch.createdDateOper = 'Before';
    const browserLang: string = translate.getBrowserLang();
    translate.use('en');
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    this.breadcrumbService.setItems([
      { label: this.translate.instant('Advanced Search') }
    ]);
  }

  ngOnInit() {
    this.corrspondenceType = operators.corrspondenceType;
    this.dateDropdown = operators.dateOperator;
    this.stringDropdown = operators.stringOperator;
    this.numberDropdown = operators.stringOperator;
    this.documentsearch = this.fb.group({
      documentname: [],
    });
    this.workTypeSearch = this.fb.group({
      workTypeName: []
    });

    this.route.queryParams.subscribe(params => {
      this.urlid = params['seachType'];
      if (this.urlid === this.translate.instant('Correspondence')) {
        this.ss.getWorkTypesForSearch().subscribe(data => this.workTypesRes(data));
        this.selectedTab = 1;
        this.correspondenceindex = 0;
      } else {
        this.selectedTab = 0;
        this.correspondenceindex = 0;
        this.cs.getDocumentClasses().subscribe(data => this.documentClasses(data));
      }
    });
    if (this.urlid === this.translate.instant('Document')) {
      this.activeIdString = 'documentTab';
    }
    this.correspondenceindex = 0;
    this.dateRange = false;
    this.classPropName = '';
    this.tabIsDisabled = false;
  }
  workTypesRes(data) {
    this.workTypeDropdown = [];
    if (data !== '' || data !== undefined) {
      this.workTypeDropdown = JSON.parse(data._body);
      if (this.workTypeDropdown.length > 0) {
        this.workTypeSearch.patchValue({ workTypeName: this.workTypeDropdown[0].id });
        this.ss.getWorkTypeSearch(this.workTypeDropdown[0].id).subscribe(datares => this.workTypeProp(datares));
      }
    }
  }
  workTypeProp(data) {
    if (data !== '' || data !== undefined) {
      const value = JSON.parse(data._body);
      for (let i = 0; i < value.properties.length; i++) {
        const control: FormControl = new FormControl(null);
        this.workTypeSearch.addControl(value.properties[i].symName, control);
      }
      this.workTypeProps = value.properties;
    }
  }

  documentSubmit(event) {
    let flag = 0;
    let name = this.docClassName;
    let symName = this.docClassSymName;
    // for (const selectField of [].slice.call(event.target.getElementsByTagName('select'))) {
    //   if (selectField.id !== undefined && selectField.id === 'docClass') {
    //     symName = selectField.options[selectField.options.selectedIndex].text;
    //     name = selectField.options[selectField.options.selectedIndex].value;
    //   }
    // }

    // for (const docClass of this.documentClass) {
    //   for (const prop of docClass.props) {
    //     if (symName === docClass.name) {
    //       symName = docClass.symName;
    //     }
    //   }
    // }
    // for (const selectField of [].slice.call(event.target.getElementsByTagName('p-dropdown'))) {
    //   if (selectField.id !== undefined && selectField.id === 'docClass') {
    //      symName = this.docClassSymName;
    //      name = this.docClassName;
    //   }
    // }
    const search = {
      'name': name, 'symName': symName, 'type': 'DOCUMENT', 'props': [],
      'contentSearch': { 'name': 'Content', 'symName': 'CONTENT', 'dtype': 'STRING', 'mvalues': [], 'oper': '' }
    };
    for (const inputField of [].slice.call(event.target)) {
      for (const selectField of [].slice.call(event.target.getElementsByTagName('p-dropdown'))) {
        if (inputField.getAttribute('type') !== 'file') {
          for (const docClass of this.documentClass) {
            for (const prop of docClass.props) {
              if (symName === docClass.symName) {
                if (inputField.id !== undefined && inputField.id === prop.symName && selectField.id !== undefined && selectField.id === prop.symName) {
                  const property = { 'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.value], 'oper': selectField.children['0'].children[1].innerText };
                  delete prop.between;
                  search.props.push(property);
                  const index = docClass.props.indexOf(prop);
                  this.documentsearch.controls[this.documentClassProp[index].symName].patchValue('');
                  if (inputField.value !== '') {
                    flag = 1;
                  }
                }
              }
            }
          }
        }
      }
    }

    for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
      for (const selectField of [].slice.call(event.target.getElementsByTagName('p-dropdown'))) {
        if (inputField.getAttribute('type') !== 'file') {
          for (const docClass of this.documentClass) {
            for (const prop of docClass.props) {
              if (symName === docClass.symName) {
                if (inputField.id !== undefined && inputField.id === prop.symName && selectField.id !== undefined && selectField.id === prop.symName) {
                  let value;
                  if (inputField.children['0'].children['0'].value.includes('-')) {
                    const val = inputField.children['0'].children['0'].value.split('-');
                    value = [];
                    value.push(val[0].split(' ')[0]);
                    value.push(val[1].split(' ')[1]);
                    const property = { 'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': value, 'oper': selectField.children['0'].children[1].innerText };
                    search.props.push(property);
                    const index = docClass.props.indexOf(prop);
                    this.documentsearch.controls[this.documentClassProp[index].symName].patchValue('');
                  } else {
                    const property = { 'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.children['0'].children['0'].value], 'oper': selectField.children['0'].children[1].innerText };
                    delete prop.between;
                    search.props.push(property);
                    const index = docClass.props.indexOf(prop);
                    this.documentsearch.controls[this.documentClassProp[index].symName].patchValue('');
                  }

                  if (inputField.value !== '') {
                    flag = 1;
                  }
                }
              }
            }
          }
        }
      }
    }

    //   for (const inputField of [].slice.call(event.target.getElementsByTagName('p-dropdown'))) {
    //     for (const selectField of [].slice.call(event.target.getElementsByTagName('p-dropdown'))) {
    //        if (inputField.getAttribute('type') !== 'file') {
    //          for (const docClass of this.documentClass) {
    //             for (const prop of docClass.props) {
    //               if (symName === docClass.symName) {
    //                  if (inputField.id !== undefined && inputField.id === prop.symName && selectField.id !== undefined && selectField.id === prop.symName) {
    //                    const property = {'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.value], 'oper': selectField.children['0'].children[1].innerText };
    //                       search.props.push(property);
    //                       if(inputField.value !== '') {
    //                        flag = 1;
    //                       }
    //                  }
    //               }
    //             }
    //          }
    //        }
    //     }
    //  }

    //  if(flag === 0) {
    //   this.tr.error('Enter at lest one field');
    //  }else {
    this.searchedDocument = [];
    this.search = search;
    this.cs.searchDocuments(search).subscribe(data => this.getsearchedDocuments(data), error => { });
    this.isDocumentCollapse = false;
    this.isDocumentResult = true;
    //  }

  }
  searchApiCalled(event) {
    if (event) {
      this.cs.searchDocuments(this.search).subscribe(data => this.getsearchedDocuments(data), error => { });
    }
  }
  getsearchedDocuments(data) {
    this.searchedDocument = data;
    if (this.searchedDocument.length > 0) {
      this.searchResultDiv = false;
    } else {
      this.searchResultDiv = true;
    }
    this.correspondenceindex = 1;
    this.advancedSearch = 'advancedSearch';
    this.isDocumentCollapse = false;
    this.isDocumentResult = true;
  }


  documentClasses(data) {
    if (data._body !== '') {
      this.documentClassToshow = [];
      this.documentClass = JSON.parse(data._body);
      this.documentsearch.patchValue({
        documentname: this.documentClass[0].id
      });
      this.docClassSymName = this.documentClass[0].symName;
      this.docClassName = this.documentClass[0].name;
      for (let i = 0; i < this.documentClass[0].props.length; i++) {
        const control: FormControl = new FormControl(null);
        this.documentsearch.addControl(this.documentClass[0].props[i].symName, control);
        if (this.documentClass[0].props[i].lookups !== undefined) {
          this.documentsearch.controls[this.documentClass[0].props[i].symName].patchValue(this.documentClass[0].props[i].lookups[0].value);
        }
      }
      this.documentClassProp = this.documentClass[0].props;
      this.documentClass[0].props.between = '';
    }
  }

  documentClassChanged(event) {
    for (const prop of this.documentClass) {
      this.documentClassProp = prop.props;
      if (prop.id === event.target.value) {
        for (let i = 0; i < this.documentClassProp.length; i++) {
          const control: FormControl = new FormControl(null);
          this.documentsearch.addControl(this.documentClassProp[i].symName, control);
          this.documentClassProp[i].between = '';
          if (this.documentClassProp[i].lookups !== undefined) {
            this.documentsearch.controls[this.documentClassProp[i].symName].setValue(this.documentClassProp[i].lookups[0].value);
          }
        }
        this.docClassSymName = prop.symName;
        this.docClassName = prop.name;
      }
    }

    for (let i = 0; i < this.documentClass.length; i++) {
      if (this.documentClass[i].id === event.target.value) {
        this.documentsearch.reset();
        this.documentsearch.patchValue({ documentname: this.documentClass[i].id });
        for (let j = 0; j < this.documentClass[i].props.length; j++) {
          const control: FormControl = new FormControl(null);
          this.documentsearch.addControl(this.documentClass[i].props[j].symName, control);
          if (this.documentClass[i].props[j].lookups !== undefined) {
            this.documentsearch.controls[this.documentClass[i].props[j].symName].patchValue(this.documentClass[i].props[j].lookups[0].value);
          }
        }
        this.docClassSymName = this.documentClass[i].symName;
        this.docClassName = this.documentClass[i].name;
        this.documentClassProp = [];
        this.documentClassProp = this.documentClass[i].props;
      }
    }
  }

  resetWorkType() {
    for (let i = 0; i < this.workTypeDropdown.length; i++) {
      if (this.workTypeDropdown[i].id === this.workTypeSearch.controls.workTypeName.value) {
        this.workTypeSearch.reset();
        this.workTypeSearch.patchValue({ workTypeName: this.workTypeDropdown[i].id });
        this.ss.getWorkTypeSearch(this.workTypeDropdown[i].id).subscribe(data => this.workTypeProp(data));
      }
    }
  }

  resetDocument() {
    for (let i = 0; i < this.documentClass.length; i++) {
      if (this.documentClass[i].id === this.documentsearch.controls.documentname.value) {
        this.documentsearch.reset();
        this.documentsearch.patchValue({ documentname: this.documentClass[i].id });
        this.ss.getWorkTypeSearch(this.documentClass[i].id).subscribe(data => this.workTypeProp(data));
      }
    }
  }
  createdDate(event) {
    this.createdDateSearch = event;
  }
  memoSubmit(memoSearch) {
    const search = {
      'sort': memoSearch.sort,
      'order': memoSearch.order,
      'pageNo': 1,
      'empNo': this.us.getCurrentUser().EmpNo,
      'properties': [
        { 'name': 'Type', 'oper': memoSearch.correspondenceTypeOper, 'value': memoSearch.corrspondenceType },
        { 'name': 'Sender', 'oper': memoSearch.fromOper, 'value': memoSearch.from },
        { 'name': 'CreatedOn', 'oper': memoSearch.createdDateOper, 'value': this.createdDateSearch },
        { 'name': 'Subject', 'oper': memoSearch.subjectOper, 'value': memoSearch.subject },
        { 'name': 'RefNo', 'oper': memoSearch.referenceIdOper, 'value': memoSearch.referenceId }
      ],
      'roleId': this.us.getCurrentUser().roles[0].id
    };
    memoSearch.referenceId = '';
    memoSearch.from = '';
    this.createdDateSearch = '';
    memoSearch.subject = '';
  }
  getSearchCorrespondence(data) {
    this.resultMemo = JSON.parse(data._body);
    if (this.resultMemo.length === 0) {
      this.searchCorrespondenceResultDiv = true;
    } else {
      this.searchCorrespondenceResultDiv = false;
    }
    this.isCorrespondenceResult = false;
    this.isCorrespondenceCollapse = true;
  }
  onDateDropdownChanged(event) {
    if (event.target.value === 'Between') {
      this.dateValue = true;
    } else {
      this.dateValue = false;
    }
  }
  onCorrespondenceDateChanged(event) {
    if (event.target.value === 'Between') {
      this.between = true;
    } else {
      this.between = false;
    }
  }
  clearCorrespondenceData(memoSearch) {
    memoSearch.referenceId = '';
    memoSearch.from = '';
    this.createdDateSearch = '';
    memoSearch.subject = '';
  }

  workTypeChange(event) {
    for (let i = 0; i < this.workTypeDropdown.length; i++) {
      if (this.workTypeDropdown[i].id.toString() === event.target.value) {
        this.workTypeSearch.reset();
        this.workTypeSearch.patchValue({ workTypeName: this.workTypeDropdown[i].id });
        this.ss.getWorkTypeSearch(this.workTypeDropdown[i].id).subscribe(data => this.workTypeProp(data));
      }
    }
  }
  workTypeSubmit(event) {
    let name = '';
    let symName = '';
    for (const selectField of [].slice.call(event.target.getElementsByTagName('select'))) {
      if (selectField.id !== undefined && selectField.id === 'docClass') {
        symName = selectField.options[selectField.options.selectedIndex].text;
        name = selectField.options[selectField.options.selectedIndex].value;
      }
    }

    // for (const selectField of [].slice.call(event.target.getElementsByTagName('select'))) {
    //   if (selectField.id !== undefined && selectField.id === 'docClass') {
    //      symName = this.docClassSymName;
    //      name = this.docClassName;
    //   }
    // }

    const search = {
      'type': name, 'properties': [],
      //  'contentSearch': {'name': 'Content', 'symName': 'CONTENT', 'dtype': 'STRING', 'mvalues': [], 'oper': ''}
    };
    for (const inputField of [].slice.call(event.target.getElementsByTagName('input'))) {
      for (const selectField of [].slice.call(event.target.getElementsByTagName('select'))) {
        if (inputField.getAttribute('type') !== 'file') {
          for (const docClass of this.workTypeDropdown) {
            for (const prop of this.workTypeProps) {
              if (symName === docClass.name) {
                if (inputField.id !== undefined && inputField.id === prop.symName && selectField.id !== undefined) {
                  const property = { 'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.value], 'oper': selectField.value };
                  search.properties.push(property);
                }
              }
            }
          }
        }
      }
    }

    for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
      for (const selectField of [].slice.call(event.target.getElementsByTagName('select'))) {
        if (inputField.getAttribute('type') !== 'file') {
          for (const docClass of this.workTypeDropdown) {
            for (const prop of this.workTypeProps) {
              if (symName === docClass.name) {
                if (inputField.id !== undefined && inputField.id === prop.symName && selectField.id !== undefined) {
                  const property = { 'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.children['0'].children['0'].value], 'oper': selectField.value };
                  search.properties.push(property);
                }
              }
            }
          }
        }
      }
    }

    for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
      for (const selectField of [].slice.call(event.target.getElementsByTagName('select'))) {
        if (inputField.getAttribute('type') !== 'file') {
          for (const docClass of this.workTypeDropdown) {
            for (const prop of this.workTypeProps) {
              if (symName === docClass.name) {
                if (inputField.id !== undefined && inputField.id === prop.symName && selectField.id !== undefined && selectField.id === prop.symName) {
                  const property = { 'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.children['0'].children['0'].value], 'oper': selectField.value };
                  search.properties.push(property);
                }
              }
            }
          }
        }
      }
    }
    for (const inputField of [].slice.call(event.target.getElementsByTagName('app-datepicker-range'))) {
      for (const selectField of [].slice.call(event.target.getElementsByTagName('select'))) {
        if (inputField.getAttribute('type') !== 'file') {
          for (const docClass of this.workTypeDropdown) {
            for (const prop of this.workTypeProps) {
              if (symName === docClass.name) {
                if (inputField.id !== undefined && inputField.id === prop.symName && selectField.id !== undefined && selectField.id === prop.symName) {
                  const property = { 'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.childNodes[0].children[0].children[0].children[0].value], 'oper': selectField.value };
                  search.properties.push(property);
                }
              }
            }
          }
        }
      }
    }
    for (const selectField of [].slice.call(event.target.getElementsByTagName('select'))) {
      for (const prop of this.workTypeProps) {
        if (prop.symName === 'CREATEDBYROLE') {
          let property;
          if (this.roleDropItems) {
            property = { 'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [this.roleDropItems.value], 'oper': selectField.value };
          } else {
            property = { 'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [''], 'oper': selectField.value };
          }
          search.properties.push(property);
        }
      }
    }
    this.workTypeSearchResult = [];
    this.workflowSearch = search;
    this.ws.searchWork(search).subscribe(data => this.getSearchWork(data));
    this.isWorkCollapse = true;
    this.isWorkResult = false;
  }

  paccordianLCicked() {
    if (this.tabIsDisabled) {
      // this.tr.warning('', 'Please close annotation tab to continue');
    } else {
      this.correspondenceindex = 0;

    }
  }
  paccordianResultclicked() {
    if (this.tabIsDisabled) {
      // this.tr.warning('', 'Please close annotation tab to continue');
    }
  }
  closedAnnotation(event) {
    if (event === true) {
      this.tabIsDisabled = true;
    } else {
      this.tabIsDisabled = false;
    }
  }
  getSearchWork(data) {
    this.listOfTaskActivities = [];
    if (data !== '' || data !== undefined) {
      this.listOfTaskActivities = data;
      this.correspondenceindex = 1;
    }
    if (this.listOfTaskActivities.length > 0) {
      this.workSearchResultDiv = false;
    } else {
      this.workSearchResultDiv = true;
    }
  }
  history(work) {
    this.ws.getWorkActivity(work.id).subscribe(data => { this.goToActivity(data); });
  }

  goToActivity(data) {
    const value = JSON.parse(data._body);
    this.router.navigate(['register/activity'], { queryParams: { 'readworkitem': 'readonly', 'workId': value } });
  }
  showworkhistory(data, history) {
    this.historydata = JSON.parse(data._body);
  }

  download(docId) {
    this.ds.downloadDocument(docId).subscribe(data => this.downloadCurrentDocument(data));
  }
  downloadCurrentDocument(data) {
    let filename = '';
    const disposition = data.headers.get('Content-Disposition');
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(disposition);
    if (matches != null && matches[1]) {
      filename = matches[1].replace(/['"]/g, '');
    }
    saveAs(data._body, filename);
  }
  attachment(work, attachments) {
    this.ws.getWorkAttachments(work.id).subscribe(data => this.showattachments(data, attachments));
  }
  showattachments(data, attachments) {
    this.attachmentres = JSON.parse(data._body);
    this.ngxSmartModalService.getModal('attachmentModel').open();
  }

  refreshComponent() {
    this.cs.searchDocuments(this.search).subscribe(data => this.getsearchedDocuments(data));
  }
  onSearch(evt) {
    const keyUp: String = evt.query;
    if (keyUp.length > 2) {
      this
        .us
        .searchRoles(keyUp)
        .subscribe(res => this.assinDBTypeAhead(res));
    }
  }
  assinDBTypeAhead(res) {
    this.createdRole = [];
    const result = JSON.parse(res._body);
    for (let index = 0; index < result.length; index++) {
      const element = {
        value: result[index].id,
        name: result[index].name
      };
      this.createdRole.push(element);
    }
  }
  selectDropOptions(event, classProp) {
    if (event.value === 'Between') {
      classProp.between = 'Between';
      for (let i = 0; i < this.documentClassProp.length; i++) {
        if (this.documentClassProp[i].id === classProp.id) {
          this.documentClassProp[i].between = 'Between';
          this.documentsearch.controls[this.documentClassProp[i].symName].patchValue('');
          break;
        } else {
        }
      }
    } else {
      for (let d = 0; d < this.documentClassProp.length; d++) {
        if (this.documentClassProp[d].id === classProp.id) {
          this.documentClassProp[d].between = 'equal';
          this.documentsearch.controls[this.documentClassProp[d].symName].patchValue('');
          break;
        } else {
        }
      }
    }
  }
  downloadAdExcelWorkflowSearched() {
    this.ws.searchWorkToExcel(this.workflowSearch).subscribe(data => { this.downloadCurrentDocument(data); });
  }
}
