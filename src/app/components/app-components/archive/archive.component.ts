import { Component, OnInit } from '@angular/core';
import { WorkService } from '../../../service/work.service';
import { UserService } from '../../../service/user.service';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { TranslateService } from '../../../../../node_modules/@ngx-translate/core';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.css']
})
export class ArchiveComponent implements OnInit {
public archiveList: any[];
  constructor(private ws: WorkService, private us: UserService, private breadcrumbeServece: BreadcrumbService, private translate: TranslateService) {
    this.breadcrumbeServece.setItems([
      {label: this.translate.instant('Archive')}
  ]);
  }

  ngOnInit() {
    this.ws.getArchiveItems(this.us.getCurrentUser().EmpNo, 1).subscribe(data => this.getArchibveResult(data));
  }
  getArchibveResult(data) {
    this.archiveList = JSON.parse(data._body);
  }
}
