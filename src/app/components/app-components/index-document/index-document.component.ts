import { Component, OnInit, OnChanges, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { ContentService } from '../../../service/content.service';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-index-document',
  templateUrl: './index-document.component.html',
  styleUrls: ['./index-document.component.css']
})
export class IndexDocumentComponent implements OnInit, OnChanges {
@Input() folderId;
@Output() documentAnnotate = new EventEmitter();
@Output() showPrintIs = new EventEmitter();
@Output() showPerticularAnnotation = new EventEmitter();
@Output() refresh = new EventEmitter();
@Output() docLinkEmitter = new EventEmitter();
@Input() reloadComponent;
public listOffilesInfolder = [];
public count = 0;
public documentActionsList = [];
public editProp: FormGroup;
public docProperty: any;
public docPropertyprops: any;
public docPropertyName: any;
public docisReserved: any;
public ddmmyy = true;
public docClass: any;
public documentClassDropdown = [];
public documentClass = [];
public toolBar = 'next';
public showEditProp = false;
public timestamp: any;
public changePage;
  constructor(private cs: ContentService, private translate: TranslateService, private fb: FormBuilder,
    private detect: ChangeDetectorRef, private spinner: Ng4LoadingSpinnerService, private tr: ToastrService) {
    if (localStorage.getItem('Date Format') === 'DD/MM/YYYY') {
      this.ddmmyy = true;
  }else {
      this.ddmmyy = false;
  }
  this.ddmmyy = true;
  }

  ngOnInit() {
    this.editProp = this.fb.group({
      docclass: [null, Validators.required]
    });
  }
ngOnChanges() {
// console.log(this.folderId);
this.cs.getFolderDocuments(this.folderId).subscribe(data => {this.getFolderDocuments(data); this.spinner.hide(); });

}
getFolderDocuments(data) {
  this.listOffilesInfolder = [];
  this.listOffilesInfolder = JSON.parse(data._body);
  if (this.listOffilesInfolder.length > 0) {
  this.showEditProp = true;
this.spinner.show();
  this.getOtherApi();
  }
}
getOtherApi() {
  this.spinner.show();
  // console.log('count ' + ' ' + this.count);
  this.cs.getDocument(this.listOffilesInfolder[this.count].id).subscribe(resData => {this.getPropertyDetail(resData, this.listOffilesInfolder[this.count].id); this.spinner.hide(); });
  this.cs.getDocumentActions(this.listOffilesInfolder[this.count].id).subscribe(result => this.getDocumentActions(result));

}
getDocumentActions(data) {
  this.documentActionsList = [];
  this.documentActionsList = JSON.parse(data._body);
}
previousClicked(value) {
  this.toolBar = value;
}
nextClicked(value) {
  this.toolBar = value;
}
submitedit(event, docPropName) {
  const params = {'id': this.listOffilesInfolder[this.count].id , 'docclass': this.docClass, 'props': []};
    for (const inputField of [].slice.call(event.target)) {
      if (inputField.getAttribute('type') !== 'file') {
          for (const prop of  this.docPropertyprops) {
              if (inputField.id !== undefined && inputField.id === prop.symName) {
                const property = {'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.value]};
                params.props.push(property);
              }
          }
      }
   }
   for (const inputField of [].slice.call(event.target.getElementsByTagName('p-dropdown'))) {
    if (inputField.getAttribute('type') !== 'file' && inputField.id !== 'docClassDropDowned') {
         for (const prop of this.docPropertyprops) {
              if (inputField.id !== undefined && inputField.id === prop.symName) {
                const property = {'symName': prop.symName, 'dtype': prop.dtype, 'mvalues':
                     [inputField.value] };
                   params.props.push(property);
           }
         }
    }
}
   for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
         if (inputField.getAttribute('type') !== 'file') {
              for (const prop of this.docPropertyprops) {
                   if (inputField.id !== undefined && inputField.id === prop.symName) {
                     const property = {'symName': prop.symName, 'dtype': prop.dtype, 'mvalues':
                          [inputField.children['0'].children['0'].value] };
                        params.props.push(property);
                }
              }
         }
   }
   this.spinner.show();
  this.cs.changeDocumentClass(this.listOffilesInfolder[this.count].id, this.docClass).subscribe(data => {}, error => {});
   this.cs.updateProperties(params).subscribe(res => {
    this.spinner.hide();
     this.detect.detectChanges();
    //  this.tr.success('', 'Update success');
     if (this.toolBar === 'next') {
      if (this.count < this.listOffilesInfolder.length - 1) {
       this.count = this.count + 1;
        this.getOtherApi();
      }
     } else {
       if (this.count > 0) {
        this.count = this.count - 1;
         this.getOtherApi();
       }
     }
  });

}
getPropertyDetail(data , docId) {
  this.detect.detectChanges();
  this.showEditProp = false;
  this.editProp.reset({});
  setTimeout(() => {
    if (data._body  !== '' ) {
    this.showEditProp = true;
    this.editProp = this.fb.group({
      docclass: [null, Validators.required]
    });
      this.docProperty = [];
      this.docPropertyprops = [];
      this.docProperty = JSON.parse(data._body);
      this.docPropertyprops = this.docProperty.props;
      this.docPropertyName = this.docProperty.className;
      this.docisReserved = this.docProperty.isReserved;
      this.docClass = this.docProperty.docclass;
      this.cs.getDocumentClasses().subscribe(classdata => this.documentClasses(classdata) );
      this.editProp.patchValue({docclass: this.docClass});
            for (const props of this.docPropertyprops){
              if (props.dtype === 'DATE') {
                if ( props.req === 'TRUE' || props.req === 'true' ) {
                  let control: FormControl ;
                  const formControlNamed = props.symName;
                  // if (props.mvalues[0] !== undefined) {
                  //   control = new FormControl(null, Validators.required);
                  // } else {
                  //   control = new FormControl(null, Validators.required);
                  // }
                  // this.editProp.addControl(props.symName, control);
                  let dateprops: any;
                  if (props.mvalues[0] !== undefined) {
                    const dateValue = props.mvalues[0];
                    dateprops = dateValue.split(' ')[0];
                    control = new FormControl(dateprops, Validators.required);
                  this.editProp.addControl(props.symName, control);
                  this.editProp.controls[formControlNamed].patchValue(props.mvalues[0]);

                  }else {
                    dateprops = null;
                    control = new FormControl(null, Validators.required);
                  this.editProp.addControl(props.symName, control);
                  this.editProp.controls[formControlNamed].patchValue('');

                  }

                }else {
                  let control: FormControl ;
                  let dateprops: any;
                  const formControlNamed = props.symName;
                  if (props.mvalues[0] !== undefined) {
                    const dateValue = props.mvalues[0];
                    dateprops = dateValue.split(' ')[0];
                    control = new FormControl(dateprops, Validators.required);
                  this.editProp.addControl(props.symName, control);
                  this.editProp.controls[formControlNamed].patchValue(props.mvalues[0]);
                  } else {
                    control = new FormControl(null, Validators.required);
                  this.editProp.addControl(props.symName, control);
                  this.editProp.controls[formControlNamed].patchValue('');
                  }
                }
              }else {
                if ( props.req === 'TRUE' || props.req === 'true') {
                  let control: FormControl ;
                  if (props.mvalues[0] !== undefined) {
                    control = new FormControl(props.mvalues[0], Validators.required);
                    this.editProp.addControl(props.symName, control);
                    const formControlNamed = props.symName;
                    this.editProp.controls[formControlNamed].patchValue(props.mvalues[0]);
                  } else {
                    control = new FormControl(null, Validators.required);
                    this.editProp.addControl(props.symName, control);
                    const formControlNamed = props.symName;
                    this.editProp.controls[formControlNamed].patchValue('');
                  }

                }else {
                  let control: FormControl ;
                  const formControlNamed = props.symName;
                  if (props.mvalues[0] !== undefined) {
                    control = new FormControl(props.mvalues[0]);
                  this.editProp.addControl(props.symName, control);
                  this.editProp.controls[formControlNamed].patchValue(props.mvalues[0]);
                  } else {
                    control = new FormControl(null);
                  this.editProp.addControl(props.symName, control);
                    this.editProp.controls[formControlNamed].patchValue('');

                  }
                }
              }
            }
    // this.propertyEdit = false;
    }
  }, 100);

}
documentClassChanged(event) {
  this.editProp.patchValue({docClass: this.docClass});
    for (const prop of this.documentClass) {
      if (prop.symName === this.docClass) {
        this.docPropertyprops = prop.props;
      }
    }
      for (let index = 0; index < this.docPropertyprops.length; index++) {
        if (this.docPropertyprops[index].lookups === undefined) {
          if (this.docPropertyprops[index].req === 'TRUE' || this.docPropertyprops[index].req === 'true') {
              let control: FormControl ;
            if (this.docPropertyprops[index].mvalues[0] !== undefined) {
              control = new FormControl(this.docPropertyprops[index].mvalues[0], Validators.required);
            } else {
              control = new FormControl(null, Validators.required);
            }
            this.editProp.addControl(this.docPropertyprops[index].symName, control);
            // this.bulkUpload.addControl(this.documentClassProp[index].symName, control);
          }else {
            let control: FormControl ;
            if (this.docPropertyprops[index].mvalues[0] !== undefined) {
              control = new FormControl(this.docPropertyprops[index].mvalues[0]);
            } else {
              control = new FormControl(null);
            }
            this.editProp.addControl(this.docPropertyprops[index].symName, control);
            // this.bulkUpload.addControl(this.documentClassProp[index].symName, control);
          }
        }else if (this.docPropertyprops[index].lookups !== undefined) {
          let control: FormControl ;
          if (this.docPropertyprops[index].mvalues[0] !== undefined) {
            control = new FormControl(this.docPropertyprops[index].mvalues[0].value);
          } else {
            control = new FormControl(this.docPropertyprops[index].lookups[0].value);
          }
          this.editProp.addControl(this.docPropertyprops[index].symName, control);
          // this.bulkUpload.addControl(this.documentClassProp[index].symName, control);
        }else {

        }
     }

  }
  documentClasses(data) {
    if (data._body !== '') {
      this.documentClassDropdown = [];
      this.documentClass = JSON.parse(data._body);
      for (let index = 0; index < this.documentClass.length; index++) {
        this.documentClassDropdown.push({label: this.translate.instant(this.documentClass[index].name), value: this.documentClass[index].symName});
      }
    }
  }
  documentClicked(event) {
    // let isReadonly;
    // this.docId = event.docId;
    // this.curPage = event.pagenumber;
    this.documentAnnotate.emit(event);
  }
  showPrint(event) {
    if (event) {
      // this
      // .ds
      // .downloadPrintDocument(event)
      // .subscribe(data => this.downloadPrintDocument(data));
      this.showPrintIs .emit(event);
    }
  }
  annotaionEmmitted(event) {
    this.showPerticularAnnotation.emit(event);
  }
  changePageOfDocument(click) {
    const date = new Date();
    this.changePage = [ click , date.getTime];
  }
}
