import { Component, OnInit } from '@angular/core';
import { AdminLayoutComponent } from '../admin-layout/admin-layout.component';
import { UserService } from '../../../../service/user.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { TranslateService } from '../../../../../../node_modules/@ngx-translate/core';
import { BreadcrumbService } from '../breadcrumb/breadcrumb.service';
import { ConfigurationService } from '../../../../service/Configuration.service';
import { DocumentService } from '../../../../service/document.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  public currentUser: any;
  public searchForm: FormGroup;
  public switchChecked = false;
  public toggleValue = 'Correspondence';
  public previewLogo = '';
  public placeholderText = this.translate.instant('Workflow');
  public display: boolean;
  public taskCounted: any;
  constructor(public app: AdminLayoutComponent, private us: UserService, private cs: ConfigurationService, private ds: DocumentService,
    private router: Router, private fb: FormBuilder, private location: Location, private translate: TranslateService, private breadcrumbeServece: BreadcrumbService) {
    this.currentUser = this.us.getCurrentUser();
    this.searchForm = this.fb.group({
      searchtext: [null, Validators.compose([Validators.required])]
    });
    const browserLang: string = translate.getBrowserLang();
    translate.use('en');
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
  }

  ngOnInit() {
    if (localStorage.getItem('Default Search') === 'DOCUMENTS') {
      this.switchChecked = true;
      this.toggleValue = 'Document';
      this.placeholderText = this.translate.instant('Document');
    } else {
      this.toggleValue = 'Correspondence';
      this.placeholderText = this.translate.instant('Workflow');
    }
    const url = this.location.prepareExternalUrl(this.location.path());
    if (url.includes('advancedsearch')) {
      this.router.navigate(['/advancedsearch'], { queryParams: { 'seachTypeseachType': this.toggleValue } });
    } else if (url.includes('search')) {
      // if (this.searchForm.valid) {
      // this.router.navigateByUrl('/search/' + this.searchForm.controls.searchData.value + '/'+ this.toggleValue);
      this.router.navigate(['/search'], { queryParams: { 'id': this.searchForm.controls.searchtext.value, 'type': this.toggleValue } });
      // }
    } else {

    }
    this.us.getAlertsByStatus(1).subscribe(data => { this.newTasksResult(data); }, error => { });
    Observable.interval(1000 * 60).subscribe(x => {
      this.us.getAlertsByStatus(1).subscribe(data => { this.newTasksResult(data); }, error => { });
    });
  }

  newTasksResult(data) {
    this.taskCounted = JSON.parse(data._body);
  }

  logOutUser() {
    localStorage.clear();
    this.router.navigateByUrl('/');
    setTimeout(() => {
      window.location.reload();
    }, 10);
    this.us.logoutUser().subscribe(data => {

    }, error => { });


  }

  searchFormSubmit(searchForm) {
    if (this.searchForm.valid) {
      const searchString = this.searchForm.controls.searchtext.value;
      this.router.navigate(['/search'], { queryParams: { 'id': searchString, 'type': this.toggleValue } });
    }
  }

  togglingChanged(event) {
  }

  advancedSearchclicked() {
    this.router.navigate(['/advancedsearch'], { queryParams: { 'seachType': this.toggleValue } });
  }
  toggle(event) {
    if (event.checked === true) {
      this.toggleValue = 'Document';
      this.placeholderText = this.translate.instant('Document');
    } else {
      this.toggleValue = 'Correspondence';
      this.placeholderText = this.translate.instant('Workflow');
    }
    setTimeout(() => {
      const url = this.location.prepareExternalUrl(this.location.path());
      // this.docSearch = url.split('/');
      if (url.includes('advancedsearch')) {
        this.router.navigate(['/advancedsearch'], { queryParams: { 'seachType': this.toggleValue } });
      } else if (url.includes('search')) {
        if (this.searchForm.valid) {
          // this.router.navigateByUrl('/search/' + this.searchForm.controls.searchData.value + '/'+ this.toggleValue);
          this.router.navigate(['/search'], { queryParams: { 'id': this.searchForm.controls.searchtext.value, 'type': this.toggleValue } });
        }
      } else {

      }
    }, 0);
  }
  settings() {
    this.router.navigate(['/settings']);
  }
  userHistory() {
    this.router.navigate(['/user-history']);
  }
  clickedOnNotifications() {
    if (this.taskCounted.length > 0) {
      this.display = true;
    } else {
      this.display = false;
    }
  }
  openTask(work) {
    this.display = false;
    this.us.updateAlertStatus(work.id, 2).subscribe(data => { this.callAlertsApi(data, work); }, error => { });
  }
  callAlertsApi(data, work) {
    this.router.navigate(['/inbox/activity'], { queryParams: { 'readworkitem': 'readworkitem', 'workId': work.message.longId, 'typeFormat': 'Inbox' } });
    this.us.getAlertsByStatus(1).subscribe(dataRes => { this.newTasksResult(dataRes); }, error => { });
  }
}
