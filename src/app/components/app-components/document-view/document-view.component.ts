import { Component, OnInit, OnChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-document-view',
  templateUrl: './document-view.component.html',
  styleUrls: ['./document-view.component.css']
})
export class DocumentViewComponent implements OnInit, OnChanges {

  public repoId;
  public folderId;
  public repoName;

  constructor(private routers: Router, public router: ActivatedRoute , private bs: BreadcrumbService, private translate: TranslateService) {
    this.bs.setItems([
      {label: this.translate.instant('Documents')}
  ]);
    this.router.queryParams.subscribe(params => {
      this.repoId = params['repoId'];
      this.repoName = params['repoName'];
      this.folderId = params['folderId'];
    });
  }

  ngOnInit() {
    this.router.queryParams.subscribe(params => {
      this.repoId = params['repoId'];
      this.repoName = params['repoName'];
      this.folderId = params['folderId'];
    });
  }

  ngOnChanges() {
    this.router.queryParams.subscribe(params => {
      this.repoId = params['rep0Id'];
      this.repoName = params['repoName'];
      this.folderId = params['folderId'];
    });
  }

}
