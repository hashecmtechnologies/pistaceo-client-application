import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { WorkService } from '../../../service/work.service';
import { UserService } from '../../../service/user.service';
import { OrgWorkItms } from '../../../models/orgworkitems.model';
import { AdministrationService } from '../../../service/Administration.service';
import { SubLevelOrg } from '../../../models/sublevelorg.model';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  public work;
  public activity;
  public individualWorkGraph = false;
  public orgActivityShow = false;
  public individualActivityGraph;
  public workDatePickerDropdown = [];
  public activityDatePickerDropdown = [];
  public workTypeDropdown = [];
  public activitytypeDropdown = [];
  public userworkItems;
  public showorg = true;
  public workdatetype = 'WEEK';
  public worktype = 'STATUS';
  public activitydatetype = 'WEEK';
  public activitytypeval = 'STATUS';
  public orgworkdatetype = 'ALL';
  public orgworktype = 'STATUS';
  public orgactivedatetype = 'ALL';
  public orgactivetype = 'STATUS';
  public orgWorkOrg = this.us.getCurrentUser().orgID;
  public orgActivityOrg = this.us.getCurrentUser().orgID;
  public dropdownforworktype: any;
  public orgactivetable;
  public workDatePickerSelected = 'ALL';
  public workTypeSelected = 'STATUS';
  public activityDatePickerSelected = 'ALL';
  public activitytypeSelected = 'STATUS';
  public datetype;
  public belowOrg = [];
  public currentorg = this.us.getCurrentUser().orgID;
  public currentorgactivity = this.us.getCurrentUser().orgID;
  public orgWorkDatePickerDropdown = [];
  public orgWOrkTypeDropdown = [];
  public orgActivityDatePickerDropdown = [];
  public orgActivitytypeDropdown = [];
  public orgWrk;
  public orgActivit;
  public orgActivityBelowSelected = 1;
  public orgWorkShow = false;
  public orgWorkDatePickerSelected = 'ALL';
  public orgWorkTypeSelected = 'STATUS';
  public orgactivityDatePickerSelected = 'ALL';
  public orgactivitytypeSelected = 'STATUS';
  public orgWorkBelowOrgSelected = 1;
  public selectedTab = 0;
  public activityValues;
  public historydata;
  public orgHistroyModel = false;
  public showorgDiv = false;
  public clickOrgWork = [];
  public orgActivtyClicked = [];
  public selectedTableTab = 0;
  public individualWorkItems = [];
  public selectedUSerTab = 0;
  public IndividualactivityItems = [];
  public individualActivityData = [];
  public defaultLegendClickHandler;
  public userWorkModel: any;
  public selectedLanguage: any;
  constructor(private breadcrumbService: BreadcrumbService, private ws: WorkService,
    private us: UserService, private changeDetectorRef: ChangeDetectorRef, private router: Router,
    private translate: TranslateService, private as: AdministrationService) {
    // this.breadcrumbService.setItems([
    //   { label: this.translate.instant('Dashboard') }
    // ]);
    this.workDatePickerSelected = 'WEEK';
    this.activityDatePickerSelected = 'WEEK';
    this.ws.getWorkStatistics(this.us.getCurrentUser().EmpNo, 'WEEK', 'STATUS').subscribe(data => this.individualWork(data));
    this.ws.getActivityItems('WEEK', 'ALL').subscribe(data => this.getUserActivities(data));
    this.ws.getActivityStatistics(this.us.getCurrentUser().EmpNo, 'WEEK', 'STATUS').subscribe(data => this.individualActivity(data));
    this.ws.getUserWorkItemsStatus('WEEK', 'ALL').subscribe(data => this.getUserWorkItems(data));
    this.ws.getOrgWorkStatistics(this.us.getCurrentUser().orgID, 'ALL', 'STATUS').subscribe(data => this.orgwork(data));
    this.ws.getOrgActivityStatistics(this.us.getCurrentUser().orgID, 'ALL', 'STATUS').subscribe(data => this.orgactivity(data));

    this.workDatePickerDropdown = [
      { label: this.translate.instant('All'), value: 'ALL' },
      { label: this.translate.instant('Today'), value: 'TODAY' },
      { label: this.translate.instant('Week'), value: 'WEEK' },
      { label: this.translate.instant('Month'), value: 'MONTH' },
      { label: this.translate.instant('Year'), value: 'YEAR' }
    ];
    this.workTypeDropdown = [
      { label: this.translate.instant('Status'), value: 'STATUS' },
      { label: this.translate.instant('Type'), value: 'TYPE' },
    ];

    this.activityDatePickerDropdown = [
      { label: this.translate.instant('All'), value: 'ALL' },
      { label: this.translate.instant('Today'), value: 'TODAY' },
      { label: this.translate.instant('Week'), value: 'WEEK' },
      { label: this.translate.instant('Month'), value: 'MONTH' },
      { label: this.translate.instant('Year'), value: 'YEAR' },
    ];

    this.activitytypeDropdown = [
      { label: this.translate.instant('Status'), value: 'STATUS' },
      { label: this.translate.instant('Type'), value: 'TYPE' },
    ];

    this.orgWorkDatePickerDropdown = [
      { label: this.translate.instant('All'), value: 'ALL' },
      { label: this.translate.instant('Today'), value: 'TODAY' },
      { label: this.translate.instant('Week'), value: 'WEEK' },
      { label: this.translate.instant('Month'), value: 'MONTH' },
      { label: this.translate.instant('Year'), value: 'YEAR' },
    ];

    this.orgWOrkTypeDropdown = [
      { label: this.translate.instant('Status'), value: 'STATUS' },
      { label: this.translate.instant('Type'), value: 'TYPE' },
    ];

    this.orgActivityDatePickerDropdown = [
      { label: this.translate.instant('All'), value: 'ALL' },
      { label: this.translate.instant('Today'), value: 'TODAY' },
      { label: this.translate.instant('Week'), value: 'WEEK' },
      { label: this.translate.instant('Month'), value: 'MONTH' },
      { label: this.translate.instant('Year'), value: 'YEAR' },
    ];

    this.orgActivitytypeDropdown = [
      { label: this.translate.instant('Status'), value: 'STATUS' },
      { label: this.translate.instant('Type'), value: 'TYPE' },
    ];

    // this.work = {
    //     labels: [],
    //     datasets: [
    //         {
    //             data: [],
    //             backgroundColor: [
    //                 '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
    //             ],
    //             hoverBackgroundColor: [
    //                 '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
    //             ]
    //         }]
    //     };


    // this.orgWrk = {
    //     labels: [],
    //     datasets: [
    //         {
    //             data: [],
    //             backgroundColor: [
    //                 '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
    //             ],
    //             hoverBackgroundColor: [
    //                 '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
    //             ]
    //         }]
    //     };

    //     this.orgActivit = {
    //         labels: [],
    //         datasets: [
    //             {
    //                 data: [],
    //                 backgroundColor: [
    //                     '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
    //                 ],
    //                 hoverBackgroundColor: [
    //                     '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
    //                 ]
    //             }]
    //         };

    // this.activity = {
    //     labels: [],
    //     datasets: [
    //         {
    //             data: [],
    //             backgroundColor: [
    //                 '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
    //             ],
    //             hoverBackgroundColor: [
    //                 '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
    //             ]
    //         }]
    //     };
  }

  ngOnInit() {
    this.as.getAllSubLevelOrgUnits(this.us.getCurrentUser().orgID).subscribe(data => this.organationsIds(data));
    this.selectedLanguage = localStorage.getItem('Default Language');
    this.workDatePickerDropdown = [
      { label: this.translate.instant('All'), value: 'ALL' },
      { label: this.translate.instant('Today'), value: 'TODAY' },
      { label: this.translate.instant('Week'), value: 'WEEK' },
      { label: this.translate.instant('Month'), value: 'MONTH' },
      { label: this.translate.instant('Year'), value: 'YEAR' }
    ];
    this.workTypeDropdown = [
      { label: this.translate.instant('Status'), value: 'STATUS' },
      { label: this.translate.instant('Type'), value: 'TYPE' },
    ];

    this.activityDatePickerDropdown = [
      { label: this.translate.instant('All'), value: 'ALL' },
      { label: this.translate.instant('Today'), value: 'TODAY' },
      { label: this.translate.instant('Week'), value: 'WEEK' },
      { label: this.translate.instant('Month'), value: 'MONTH' },
      { label: this.translate.instant('Year'), value: 'YEAR' },
    ];

    this.activitytypeDropdown = [
      { label: this.translate.instant('Status'), value: 'STATUS' },
      { label: this.translate.instant('Type'), value: 'TYPE' },
    ];

    this.orgWorkDatePickerDropdown = [
      { label: this.translate.instant('All'), value: 'ALL' },
      { label: this.translate.instant('Today'), value: 'TODAY' },
      { label: this.translate.instant('Week'), value: 'WEEK' },
      { label: this.translate.instant('Month'), value: 'MONTH' },
      { label: this.translate.instant('Year'), value: 'YEAR' },
    ];

    this.orgWOrkTypeDropdown = [
      { label: this.translate.instant('Status'), value: 'STATUS' },
      { label: this.translate.instant('Type'), value: 'TYPE' },
    ];

    this.orgActivityDatePickerDropdown = [
      { label: this.translate.instant('All'), value: 'ALL' },
      { label: this.translate.instant('Today'), value: 'TODAY' },
      { label: this.translate.instant('Week'), value: 'WEEK' },
      { label: this.translate.instant('Month'), value: 'MONTH' },
      { label: this.translate.instant('Year'), value: 'YEAR' },
    ];

    this.orgActivitytypeDropdown = [
      { label: this.translate.instant('Status'), value: 'STATUS' },
      { label: this.translate.instant('Type'), value: 'TYPE' },
    ];

  }

  ngAfterViewInit() {
    if (this.us.getCurrentUser().headof.length !== 0) {
      if (this.us.getCurrentUser().orgID === this.us.getCurrentUser().headof[0].id) {
        this.showorgDiv = true;
        this.selectedTab = 1;
        this.changeDetectorRef.detectChanges();
        this.ws.getOrgWorkItems(this.us.getCurrentUser().orgID, this.orgactivedatetype,
          'ALL').subscribe(data => this.getorgworkdata(data));
        this.ws.getOrgActivityItems(this.us.getCurrentUser().orgID, this.orgworkdatetype,
          'ALL').subscribe(data => this.orgactivitydata(data));
      } else {
        this.showorgDiv = false;
      }
    }
  }
  organationsIds(data) {
    const belowOrg = JSON.parse(data._body);
    this.belowOrg = [];
    for (let index = 0; index < belowOrg.length; index++) {
      this.belowOrg.push({ label: belowOrg[index].name, value: belowOrg[index].id });
    }
    this.currentorg = this.us.getCurrentUser().orgID;
    this.currentorgactivity = this.us.getCurrentUser().orgID;
  }

  individualWork(data) {
    this.work = {
      labels: [],
      options: {
        legend: {
          legendCallback: function (e, legendItem) {
            // console.log(legendItem);
            // return true;
          },
          //  onClick: this.newLegendClickHandler
        }
      },
      datasets: [
        {
          data: [],
          backgroundColor: [
            '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
          ],
          hoverBackgroundColor: [
            '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
          ]
        }],
    };
    const weekItem = JSON.parse(data._body);
    this.individualWorkItems = [];
    this.individualWorkItems = JSON.parse(data._body);
    for (let i = 0; i < weekItem.length; i++) {
      if (weekItem[i].name === undefined) {
      } else {
        this.work.labels.push(weekItem[i].name);
        this.work.datasets[0].data.push(weekItem[i].count);
      }
    }
    if (this.work.labels.length > 0) {
      this.individualWorkGraph = true;
    }
  }
  newLegendClickHandler(e, legendItem) {
    console.log(legendItem.index);
    e.preventDefault();

    // original.call(this, e, legendItem);
    // if (index > 1) {
    //     // Do the original logic
    //     this.defaultLegendClickHandler(e, legendItem);
    // } else {
    //     const ci = this.chart;
    //     [
    //         ci.getDatasetMeta(0),
    //         ci.getDatasetMeta(1)
    //     ].forEach(function(meta) {
    //         meta.hidden = meta.hidden === null ? !ci.data.datasets[index].hidden : null;
    //     });
    //     ci.update();
    // }
  }
  individualActivity(data) {
    this.activity = {
      labels: [],
      datasets: [
        {
          data: [],
          backgroundColor: [
            '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
          ],
          hoverBackgroundColor: [
            '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
          ]
        }]
    };
    const todayItem = JSON.parse(data._body);
    this.individualActivityData = [];
    this.individualActivityData = JSON.parse(data._body);
    for (let i = 0; i < todayItem.length; i++) {
      if (todayItem[i].name === undefined) {
        this.individualWorkGraph = false;
        break;
      } else {
        this.activity.labels.push(todayItem[i].name);
        this.activity.datasets[0].data.push(todayItem[i].count);
      }
    }
    if (this.activity.labels.length > 0) {
      this.individualActivityGraph = true;
    }
  }

  getUserWorkItems(data) {
    this.userworkItems = [];
    this.userworkItems = JSON.parse(data._body);
  }

  getorgworkdata(data) {
    this.dropdownforworktype = [];
    this.dropdownforworktype = JSON.parse(data._body);
  }

  orgactivitydata(data) {
    this.orgactivetable = [];
    this.orgactivetable = JSON.parse(data._body);
  }

  workdatetypeChanged() {
    this.workdatetype = this.workDatePickerSelected;
    this.ws.getWorkStatistics(this.us.getCurrentUser().EmpNo, this.workdatetype,
      this.workTypeSelected).subscribe(data => this.individualWork(data));
    this.selectedUSerTab = 0;
    if (this.workTypeSelected === 'TYPE') {
      this.ws.getUserWorkItemsType(this.workDatePickerSelected, this.workTypeSelected).subscribe(data => this.getUserWorkItems(data));
    } else {
      this.ws.getUserWorkItemsStatus(this.workDatePickerSelected, 'ALL').subscribe(data => this.getUserWorkItems(data));

    }
  }

  worktypeChanged() {
    this.worktype = this.workTypeSelected;
    this.ws.getWorkStatistics(this.us.getCurrentUser().EmpNo, this.workdatetype,
      this.workTypeSelected).subscribe(data => this.individualWork(data));
    this.selectedUSerTab = 0;
    if (this.workTypeSelected === 'TYPE') {
      this.ws.getUserWorkItemsType(this.workDatePickerSelected, this.workTypeSelected).subscribe(data => this.getUserWorkItems(data));
    } else {
      this.ws.getUserWorkItemsStatus(this.workDatePickerSelected, 'ALL').subscribe(data => this.getUserWorkItems(data));

    }
  }

  activitydatetypeChanged() {
    this.datetype = this.activityDatePickerSelected;
    this.ws.getActivityStatistics(this.us.getCurrentUser().EmpNo, this.activityDatePickerSelected,
      this.activitytypeSelected).subscribe(data => this.individualActivity(data));
    this.selectedUSerTab = 1;
    if (this.activitytypeSelected === 'TYPE') {
      this.ws.getActivityItemsType(this.activityDatePickerSelected, this.activitytypeSelected).subscribe(data => this.getUserActivities(data));
    } else {
      this.ws.getActivityItems(this.activityDatePickerSelected, 'ALL').subscribe(data => this.getUserActivities(data));

    }
  }

  activitytypeChnaged() {
    this.ws.getActivityStatistics(this.us.getCurrentUser().EmpNo, this.activityDatePickerSelected,
      this.activitytypeSelected).subscribe(data => this.individualActivity(data));
    this.selectedUSerTab = 1;
    if (this.activitytypeSelected === 'TYPE') {
      this.ws.getActivityItemsType(this.activityDatePickerSelected, this.activitytypeSelected).subscribe(data => this.getUserActivities(data));
    } else {
      this.ws.getActivityItems(this.activityDatePickerSelected, 'ALL').subscribe(data => this.getUserActivities(data));

    }

  }

  orgwork(data) {
    this.clickOrgWork = [];
    this.orgWrk = {
      labels: [],
      datasets: [
        {
          data: [],
          backgroundColor: [
            '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
          ],
          hoverBackgroundColor: [
            '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
          ]
        }]
    };
    const orgwk = JSON.parse(data._body);
    for (let i = 0; i < orgwk.length; i++) {
      if (orgwk[i].name === undefined) {
        this.orgWorkShow = false;
      } else {
        this.orgWrk.labels.push(this.translate.instant(orgwk[i].name));
        this.orgWrk.datasets[0].data.push(orgwk[i].count);
      }
    }
    if (this.orgWrk.labels.length > 0) {
      this.clickOrgWork = JSON.parse(data._body);
      this.orgWorkShow = true;
    } else {
      this.orgWorkShow = false;
    }
  }

  orgactivity(data) {
    this.orgActivtyClicked = [];
    this.orgActivit = {
      labels: [],
      datasets: [
        {
          data: [],
          backgroundColor: [
            '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
          ],
          hoverBackgroundColor: [
            '#00858d', '#00C292', '#002060', '#246275', '#2BC5E0', '#FF3A3A', '#49E84E', '#E7632C'
          ]
        }]
    };
    const orgact = JSON.parse(data._body);
    for (let i = 0; i < orgact.length; i++) {
      if (orgact[i].name === undefined) {
        this.orgActivityShow = false;
      } else {
        this.orgActivit.labels.push(this.translate.instant(orgact[i].name));
        this.orgActivit.datasets[0].data.push(orgact[i].count);
      }
    }
    if (this.orgActivit.labels.length > 0) {
      this.orgActivtyClicked = JSON.parse(data._body);
      this.orgActivityShow = true;
    } else {
      this.orgActivityShow = false;
    }
  }

  orgWorkChanged() {
    this.ws.getOrgWorkStatistics(this.orgWorkBelowOrgSelected, this.orgWorkDatePickerSelected,
      this.orgWorkTypeSelected).subscribe(data => this.orgwork(data));
    this.selectedTableTab = 0;
    if (this.orgWorkTypeSelected === 'TYPE') {
      this.ws.getOrgWorkItemsType(this.orgWorkBelowOrgSelected, this.orgWorkDatePickerSelected, this.orgWorkTypeSelected).subscribe(data => this.getorgworkdata(data));
    } else {
      this.ws.getOrgWorkItems(this.orgWorkBelowOrgSelected, this.orgWorkDatePickerSelected, 'ALL').subscribe(data => this.getorgworkdata(data));
    }
  }

  orgActivityChanged() {
    this.ws.getOrgActivityStatistics(this.orgActivityBelowSelected, this.orgactivityDatePickerSelected,
      this.orgactivitytypeSelected).subscribe(data => this.orgactivity(data));
    this.selectedTableTab = 1;
    if (this.orgactivitytypeSelected === 'TYPE') {
      this.ws.getOrgAcitityItemsType(this.orgActivityBelowSelected, this.orgactivityDatePickerSelected, this.orgactivitytypeSelected).subscribe(data => this.orgactivitydata(data));
    } else {
      this.ws.getOrgActivityItems(this.orgActivityBelowSelected, this.orgactivityDatePickerSelected, 'ALL').subscribe(data => this.orgactivitydata(data));
    }


  }

  onTabChange($event) {
    this.changeDetectorRef.detectChanges();
  }

  individualWorkClicked(event) {
    this.ws.getWorkActivity(event.data.id).subscribe(data => { this.goToActivity(data); });
  }
  individualActivityClicked(event) {
    if (event.data.status === 'DRAFT') {
      this.router.navigate(['draft/activity'], { queryParams: { 'readworkitem': 'readonly', 'workId': event.data.id, 'recall': 'false' } });
    } else if (event.data.status === 'NEW' || event.data.status === 'READ') {
      this.router.navigate(['inbox/activity'], { queryParams: { 'readworkitem': 'readonly', 'workId': event.data.id, 'recall': 'false' } });
    } else if (event.data.status !== 'NEW' && event.data.status !== 'READ' && event.data.status !== 'DRAFT') {
      this.router.navigate(['sent/activity'], { queryParams: { 'readworkitem': 'readonly', 'workId': event.data.id, 'workAssignedTo': event.data.assignedTo, 'workAssignedToName': event.data.assignedToName, 'recall': 'false' } });
    } else {
      this.router.navigate(['sent/activity'], { queryParams: { 'readworkitem': 'readonly', 'workId': event.data.id, 'workAssignedTo': event.data.assignedTo, 'workAssignedToName': event.data.assignedToName, 'recall': 'false' } });
    }
  }
  goToActivity(data) {
    const value = JSON.parse(data._body);
    this.ws.getActivityInfo(value).subscribe(datares => this.getactivityinfo(datares), error => (error));
  }
  getactivityinfo(data) {
    this.activityValues = JSON.parse(data._body);
    if (this.activityValues.status === 'DRAFT') {
      if (this.activityValues.createdBy === this.us.getCurrentUser().roles[0].id) {
        this.router.navigate(['draft/activity'], { queryParams: { 'readworkitem': 'readonly', 'workId': this.activityValues.id, 'recall': 'false' } });
      } else {
        //  this.router.navigate(['draft']);
        this.router.navigate(['draft/activity'], { queryParams: { 'readworkitem': 'readonly', 'workId': this.activityValues.id, 'recall': 'false' } });

      }
    } else if (this.activityValues.status === 'NEW' || this.activityValues.status === 'READ') {
      if (this.activityValues.assignedTo === this.us.getCurrentUser().roles[0].id) {
        this.router.navigate(['inbox/activity'], { queryParams: { 'readworkitem': 'readonly', 'workId': this.activityValues.id, 'recall': 'false' } });

      } else {
        //  this.router.navigate(['inbox']);
        this.router.navigate(['inbox/activity'], { queryParams: { 'readworkitem': 'readonly', 'workId': this.activityValues.id, 'recall': 'false' } });

      }
    } else if (this.activityValues.status !== 'NEW' && this.activityValues.status !== 'READ' && this.activityValues.status !== 'DRAFT') {
      if (this.activityValues.createdBy === this.us.getCurrentUser().roles[0].id) {
        // this.router.navigate(['draft/activity'] , {queryParams: {'readworkitem': 'readworkitem', workId: this.activityValues.id}});
        this.router.navigate(['sent/activity'], { queryParams: { 'readworkitem': 'readonly', 'workId': this.activityValues.id, 'workAssignedTo': this.activityValues.assignedTo, 'workAssignedToName': this.activityValues.assignedToName, 'recall': 'false' } });
      } else {
        //  this.router.navigate(['sent']);
        this.router.navigate(['sent/activity'], { queryParams: { 'readworkitem': 'readonly', 'workId': this.activityValues.id, 'workAssignedTo': this.activityValues.assignedTo, 'workAssignedToName': this.activityValues.assignedToName, 'recall': 'false' } });

      }
    }
  }
  orgWorkHistory(all) {
    this.ws.getWorkHistory(all.data.id).subscribe(data => this.showworkhistory(data));
  }
  showworkhistory(data) {
    this.historydata = JSON.parse(data._body);
    this.orgHistroyModel = true;
  }
  getUserActivities(data) {
    this.IndividualactivityItems = [];
    this.IndividualactivityItems = JSON.parse(data._body);
  }
  orgworkclick(event) {
    for (let amt = 0; amt < this.clickOrgWork.length; amt++) {
      if (event.element._index === amt) {
        this.selectedTableTab = 0;
        if (this.orgWorkTypeSelected === 'TYPE') {
          this.ws.getOrgWorkItemsType(this.orgWorkBelowOrgSelected, this.orgWorkDatePickerSelected, this.clickOrgWork[amt].typeId).subscribe(data => this.getorgworkdata(data));
        } else {
          this.ws.getOrgWorkItems(this.orgWorkBelowOrgSelected, this.orgWorkDatePickerSelected, this.clickOrgWork[amt].name).subscribe(data => this.getorgworkdata(data));
        }
      }
    }
  }
  orgactivityclick(event) {
    for (let amt = 0; amt < this.orgActivtyClicked.length; amt++) {
      if (event.element._index === amt) {
        this.selectedTableTab = 1;
        if (this.orgactivitytypeSelected === 'TYPE') {
          this.ws.getOrgAcitityItemsType(this.orgActivityBelowSelected, this.orgactivityDatePickerSelected, this.orgActivtyClicked[amt].typeId).subscribe(data => this.orgactivitydata(data));
        } else {
          this.ws.getOrgActivityItems(this.orgActivityBelowSelected, this.orgactivityDatePickerSelected, this.orgActivtyClicked[amt].name).subscribe(data => this.orgactivitydata(data));
        }
      }
    }
  }
  individualworkclicked(event) {
    for (let amt = 0; amt < this.individualWorkItems.length; amt++) {
      if (event.element._index === amt) {
        this.selectedUSerTab = 0;
        if (this.workTypeSelected === 'TYPE') {
          this.ws.getUserWorkItemsType(this.workDatePickerSelected, this.individualWorkItems[amt].typeId).subscribe(data => this.getUserWorkItems(data));

        } else {
          this.ws.getUserWorkItemsStatus(this.workDatePickerSelected, this.individualWorkItems[amt].name).subscribe(data => this.getUserWorkItems(data));
        }
      }
    }


  }
  selected(event) {
    console.log(event);
  }
  userActivityClicked(event) {
    for (let amt = 0; amt < this.individualActivityData.length; amt++) {
      if (event.element._index === amt) {
        this.selectedUSerTab = 1;
        if (this.activitytypeSelected === 'TYPE') {
          this.ws.getActivityItemsType(this.activityDatePickerSelected, this.individualActivityData[amt].typeId).subscribe(data => this.getUserActivities(data));

        } else {
          this.ws.getActivityItems(this.activityDatePickerSelected, this.individualActivityData[amt].name).subscribe(data => this.getUserActivities(data));
        }
      }
    }
  }

  lookupRowStyleClass(rowData) {
    return rowData.status === 'COMPLETE' ? 'compelete-row' : 'active-row';
  }
}
