import { Component, OnInit, ViewChild, ChangeDetectorRef, OnChanges, Renderer, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../../node_modules/@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subscription } from '../../../../../node_modules/rxjs';
import { WorkService } from '../../../service/work.service';
import { UserService } from '../../../service/user.service';
import { SchemaService } from '../../../service/schema.service';
import { IntegrationService } from '../../../service/integration.service';
import { DocumentService } from '../../../service/document.service';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { FileUploader, FileLikeObject, FileItem } from 'ng2-file-upload';
import { Message } from '../../../../../node_modules/primeng/primeng';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { ContentService } from '../../../service/content.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit, OnChanges {

  @ViewChild('addDocument') addDoc: ElementRef;
  public form: FormGroup;
  public dynamicform: FormGroup;
  public selectedTempId;
  public val: any;
  public name: any;
  public activitytype: any;
  public uploader;
  public showDoc = false;
  public tempFileobject = new FileUploader({});
  public globelFileUploder = new FileUploader({});
  public selectedObject;
  public duplicateActivity;
  public closeResult;
  public documentsSelected = [];
  public tempAttachments = [];
  public formData;
  public title: 'ScanedImage';
  public workid: any;
  @ViewChild('t') ngbTabSet;
  public createformtabshow = false;
  public activeIdString: any;
  public createformtitle: any;
  public tempTemplateId;
  public showDocumentsTick = false;
  public websocketNotOpen = false;
  public dropdownList = [];
  public selectedItems = [];
  public dropdownSettings = {};
  public formarray = [];
  public showform = false;
  public showcreate = false;
  public increament = 0;
  public sub = new Subscription();
  public createis = false;
  public singleSelectDropdown;
  public multipleSelectDropdown;
  public singleSelectDropdownTA;
  public itemList;
  public formObjectSubmit = [];
  public addarray = [];
  public showaddlabel: any;
  public tempid = 1;
  public temptask = [];
  public subjectProps: any;
  public checkouid = 0;
  public fileNameList = [];
  public nextScreen: any;
  public attachDocNonMandetory = false;
  tabs: Array<boolean> = new Array<boolean>(false, false);
  public lookupOptions = [];
  public selectedTab: any;
  public toldisable = false;
  public msgs: Message[];
  public datePicker: any;
  public quillSettings = {
    theme: 'bubble'
  };
  public hashTwainModel;
  public documentClass;
  public documentClassProp;
  public documentAdd: FormGroup;
  public docClassSymName;
  public docClassName;
  public attachmentfileName = null;
  public attachmentfile;
  public selectedClassItem;
  public docProperty;
  public recentDocumentList = [];
  public systemDate;
  public attachfilestabshow = false;
  public formsButtonsList;
  public formObject;
  public ddmmyy = true;


  // @ViewChild('editor') editor: QuillEditorComponent;  private tr: ToastrService,  private modalService: NgbModal, private cdRef: ChangeDetectorRef,
  // public http: Http,  private _location: Location,  private location: Location,
  constructor(private fb: FormBuilder, private ar: ActivatedRoute,
    private ws: WorkService, private us: UserService, private renderer: Renderer,
    private ss: SchemaService, private router: Router, public ngxSmartModalService: NgxSmartModalService,
    private ds: DocumentService, private breadcrumbService: BreadcrumbService,
    private integrationservice: IntegrationService, private cs: ContentService,
    public changeDetectRef: ChangeDetectorRef, private translate: TranslateService,
    private tr: ToastrService, private spinnerService: Ng4LoadingSpinnerService) {
    if (localStorage.getItem('Date Format') === 'DD/MM/YYYY') {
      this.ddmmyy = true;
    } else {
      this.ddmmyy = false;
    }
    this.ddmmyy = true;
    this.tempAttachments = [];
    this.breadcrumbService.setItems([
      { label: 'Create' }
    ]);
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: 'Document added', detail: '' });
    const browserLang: string = translate.getBrowserLang();
    translate.use('en');
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    //   this.sub = router.events.subscribe((val) => {
    //     if(val instanceof NavigationStart) {
    //       if(this.createis === false) {
    //     // window.confirm('Create is not done');
    //     this.initialiseInvites();

    //       }
    //     }
    // });

    this.form = this.fb.group({
      subject: [null, Validators.compose([Validators.required])]
    });

    this.dynamicform = this.fb.group({});
    this.documentAdd = this.fb.group({});
  }
  // ngOnDestroy() {
  //   this.sub.unsubscribe();
  // }

  initialiseInvites() {
    // Set default values and re-fetch any data you need.
  }
  tabChangeHelper(name, value) {
  }
  ngOnInit() {
    this.spinnerService.show();
    this.ar.queryParams.subscribe(parms => {
      this.formarray = [];
      this.showform = false;
      this.showcreate = false;
      this.increament = 0;
      this.tempAttachments = [];
      this.val = parms['id'];
      this.name = parms['name'];
      this.workid = parms['typeId'];
      this.ss.getActivityTypeFromID(this.val).subscribe(data => { this.activityType(data); });
      this.showDocumentsTick = false;
      this.formObject = [];
      this.activeIdString = null;
      this.createformtabshow = false;
      this.form.reset();
      this.dynamicform.reset();
      this.createis = false;
      this.showaddlabel = parms['multtiFromSupport'];
      this.tempid = 1;
      this.temptask = [];
      this.subjectProps = parms['subjectProps'],
        this.fileNameList = [];
      this.attachDocNonMandetory = false;
    });
    this.dropdownList = [];
    this.selectedItems = [];
    this.singleSelectDropdown = {
      text: 'Single Select',
      singleSelection: true,
      enableSearchFilter: true,
      noDataLabel: 'Searching...'
    };
    this.multipleSelectDropdown = {
      singleSelection: false,
      text: 'Multiple Select',
      enableSearchFilter: true,
      noDataLabel: 'Searching...',
      classes: 'backgroundColor'
    };
  }

  createTabClicked(event) {
  }

  onTabChange(event) { // this always works
    this.selectedTab = event.index;
    if (event.index === 0) {
      this.createformtabshow = false;
      this.attachfilestabshow = false;
      this.formObject = [];
      this.dynamicform.reset();
    } else {
      this.createformtabshow = true;
    }
  }

  ngOnChanges() {
    this.tempAttachments = [];
  }
  onsubmit() {
    this.spinnerService.show();
    let flag = false;
    for (let index = 0; index < this.activitytype.docTypes.length; index++) {
      if (this.activitytype.docTypes[index].req === 1 && this.activitytype.docTypes[index].type === 'FORM') {
        for (let attach = 0; attach < this.temptask.length; attach++) {
          if (this.activitytype.docTypes[index].id === this.temptask[attach].docType.id && this.activitytype.docTypes[index].tempid === this.temptask[attach].tempid) {
            flag = true;
            break;
          } else {
            flag = false;
          }
        }
      }
    }

    let flag3 = true;
    for (let attach3 = 0; attach3 < this.duplicateActivity.docTypes.length; attach3++) {
      if (this.duplicateActivity.docTypes[attach3].req === 1 && this.duplicateActivity.docTypes[attach3].type !== 'FORM' && this.attachDocNonMandetory === false) {
        if (this.tempAttachments.length > 0) {
          for (let attach1 = 0; attach1 < this.tempAttachments.length; attach1++) {
            if (this.duplicateActivity.docTypes[attach3].id === this.tempAttachments[attach1].docType.id) {
              flag3 = true;
              break;
            } else {
              flag3 = false;
            }
          }
        } else {
          flag3 = false;
        }
      }
    }
    if (flag === true && flag3 === true) {
      let formarraycrate;
      // formarraycrate = [];
      for (let i = 0; i < this.temptask.length; i++) {
        let paramval;
        paramval = {
          subject: this.form.controls.subject.value,
          typeId: this.workid,
          createdBy: this.us.getCurrentUser().EmpNo,
          creatorRoleId: this.us.getCurrentUser().roles[0].id,
          attachments: this.tempAttachments
        };
        paramval.attachments = paramval.attachments.concat(this.temptask[i]);
        for (let o = 0; o < paramval.attachments.length; o++) {
          delete paramval.attachments[o].tempid;
        }
        formarraycrate = paramval;
        // formarraycrate.push(paramval);
      }
      this.ws.saveWork(formarraycrate).subscribe(data => { this.createIsDone(data); }, eror => {
        this.spinnerService.hide(); this.ngOnInit();
      }); // this.tr.error('', 'Failed To Create')
    } else {
      this.tr.error('', 'Need to attach required documents');
      this.spinnerService.hide();
    }
  }
  createIsDone(data) {
    let route;
    route = JSON.parse(data._body);
    this.createis = true;
    // this.route.navigateByUrl('sent');
    if (route > 0 && (this.nextScreen === 'DRAFT' || this.nextScreen === 'Draft')) {
      this.ws.getWorkDraftActivity(route).subscribe(res => this.openActivity(res), error => {
        this.spinnerService.hide();
      }); // this.toastr.error('Failed to route')
      // this.router.navigateByUrl('inbox/readworkitem/' + route);
    } else {
      this.tr.success('', 'Created Successfully');
      this.spinnerService.hide();
      this.router.navigate(['/sent']);

    }
  }
  openActivity(data) {
    // this.navigateByUrl('')
    this.tr.success('', 'Created And Document Routed');
    this.spinnerService.hide();
    let value;
    value = JSON.parse(data._body);
    this.router.navigate(['/draft/activity'], { queryParams: { 'readworkitem': 'readworkitem', 'workId': value } });
  }
  activityType(data) {
    this.spinnerService.hide();
    this.activitytype = JSON.parse(data._body);
    this.nextScreen = this.activitytype.nextScreen;
    if (this.activitytype && this.activitytype.docTypes && this.activitytype.docTypes.length > 0) {
      this.showDoc = true;

      this.duplicateActivity = JSON.parse(data._body);
      for (let index = 0; index < this.activitytype.docTypes.length; index++) {
        if (this.activitytype.docTypes[index].type === 'DOCUMENT') {
          // this.activitytype.docTypes[index].id = new FileUploader({});
        } else if (this.activitytype.docTypes[index].type === 'FORM') {
          this.showform = true;
          this.showcreate = false;
          this.formarray.push(this.activitytype.docTypes[index]);
        }
      }
      this.fill(this.formarray);
    } else {
      this.showDoc = false;
    }
  }

  geterror(error) {
    //   this.tr.error('', 'Connection issue');
  }
  fill(array) {
    if (array.length > 0) {
      if (array.length > this.increament) {
        this.documentForm(array[this.increament]);
      } else {
        this.showform = false;
        this.showcreate = true;
      }
    }
  }
  tempObject(obj, name) {
    this.tempFileobject = obj;
    for (let index = 0; index < this.activitytype.docTypes.length; index++) {
      if (this.activitytype.docTypes[index].type === 'DOCUMENT' && this.activitytype.docTypes[index].name === name) {
        this.selectedObject = this.duplicateActivity.docTypes[index].id;
      }
    }
  }

  getRecent() {
    const date = new Date();
    this.systemDate = date;
    this.cs.getRecent(1).subscribe(data => this.getRecentDocuments(data));
  }

  getRecentDocuments(data) {
    if (data._body !== '') {
      this.recentDocumentList = JSON.parse(data._body);
      this.attachfilestabshow = true;
      this.selectedTab = 2;
      this.changeDetectRef.detectChanges();
      this.documentsSelected = [];
    }
  }

  openDocumentSearchModel(documentsSearch) {
    this.documentsSelected = [];
    let val;
    val = 10;
    // this.modalService.open(documentsSearch, this.popupOptions).result.then((result) => {
    //   this.closeResult = `Closed with: ${result}`;
    // }, (reason) => {
    //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    // });
  }

  documentSearchList(event) {
    this.documentsSelected = event;
    for (let i = 0; i < this.documentsSelected.length; i++) {
      //   if (this.documentsSelected[i].format !== 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      //     && this.documentsSelected[i].format !== 'application/pdf' && this.documentsSelected[i].format !== 'application/msword' && this.documentsSelected[i].format !== 'image/jpeg' && this.documentsSelected[i].format !== 'image/png') {
      // this.tr.error('', this.documentsSelected[i].name  + ': Not able to attach please select only pdf, doc and image files');
      //  this.documentsSelected.splice(i, 1);
      //   }else {
      //   }
    }
  }

  attachRepDoc() {
    for (let index = 0; index < this.documentsSelected.length; index++) {
      const attachments = {
        docId: this.documentsSelected[index].docId,
        docTitle: this.documentsSelected[index].name,
        format: this.documentsSelected[index].format,
        docType: {
          id: this.selectedObject
        },
        tempid: 1
      };
      this.tr.success('', 'Document added');
      this.tempAttachments.push(attachments);
    }
    this.attachfilestabshow = false;
    for (let index = 0; index < this.documentsSelected.length; index++) {
      this.uploadFile(this.documentsSelected[index].name);
    }
    this.documentsSelected = [];
  }

  uploadFile(desiredFilename: string) {
    //   let date: number ;
    //   date = new Date().getTime();
    // //  let contents: string[];
    //   // contents must be an array of strings, each representing a line in the new file
    //   let file ;
    //   file = new File([''], desiredFilename, {type: 'text/plain', lastModified: date});
    //   let fileItem ;
    //   fileItem = new FileItem(this.tempFileobject, file, {});
    //   // (Visual Only) adds the new fileItem to the upload queue
    //   this.tempFileobject.queue.push(fileItem);


    const date: number = new Date().getTime();
    // contents must be an array of strings, each representing a line in the new
    // file
    const file = new File([''], desiredFilename, {
      type: 'text/plain',
      lastModified: date
    });
    const fileItem = new FileItem(this.tempFileobject, file, {});
    // (Visual Only) adds the new fileItem to the upload queue
    this.tempFileobject.queue.push(fileItem);



  }

  inputfileChanged(id, docClass) {
    for (let i = 0; i < this.globelFileUploder.queue.length; i++) {
      // if (this.globelFileUploder.queue[i]._file.type === 'application/octet-stream'
      // || this.globelFileUploder.queue[i]._file.type === 'application/java-archive'
      // || this.globelFileUploder.queue[i]._file.type === 'application/x-zip-compressed'
      // || this.globelFileUploder.queue[i]._file.type === 'application/x-msdownload' ) {
      //   this
      //   .tr
      //   .error(
      //       '',
      //       this.globelFileUploder.queue[i]._file.name + ': Not able to attach please select only pdf, docs, xls and image files'
      //   );
      //    this.globelFileUploder.removeFromQueue(this.globelFileUploder.queue[i]);
      //   i--;
      // }else {
      //   this.addDocument(id, docClass);
      // }
    }
    this.addDocument(id, docClass);
  }

  addDocument(fileUploader, docClass) {
    this.spinnerService.show();
    let count = 0;
    for (let i = 0; i < this.globelFileUploder.queue.length; i++) {
      this.spinnerService.show();
      const docInfo = {
        docclass: docClass,
        props: [{
          'name': 'Document Title',
          'symName': 'DocumentTitle',
          'dtype': 'STRING',
          'mvalues': [this.globelFileUploder.queue[i]._file.name],
          'mtype': 'N',
          'len': 255,
          'rOnly': 'false',
          'hidden': 'false',
          'req': 'false'
        }],
        accessPolicies: []
      };
      this.formData = new FormData();
      this.formData.append('DocInfo', JSON.stringify(docInfo));
      this.formData.append('file' + i, this.globelFileUploder.queue[i]._file);
      this.ds.addDocument(this.formData).subscribe(data => this.addDocTemp(data, this.globelFileUploder.queue[i]._file.name, this.globelFileUploder.queue[i]._file.type, ++count), error => { this.spinnerService.hide(); });
    }
  }

  addDocTemp(data, name, format, count) {
    let adddocument;
    adddocument = [];
    adddocument.push({
      docid: data,
      docTitle: name,
      format: format
    });
    this.spinnerService.hide();
    this.saveAttachemtObjects(adddocument, count);
  }

  saveAttachemtObjects(data, count) {
    const attachments = {
      docId: data[0].docid._body,
      docTitle: data[0].docTitle,
      format: data[0].format,
      docType: {
        id: this.selectedObject
      }
    };
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: 'Document added', detail: '' });
    this.tr.success('', 'Document Added');
    this.tempAttachments.push(attachments);
    const date: number = new Date().getTime();
    // contents must be an array of strings, each representing a line in the new file
    // const file = new File([''], data[0].docTitle, {type: data[0].format, lastModified: date});
    // const fileItem = new FileItem(this.tempFileobject, file, {});
    // // (Visual Only) adds the new fileItem to the upload queue
    // this.tempFileobject.queue.push(fileItem);
    if (count === this.globelFileUploder.queue.length) {
      this.globelFileUploder.clearQueue();
    }

  }

  parseInt(str) {
    let x = str;
    let y = +x; // y: number
    return y;
  }

  formView(docId) {
    this.router.navigateByUrl('inbox/form/readonly/' + docId);
  }

  onMessageWithProp(e) {
    this.websocketNotOpen = false;
    const storedFiles = [];
    let count = 0;
    if (typeof e.data === 'string') {
      // IF Received Data is String
    } else if (e.data instanceof ArrayBuffer) {
      // IF Received Data is ArrayBuffer
    } else if (e.data instanceof Blob && count <= 1) {
      const f = e.data;
      count++;
      const reader = new FileReader();
      reader.onload = (f: FileReaderEvent) => {
        const pdfdata = f.target.result;
        const byteCharacters = atob(pdfdata.replace('data:application/octet-stream;base64,', ''));
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const blob = new Blob([byteArray], { type: 'application/pdf' });
        let string: String;
        string = this.title;
        let date;
        date = new Date();
        for (let i = 0; i < this.documentClassProp.length; i++) {
          if (this.documentClassProp[i].symName === 'documenttitle') {
            this.documentAdd.controls[this.documentClassProp[i].symName].patchValue('ScannedImage'.concat(date.getTime().toString()).concat('.pdf'));
          }
        }
        this.attachmentfile = null;
        this.attachmentfile = blob;
        this.attachmentfileName = 'ScannedImage'.concat(date.getTime().toString()).concat('.pdf');
      };
      reader.readAsDataURL(f);
    }
  }

  onMessage(e, docClass) {
    this.spinnerService.show();
    this.websocketNotOpen = false;
    const storedFiles = [];
    let count = 0;
    if (typeof e.data === 'string') {
      // IF Received Data is String
    } else if (e.data instanceof ArrayBuffer) {
      // IF Received Data is ArrayBuffer
    } else if (e.data instanceof Blob && count <= 1) {
      const f = e.data;
      count++;
      const reader = new FileReader();
      reader.onload = (f: FileReaderEvent) => {
        const pdfdata = f.target.result;
        const byteCharacters = atob(pdfdata.replace('data:application/octet-stream;base64,', ''));
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const blob = new Blob([byteArray], { type: 'application/pdf' });
        // saveAs(blob, this.title);
        let string: String;
        string = this.title;
        let date;
        date = new Date();
        const docInfo = {
          docclass: docClass,
          props: [{
            'name': 'Document Title',
            'symName': 'DocumentTitle',
            'dtype': 'STRING',
            'mvalues': ['ScannedImage'.concat(date.getTime().toString()).concat('.pdf')],
            'mtype': 'N',
            'len': 255,
            'rOnly': 'false',
            'hidden': 'false',
            'req': 'false'
          }],
          accessPolicies: []
        };
        let formData;
        formData = new FormData();
        formData.append('DocInfo', JSON.stringify(docInfo));
        formData.append('document', blob, 'ScannedImage'.concat(date.getTime().toString()).concat('.pdf'));
        this.ds.addDocument(formData).subscribe(data => { this.scannedDocument(data, 'ScannedImage'.concat(date.getTime().toString()).concat('.pdf')); this.spinnerService.hide(); }, error => this.spinnerService.hide());
      };
      reader.readAsDataURL(f);
    }
  }

  scannedDocument(data, name) {
    this.uploadFile(name);
    const str = 'Scanned-';
    const attachments = {
      docId: data._body,
      docTitle: name,
      format: 'application/pdf',
      docType: {
        id: this.selectedObject
      }
    };
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: 'Document added', detail: '' });
    this.tr.success('', 'Document added');
    this.tempAttachments.push(attachments);
  }

  scanImage(id, docClass) {
    const socket = new WebSocket('ws://localhost:8181');
    socket.onopen = () => socket.send('1100');
    socket.onmessage = (e) => this.onMessage(e, docClass);
    socket.onerror = (e) => this.onWebsocketError(e);
  }

  scanImageWithProp() {
    const socket = new WebSocket('ws://localhost:8181');
    socket.onopen = () => socket.send('1100');
    socket.onmessage = (e) => this.onMessageWithProp(e);
    socket.onerror = (e) => this.onWebsocketError(e);
  }

  onWebsocketError(e) {
    this.websocketNotOpen = true;
    const socket = new WebSocket('ws://localhost:8181');
    socket.close();
    this.ngxSmartModalService.getModal('hashTwainModel').open();
  }
  removeAttachments(fileobj) {
    for (let index = 0; index < this.tempAttachments.length; index++) {
      if (fileobj.docTitle === this.tempAttachments[index].docTitle) {
        this.tempAttachments.splice(index, 1);
      }
    }
    this.tr.warning('', 'Document removed');
  }

  backClick() {
    //  this._location.back();
  }

  downloadDocument(docId) {
    this.ds.downloadDocument(docId).subscribe(data => this.downloadCurrentDocument(data));

  }

  downloadCurrentDocument(data) {
    // let filename ='';
    //   let disposition = data.headers.get('Content-Disposition');
    //   let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    //     let matches = filenameRegex.exec(disposition);
    //     if (matches != null && matches[1]) {
    //       filename = matches[1].replace(/['"]/g, '');
    //     }
    //     saveAs(data._body, filename);
  }


  documentForm(item) {
    this.spinnerService.show();
    this.selectedTempId = item.id;
    if (item.tempid === undefined) {
      this.tempid = 1;
      item.tempid = 1;
    } else {
      this.tempid = item.tempid;
    }

    this.createformtitle = item.name;
    if (this.showform === false) {
      this.createformtabshow = true;
      this.formarray = [];
      this.showform = false;
      this.increament = 0;
    }
    this.activeIdString = 'createformTab';
    this.selectedTab = 1;
    this.formObject = [];
    this.tempTemplateId = item.template;
    this.selectedTempId = item.id;
    let flag = 1;
    if (this.temptask.length > 0) {
      for (let i = 0; i < this.temptask.length; i++) {
        if (this.temptask[i].docType.id === item.id && this.temptask[i].tempid === item.tempid) {
          this.ds.getJSONFromDocument(this.temptask[i].docId).subscribe(data => this.getJSONFromDocument(data), error => this.spinnerService.hide());
          flag = 1;
          break;
        } else {
          flag = 0;
        }
      }
    } else {
      this.ds.getJSONFromDocument(this.tempTemplateId).subscribe(data => this.getJSONFromDocument(data), error => this.spinnerService.hide());
    }
    if (flag === 0) {
      this.ds.getJSONFromDocument(this.tempTemplateId).subscribe(data => this.getJSONFromDocument(data), error => this.spinnerService.hide());
    }
  }
  getJSONFromDocument(data) {
    this.formObject = data._body;
    this.spinnerService.hide();
  }




  formDynamicSubmit(event) {
    this.spinnerService.show();
    this.formObjectSubmit = event;
    let subjectProps = '';
    if (this.subjectProps !== undefined) {
      const props = this.subjectProps.split(',');
      for (let l = 0; l < props.length; l++) {
        for (let index = 0; index < this.formObjectSubmit.length; index++) {
          for (let i = 0; i < this.formObjectSubmit[index].sections.length; i++) {
            if (this.formObjectSubmit[index].sections[i].type === 'FORM') {
              for (let j = 0; j < this.formObjectSubmit[index].sections[i].columns.length; j++) {
                for (let k = 0; k < this.formObjectSubmit[index].sections[i].columns[j].properties.length; k++) {
                  // this.formObjectSubmit[index].sections[i].columns[j].properties[k].value = this.formObject[index].sections[i].columns[j].properties[k].value;
                  if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].name === props[l]) {
                    subjectProps = subjectProps + this.formObjectSubmit[index].sections[i].columns[j].properties[k].value[0].value;
                    if (l + 1 < props.length) {
                      subjectProps = subjectProps + '-';
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    for (let index = 0; index < this.formObjectSubmit.length; index++) {
      for (let i = 0; i < this.formObjectSubmit[index].sections.length; i++) {
        if (this.formObjectSubmit[index].sections[i].type === 'FORM') {
          for (let j = 0; j < this.formObjectSubmit[index].sections[i].columns.length; j++) {
            for (let k = 0; k < this.formObjectSubmit[index].sections[i].columns[j].properties.length; k++) {
              if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].name === 'privacy') {
                if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].value[0].value === '5') {
                  this.attachDocNonMandetory = true;
                } else {
                  this.attachDocNonMandetory = false;
                }
              }
            }
          }
        }
      }
    }
    this.form.patchValue({
      subject: subjectProps
    });

    const date = new Date();
    const docInfo = {
      id: this.tempTemplateId,
      docclass: 'ProductivitiDocument',
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [this.createformtitle.concat(date.getTime().toString())],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
      accessPolicies: []
    };
    const formData = new FormData();
    // formData.append('DocInfo', JSON.stringify(docInfo));
    for (const docClass of this.formObjectSubmit) {
      docClass.sections.forEach(element => {
        if (element.type === 'FORM') {
          for (const cols of element.columns) {
            for (const props of cols.properties) {
              delete props.dbValue;
              delete props.dbDummyValue;
              delete props.lookupOptions;
              for (const value of props.value) {
                delete value._$visited;
              }
            }
          }
        } else if (element.type === 'TABLE') {
          for (const row of element.rows) {
            for (const item of row.items) {
              delete item.rOnly;
              delete item.req;
              delete item.label;
              delete item.type;
              delete item.length;
              delete item.lookup;
            }
          }
        }
      });
    }
    formData.append('file', new Blob([JSON.stringify(this.formObjectSubmit)]), this.createformtitle.concat(date.getTime().toString()));
    // this.ds.addJSONDocument(this.formObject.pop()).subscribe(data => this.getDocId(data, this.createformtitle.concat(date.getTime().toString())));
    let attached = false;
    if (this.temptask.length > 0) {
      for (let i = 0; i < this.temptask.length; i++) {
        if (this.temptask[i].docType.id === this.selectedTempId && this.temptask[i].tempid === this.tempid) {
          attached = true;
          this.checkouid = this.temptask[i].docId;
          break;
        } else {
          attached = false;
        }
      }
    } else {
      attached = false;
    }
    if (attached === true) {
      let flag = 0;
      if (this.formObjectSubmit[0].datatable === undefined) {
        this.formObjectSubmit[0].datatable = [];
        // this.formObjectSubmit[0].datatable.push({'key': 'DOCID', 'value': this.checkouid});
      }
      if (this.formObjectSubmit[0].datatable.length > 0) {
        for (let index = 0; index < this.formObjectSubmit[0].datatable.length; index++) {
          if (this.formObjectSubmit[0].datatable[index].key === 'DOCID') {
            this.formObjectSubmit[0].datatable[index].value = this.checkouid;
            flag = 1;
            break;
          } else {
            flag = 0;
          }
        }
      } else {
        this.formObjectSubmit[0].datatable.push({ 'key': 'DOCID', 'value': this.checkouid });
        flag = 1;
      }
      if (flag === 0) {
        // this.formObjectSubmit[0].datatable.push({'DOCID': this.checkouid});
        this.formObjectSubmit[0].datatable.push({ 'key': 'DOCID', 'value': this.checkouid });
        //
      }
      this.ds.saveFormDocument(this.formObjectSubmit[0]).subscribe(datares => { this.chechedin(datares); this.spinnerService.hide(); });
      this.activeIdString = null;
      this.createformtabshow = false;
      this.activeIdString = 'createTab';
      this.selectedTab = 0;
    } else {
      if (this.activitytype.docTypes.length === 1) {
        this.ds.saveFormDocument(this.formObjectSubmit[0]).subscribe(data => { this.spinnerService.hide(); this.getDocIdSubmit(data, this.createformtitle.concat(date.getTime().toString())); }, error => { this.failedToAddForm(error); });
      } else {
        this.ds.saveFormDocument(this.formObjectSubmit.pop()).subscribe(data => { this.spinnerService.hide(); this.getDocId(data, this.createformtitle.concat(date.getTime().toString())); }, error => { this.failedToAddForm(error); });
        this.formObjectSubmit = [];
      }
    }
  }

  getDocIdSubmit(data, name) {
    const attachments = {
      docId: data._body,
      docTitle: name,
      format: 'application/json',
      docType: {
        id: this.selectedTempId
      },
      tempid: this.tempid
    };
    this.temptask.push(attachments);
    for (let i = 0; i < this.activitytype.docTypes.length; i++) {
      if (this.tempid === 1) {
        if (this.activitytype.docTypes[i].id === this.selectedTempId) {
          this.activitytype.docTypes[i].showgreentich = true;
          this.activitytype.docTypes[i].showadd = true;
          this.activitytype.docTypes[i].tempid = this.tempid;
          break;
        }
      }
    }
    if (this.activitytype.docTypes.length > 0) {
      for (let t = 0; t < this.activitytype.docTypes.length; t++) {
        if (this.activitytype.docTypes[t].id === this.selectedTempId && this.activitytype.docTypes[t].tempid === this.tempid) {
          this.activitytype.docTypes[t].showgreentich = true;
          this.activitytype.docTypes[t].tempid = this.tempid;
          break;
        }
      }
    }
    //   if (this.showform === true && this.formarray.length > this.increament) {
    //     this.increament = this.increament + 1;
    //     if (this.formarray.length === this.increament) {
    // this.increament = 0;
    // this.tempAttachments = [];
    //     }else if (this.formarray.length > this.increament) {
    //       this.fill(this.formarray);
    //     }
    //   }
    if (this.activitytype.docTypes.length === 1) {
      this.onsubmit();
    }
  }

  failedToAddForm(error) {
    this.spinnerService.hide();
    // this.tr.error('', 'Failed to Add Document');
  }
  checkinfile(data, response) {
    const date = new Date();
    const docInfo = {
      id: data._body,
      docclass: 'ProductivitiDocument',
      props: [{
        'name': 'Document Title',
        'symName': 'DocumentTitle',
        'dtype': 'STRING',
        'mvalues': [this.createformtitle.concat(date.getTime().toString())],
        'mtype': 'N',
        'len': 255,
        'rOnly': 'false',
        'hidden': 'false',
        'req': 'false'
      }],
      accessPolicies: []
    };
    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(docInfo));
    formData.append('file', new Blob([JSON.stringify(this.formObjectSubmit.pop())]), this.createformtitle.concat(date.getTime().toString()));
    this.ds.checkIn(formData).subscribe(datares => { this.chechedin(datares); });
  }
  chechedin(data) {
    this.tr.success('Saved Successfully');
    for (let i = 0; i < this.temptask.length; i++) {
      if (this.temptask[i].docType.id === this.selectedTempId && this.temptask[i].tempid === this.tempid) {
        this.temptask[i].docId = data._body;
        break;
      }
    }
    // this.formObjectSubmit = [];
  }
  cancelForm(vale) {
    this.activeIdString = null;
    this.createformtabshow = false;
    this.activeIdString = 'createTab';
    this.selectedTab = 0;
  }

  getDocId(data, name) {
    // this.tr.success('', this.createformtitle + ' ' + 'Saved');
    this.activeIdString = null;
    const attachments = {
      docId: data._body,
      docTitle: name,
      format: 'application/json',
      docType: {
        id: this.selectedTempId
      },
      tempid: this.tempid
    };
    // this.tempAttachments.push(attachments);
    this.temptask.push(attachments);
    for (let i = 0; i < this.activitytype.docTypes.length; i++) {
      if (this.tempid === 1) {
        if (this.activitytype.docTypes[i].id === this.selectedTempId) {
          this.activitytype.docTypes[i].showgreentich = true;
          this.activitytype.docTypes[i].showadd = true;
          this.activitytype.docTypes[i].tempid = this.tempid;
          break;
        }
      }
    }
    this.createformtabshow = false;
    if (this.activitytype.docTypes.length > 0) {
      for (let t = 0; t < this.activitytype.docTypes.length; t++) {
        if (this.activitytype.docTypes[t].id === this.selectedTempId && this.activitytype.docTypes[t].tempid === this.tempid) {
          this.activitytype.docTypes[t].showgreentich = true;
          this.activitytype.docTypes[t].tempid = this.tempid;
          break;
        }
      }
    }
    if (this.showform === true && this.formarray.length > this.increament) {
      this.increament = this.increament + 1;
      if (this.formarray.length === this.increament) {
        this.formarray = [];
        this.showform = false;
        this.showcreate = false;
        this.increament = 0;
        this.showform = false;
        this.tempAttachments = [];
        this.showcreate = true;
      } else if (this.formarray.length > this.increament) {
        this.fill(this.formarray);
      }
    }
    this.showDocumentsTick = true;
    this.activeIdString = 'createTab';
    this.selectedTab = 0;
  }

  getCheckIn(data, name) {
    this.activeIdString = null;
    const attachments = {
      docId: data._body,
      docTitle: name,
      format: 'application/json',
      docType: {
        id: this.selectedTempId
      },
      // tempid: this.tempid
    };
    this.tempAttachments.push(attachments);
    this.activeIdString = 'createTab';
    this.selectedTab = 0;
    this.createformtabshow = false;
  }


  onItemSelect(item: any, ind, i, j, k) {
    this.formObject[0].sections[i].columns[j].properties[k].value = [];

    for (let index = 0; index < item.length; index++) {
      const selected = {
        'name': item[index].itemName,
        'value': item[index].id
      };
      this.formObject[ind].sections[i].columns[j].properties[k].value.push(selected);
    }
  }
  OnItemDeSelect(item: any, ind, sectionValue, columnvalue, propvalue) {
    this.formObject[ind].sections[sectionValue].columns[columnvalue].properties[propvalue].value = [];
    for (let index = 0; index < item.length; index++) {
      const selected = {
        'name': item[index].itemName,
        'value': item[index].id
      };
      this.formObject[ind].sections[sectionValue].columns[columnvalue].properties[propvalue].value.push(selected);
    }
  }
  onSelectAll(items: any, ind, i, j, k) {
  }
  onDeSelectAll(items: any, ind, i, j, k) {
  }
  add(value, tempid) {
    let res: any;
    const date = new Date();
    res = Object.assign({}, value);
    res.showright = false;
    res.tempid = date.getTime();
    this.tempid = res.tempid;
    res.showgreentich = false;
    res.showadd = false;
    res.showdelete = true;
    this.activitytype.docTypes.push(res);
  }
  addeddelete(add) {
    for (let i = 0; i < this.activitytype.docTypes.length; i++) {
      if (this.activitytype.docTypes[i].id === add.id && this.activitytype.docTypes[i].tempid === add.tempid) {
        this.activitytype.docTypes.splice(i, 1);
      }
    }
    for (let t = 0; t < this.temptask.length; t++) {
      if (this.temptask[t].docType.id === add.id && this.temptask[t].tempid === add.tempid) {
        this.temptask.splice(t, 1);
      }
    }
  }




  showClassProp(item) {
    this.selectedClassItem = item;
    this.cs.getDocumentClasses().subscribe(data => this.documentClasses(data, item));
  }

  showClassPropScan(item) {
    this.selectedClassItem = item;
    this.cs.getDocumentClasses().subscribe(data => this.documentClassesScan(data, item));
  }

  documentClassesScan(data, item) {
    if (data._body !== '') {
      this.documentClass = JSON.parse(data._body);
      for (let index = 0; index < this.documentClass.length; index++) {
        if (this.documentClass[index].symName === item.docClass) {
          this.docProperty = this.documentClass[index];
          this.documentClassProp = this.documentClass[index].props;
          this.docClassSymName = this.documentClass[index].value;
          this.docClassName = this.documentClass[index].name;
        }
      }
      for (let index = 0; index < this.documentClassProp.length; index++) {
        if (this.documentClassProp[index].req === 'true' && this.documentClassProp[index].symName !== 'documenttitle') {
          const control: FormControl = new FormControl(null, Validators.required);
          this.documentAdd.addControl(this.documentClassProp[index].symName, control);
        } else {
          const control: FormControl = new FormControl(null);
          this.documentAdd.addControl(this.documentClassProp[index].symName, control);
        } if (this.documentClassProp[index].lookup !== undefined && this.documentClassProp[index].symName !== 'documenttitle') {
          const control: FormControl = new FormControl(null);
          this.documentAdd.addControl(this.documentClassProp[index].lookups[0].value, control);
        }
      }
    }
    this.attachmentfileName = null;
    this.addDoc.nativeElement.value = null;
    this.documentAdd.reset();
    this.ngxSmartModalService.getModal('documentAddScanWithProps').open();
  }

  documentClasses(data, item) {
    if (data._body !== '') {
      this.documentClass = JSON.parse(data._body);
      for (let index = 0; index < this.documentClass.length; index++) {
        if (this.documentClass[index].symName === item.docClass) {
          this.docProperty = this.documentClass[index];
          this.documentClassProp = this.documentClass[index].props;
          this.docClassSymName = this.documentClass[index].value;
          this.docClassName = this.documentClass[index].name;
        }
      }
      for (let index = 0; index < this.documentClassProp.length; index++) {
        if (this.documentClassProp[index].req === 'true' && this.documentClassProp[index].symName !== 'documenttitle') {
          const control: FormControl = new FormControl(null, Validators.required);
          this.documentAdd.addControl(this.documentClassProp[index].symName, control);
        } else {
          const control: FormControl = new FormControl(null);
          this.documentAdd.addControl(this.documentClassProp[index].symName, control);
        } if (this.documentClassProp[index].lookup !== undefined && this.documentClassProp[index].symName !== 'documenttitle') {
          const control: FormControl = new FormControl(null);
          this.documentAdd.addControl(this.documentClassProp[index].lookups[0].value, control);
        }
      }
    }
    this.attachmentfileName = null;
    this.addDoc.nativeElement.value = null;
    this.documentAdd.reset();
    this.ngxSmartModalService.getModal('documentAddWithProps').open();
  }

  fileChanegd(event) {
    this.attachmentfile = null;
    this.attachmentfile = event.target.files[0];
    this.attachmentfileName = event.target.files[0].name;
    for (let i = 0; i < this.documentClassProp.length; i++) {
      if (this.documentClassProp[i].symName === 'documenttitle') {
        this.documentAdd.controls[this.documentClassProp[i].symName].patchValue(event.target.files[0].name);
      }
    }
  }

  documentSubmit(event) {
    this.spinnerService.show();
    let adddocument;
    adddocument = [];
    let count = 0;
    const docInfo = {
      id: this.docProperty.id,
      creator: this.docProperty.creator,
      addOn: this.docProperty.addOn,
      modOn: this.docProperty.modOn,
      docclass: this.docProperty.symName,
      props: []
    };
    for (const inputField of [].slice.call(event.target)) {
      if (inputField.getAttribute('type') !== 'file') {
        for (const prop of this.docProperty.props) {
          if (inputField.id !== undefined && inputField.id === prop.symName) {
            const property = {
              'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.value], mtype: prop.mtype,
              len: prop.len, rOnly: prop.rOnly, hidden: prop.hidden, req: prop.req, ltype: prop.ltype
            };
            docInfo.props.push(property);
          }
        }
      }
    }

    for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
      if (inputField.getAttribute('type') !== 'file') {
        for (const datepick of this.docProperty.props) {
          if (inputField.id !== undefined && inputField.id === datepick.symName) {
            const property = {
              'symName': datepick.symName, 'dtype': datepick.dtype,
              'mvalues': [inputField.children['0'].children['0'].value],
              len: datepick.len, rOnly: datepick.rOnly, hidden: datepick.hidden, req: datepick.req, ltype: datepick.ltype
            };
            docInfo.props.push(property);
          }
        }
      }
    }

    const formData = new FormData();
    formData.append('DocInfo', JSON.stringify(docInfo));
    formData.append('file', this.attachmentfile);
    this.ds.addDocument(formData).subscribe(data => {
      adddocument.push({
        docid: data,
        docTitle: this.attachmentfileName,
        format: this.attachmentfile.type
      });

      this.ngxSmartModalService.getModal('documentAddWithProps').close();
      this.ngxSmartModalService.getModal('documentAddScanWithProps').close();
      this.documentAdd.reset();
      this.saveAttachemtObjects(adddocument, ++count);
    }, error => { this.spinnerService.hide(); });
  }

  converToUppercase(string) {
    return string.toUpperCase();
  }

}

interface FileReaderEventTarget extends EventTarget {
  result: string;
}

interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage(): string;
}

interface FileUploaderCustom extends FileItem {
  filec: FileLikeObjectCustom;
}

interface FileLikeObjectCustom extends FileLikeObject {
  name: string;
  size: number;
  type: string;
}
