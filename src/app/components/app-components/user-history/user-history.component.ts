import { Component, OnInit, Input, Output, ChangeDetectorRef, OnChanges, EventEmitter } from '@angular/core';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../../../service/user.service';

@Component({
     selector: 'app-user-history',
     templateUrl: './user-history.component.html',
     styleUrls: ['./user-history.component.css']
})

export class UserHistoryComponent implements OnInit {
     public userHistoryList = [];
     public userHistory: any;

     constructor(private breadcrumbService: BreadcrumbService, private us: UserService, private translate: TranslateService) {
          this.breadcrumbService.setItems([{ label: this.translate.instant(' User History') }]);
     }
     ngOnInit() {
          this.us.getUserHistoryItems().subscribe(data => this.getUserHistoryResult(data));
     }
     refreshEvent() {
          this.us.getUserHistoryItems().subscribe(data => this.refreshedData(data));
     }
     refreshedData(data) {
          const value = JSON.parse(data._body);
          this.userHistory = value.activities;
     }
     getUserHistoryResult(data) {
          this.userHistory = [];
          this.userHistory = JSON.parse(data._body);
          this.userHistoryList = this.userHistory;

     }

}
