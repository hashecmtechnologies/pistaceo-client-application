import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SchemaService } from '../../../service/schema.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-create-form-names',
  templateUrl: './create-form-names.component.html',
  styleUrls: ['./create-form-names.component.css']
})
export class CreateFormNamesComponent implements OnInit {
 public datares: any;
  constructor(private breadcrumbService: BreadcrumbService, private translate: TranslateService, private route: ActivatedRoute,
    private ss: SchemaService, private router: Router, private spinnerService: Ng4LoadingSpinnerService) {
    this.breadcrumbService.setItems([
      {label: this.translate.instant('Form Lists')}
    ]);
   }

  ngOnInit() {
    this.spinnerService.show();
    this.ss.getWorkTypes().subscribe(data => {this.dynamicItems(data); } , error => this.spinnerService.hide() );
  }
  dynamicItems(data) {
    this.datares = JSON.parse(data._body);
    this.spinnerService.hide();
  }
  openCreate(value) {
    this.router.navigate(['/create'], {queryParams: {'id': value.initActivityType, 'typeId': value.id, 'multtiFromSupport': value.multiFormSupport, 'subjectProps': value.subjectProps}});
  }
}
