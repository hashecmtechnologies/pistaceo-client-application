import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFormNamesComponent } from './create-form-names.component';

describe('CreateFormNamesComponent', () => {
  let component: CreateFormNamesComponent;
  let fixture: ComponentFixture<CreateFormNamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFormNamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFormNamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
