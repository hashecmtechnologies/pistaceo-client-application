import { Component, OnInit, Output } from '@angular/core';
import { UserService } from '../../../service/user.service';
import { ActivatedRoute } from '@angular/router';
import { WorkService } from '../../../service/work.service';
import { MenuItemsComponent } from '../layout/menu-items/menu-items.component';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { TranslateService } from '@ngx-translate/core';
import { AdminLayoutComponent } from '../layout/admin-layout/admin-layout.component';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {
  public inboxList: any;
  public checktoggle = false;
  public columnValue;
  public statusValue;
  public inboxType;
  public sub;
  public firstParam;
  public secondParam;
  public param;
  public todayDate = new Date();
  public fullDate: any;
  page = 1;
  curPage = 1;
  pageSize = 25;
  totalPages = 1;
  public checkedvalues: any;
  public buttons: any;
  public checkedbutt: any;
  public checkedref: any;
  public refreshdata = false;
  public createis = false;
  public Unread = 0;

  constructor(
    private us: UserService, private route: ActivatedRoute, private ws: WorkService, private translate: TranslateService,
    private breadcrumbeServece: BreadcrumbService, private app: AdminLayoutComponent) { // private menu: MenuItemsComponent
    this.todayDate = new Date();
    this.fullDate = this.todayDate.getDate() + '/' + (this.todayDate.getMonth() + 1) + '/' + this.todayDate.getFullYear();
    this.breadcrumbeServece.setItems([
      { label: this.translate.instant('Inbox') }
    ]);
  }
  ngOnInit() {
    this.inboxType = 'Inbox';
    this.sub = this.route.params.subscribe(params => {
      this.firstParam = params['id'];
      this.secondParam = params['id1'];
    });
    if (this.firstParam === undefined) {
      // this.ts.getUserInbox(this.us.getCurrentUser().EmpNo, this.page).subscribe(data => this.getInboxResult(data));
      this.ws.getInboxItems(this.us.getCurrentUser().EmpNo, this.page).subscribe(data => this.getInboxResult(data));
    } else {
      if (this.secondParam === 'today') {
        const search = {
          sort: 'Priority',
          order: 'Ascending',
          pageNo: 1,
          empNo: this.us.getCurrentUser().EmpNo,
          receivedDate: this.fullDate,
          fromDate: this.fullDate,
          toDate: this.fullDate,
          queue: 'Inbox',
          roleIds: [
            this.us.getCurrentUser().roles[0].id
          ],
          subject: '',
          type: '',
          status: ''
        };
        if (this.firstParam !== 'new') {
          search.type = this.firstParam;
        } else {
          search.status = 'new';
        }
        this.ws.searchInbox().subscribe(data => this.getInboxResult(data));
      } else if (this.secondParam === 'pending') {
        const search = {
          sort: 'Priority',
          order: 'Ascending',
          pageNo: 1,
          empNo: this.us.getCurrentUser().EmpNo,
          fromDate: '',
          toDate: this.fullDate,
          queue: 'Inbox',
          roleIds: [
            this.us.getCurrentUser().roles[0].id
          ],
          subject: '',
          type: this.firstParam
        };
        this.ws.searchInbox()
          .subscribe(data => this.getInboxResult(data));
      } else if (this.secondParam === 'week') {
        const date = new Date();
        const last = new Date(date.getTime() - (7 * 24 * 60 * 60 * 1000));
        const subDate = last.getDate() + '/' + (last.getMonth() + 1) + '/' + last.getFullYear();
        const search = {
          sort: 'Priority',
          order: 'Ascending',
          pageNo: 1,
          empNo: this.us.getCurrentUser().EmpNo,
          receivedDate: subDate + ';' + this.fullDate,
          fromDate: subDate,
          toDate: this.fullDate,
          queue: 'Inbox',
          roleIds: [
            this.us.getCurrentUser().roles[0].id
          ],
          subject: '',
          type: this.firstParam
        };
        this.ws.searchInbox()
          .subscribe(data => this.getInboxResult(data));
      }
    }
  }

  getInboxResult(data) {
    this.inboxList = [];
    this.inboxList = JSON.parse(data._body);
    this.pageSize = this.inboxList.pageSize;
    this.totalPages = (this.inboxList.pages * this.pageSize);
    this.curPage = this.inboxList.curPage;
    const valu = JSON.parse(data._body);
    const value = valu.activities;
    let count = 0;
    for (let index = 0; index < value.length; index++) {
      if (value[index].status === 'NEW') {
        count++;
      }
    }
    this.Unread = count;
    this.app.updateValue('Inbox', count);
    // this.menu.updateInboxBadge('Inbox', count);
    // for (let i = 0; i < this.menuitems.model.length; i++) {
    //   if (this.menuitems.model[i].label === 'Inbox') {
    //     this.menuitems.model[i].badge = count;
    //     }
    //   }
  }

  actionbarCheckbox(event) {
    this.checktoggle = !this.checktoggle;
  }
  columnCheckBoxvalue(value) {
    this.columnValue = value;
  }
  status(status) {
    this.statusValue = status;
  }
  getInboxList(event) {
    this.inboxList = event;
    this.refreshdata = true;
  }
  pagination(event) {
    this.page = event;
    this.ws.getInboxItems(this.us.getCurrentUser().EmpNo, this.page).subscribe(data => this.getInboxResult(data));
  }
  checked(event) {
    this.checkedbutt = event;
  }
  checkedreference(event) {
    this.checkedref = event;
  }
  canDeactivate() {

    // if the editName !== this.user.name
    if (this.createis === false) {
      return window.confirm('Discard changes?');
    }

    return true;
  }
}
