import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../service/user.service';
import { WorkService } from '../../../service/work.service';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sent',
  templateUrl: './sent.component.html',
  styleUrls: ['./sent.component.css']
})
export class SentComponent implements OnInit {
  public rows: any[] = [];
  public sendworkitem: any;
  public sentType: string;
  public sentMail: string;
  public errorMessage: any;
  public displayTogggle;
  page = 1;
  curPage = 1;
  pageSize = 25;
  totalPages = 1;
  public checkedbutt: any;

  constructor(
    private us: UserService, private ws: WorkService, private breadcrumbeServece: BreadcrumbService, public translate: TranslateService) {
      this.breadcrumbeServece.setItems([
        {label: this.translate.instant('Sent')}
    ]);
  }
  ngOnInit() {
    this.sentType = 'Sent';
    this.ws.getSentItems(this.us.getCurrentUser().EmpNo, this.page)
      .subscribe(data => this.getSentItems(data));
  }

  getSentItems(data) {
    this.sendworkitem = [];
    this.sendworkitem = JSON.parse(data._body);
     this.pageSize = this.sendworkitem.pageSize;
      this.totalPages = (this.sendworkitem.pages * this.pageSize);
      this.curPage = this.sendworkitem.curPage;
  }
  getSentList(event) {
    this.sendworkitem = event;
  }
  pagination(event) {
    this.page = event;
    this.ws.getSentItems(this.us.getCurrentUser().EmpNo, this.page).subscribe(data => this.getSentItems(data));
  }
  checked(event) {
    this.checkedbutt =  event;
   }
}
