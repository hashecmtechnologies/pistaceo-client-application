import { Component, OnInit } from '@angular/core';
import { BreadcrumbComponent } from '../layout/breadcrumb/breadcrumb.component';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-annotation',
  templateUrl: './annotation.component.html',
  styleUrls: ['./annotation.component.css']
})
export class AnnotationComponent implements OnInit {

  public docId;
  public currentPage;
  public documentType;
  public annotationType;

  constructor(private breadcrumbService: BreadcrumbService, public translate: TranslateService, private route: ActivatedRoute) {
    if (window.location.href.includes('draft')) {
      this.breadcrumbService.setItems([
        {label: this.translate.instant('Draft')},
        {label: this.translate.instant('Annotation')}
    ]);
    }else if (window.location.href.includes('inbox')) {
      this.breadcrumbService.setItems([
        {label: this.translate.instant('Inbox')},
        {label: this.translate.instant('Annotation')}
    ]);
    } else if (window.location.href.includes('sent')) {
      this.breadcrumbService.setItems([
        {label: this.translate.instant('Sent')},
        {label: this.translate.instant('Annotation')}
    ]);
    }else if (window.location.href.includes('archive')) {
      this.breadcrumbService.setItems([
        {label: this.translate.instant('Archive')},
        {label: this.translate.instant('Annotation')}
    ]);
    }else {
      this.breadcrumbService.setItems([
        {label: this.translate.instant('Document')},
        {label: this.translate.instant('Annotation')}
    ]);
    }
    this.route.queryParams.subscribe(p => {this.ngOnInit(); });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.docId = params['docId'];
      this.documentType = params['documentType'];
      this.currentPage = params['currentPage'];
      this.annotationType = params['annotationType'];
   });
}
}
