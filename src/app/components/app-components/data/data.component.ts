import { Component, OnInit } from '@angular/core';
import { Route, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../service/data.service';
import { DocumentService } from '../../../service/document.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { IntegrationService } from '../../../service/integration.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {
  public dataSetArray = [];
  public dataSetItems = [];
  public dataSchema: any;
  public reportName: any;
  public tempelateRender: FormGroup;
  public formFilter: any;
  public selectedReport;
  public formFilterproperties = [];
  public ddmmyy = false;
  public selectedLanguage: any;
  public isRtl = false;
  public isAdd = true;
  constructor(private route: ActivatedRoute, private dataService: DataService, private ds: DocumentService, private fb: FormBuilder,
    private ngxSmartModalService: NgxSmartModalService, private spinerService: Ng4LoadingSpinnerService, private integrationservice: IntegrationService,
    private tr: ToastrService) { }

  ngOnInit() {
    this.selectedLanguage = localStorage.getItem('Default Language');
    if (localStorage.getItem('Date Format') === 'DD/MM/YYYY') {
      this.ddmmyy = true;
    } else {
      this.ddmmyy = false;
    }
    if (localStorage.getItem('Default Language') === 'ar') {
      this.isRtl = true;
    } else {
      this.isRtl = false;
    }
    this.route.queryParams.subscribe(params => {
      this.dataSchema = {
        formId: params['formId'],
        id: params['id'],
        label: params['label'],
        name: params['name'],
        status: params['status'],
        table: params['table'],
        keyName: params['keyName']
      };
      this.dataService.getDataSet(this.dataSchema).subscribe(data => { this.getDataSetResponse(data); }, error => { });
      this.tempelateRender = this.fb.group({});
      this.isAdd = true;
      this.formFilter = [];
    });
  }
  getDataSetResponse(data) {
    const response = JSON.parse(data._body);
    this.dataSetArray = [];
    this.dataSetArray = response.columns;
    this.dataSetItems = [];
    this.dataSetItems = response.items;

  }
  addDataSetProps() {
    this.isAdd = true;
    this.ds.getJSONFromDocument(this.dataSchema.formId).subscribe(data => this.getFilterFormJSONTemp(data, 'items', 'Add'));

  }
  getFilterFormJSONTemp(data, items, isDelete) {
    this.formFilter = [];
    this.formFilter = JSON.parse(data._body);
    this.reportName = this.formFilter.name;
    if (this.isAdd) {
      this.getFilterFormJSON();
    } else {
      for (let a = 0; a < this.formFilter.properties.length; a++) {
        // for (let d = 0; d < items.length; d++) {
        // if (this.converToUppercase(this.formFilter.properties[a].name) === this.converToUppercase(items[d].name)) {
        this.formFilter.properties[a].value = items[a].value;
        // break;
        // }
        // }

      }
      if (isDelete === 'Delete') {
        this.ngxSmartModalService.getModal('deleteDataModel').open();
      } else {
        this.getFilterFormJSON();
      }
    }
  }
  deleteData() {
    const FDataEditor = {
      form: this.formFilter,
      action: 'DELETE',
      schema: this.dataSchema
    };
    this.dataService.saveData(FDataEditor).subscribe(data => { this.deletedData(data); }, error => { this.ngxSmartModalService.getModal('deleteDataModel').close(); });
  }
  deletedData(data) {
    this.tr.success('', 'Deleted successfully');
    this.ngxSmartModalService.getModal('deleteDataModel').close();
    this.ngOnInit();
  }
  editDataFormProps(items) {
    this.isAdd = false;
    this.ds.getJSONFromDocument(this.dataSchema.formId).subscribe(data => this.getFilterFormJSONTemp(data, items, 'Edit'));
  }
  deleteDataFormProps(items) {
    this.isAdd = false;
    this.ds.getJSONFromDocument(this.dataSchema.formId).subscribe(data => this.getFilterFormJSONTemp(data, items, 'Delete'));
  }
  getFilterFormJSON() {

    if (this.formFilter.properties.length > 0) {
      this.formFilterproperties = this.formFilter.properties;
      this.tempelateRender = this.fb.group({});
      for (let index = 0; index < this.formFilter.properties.length; index++) {
        if (this.formFilter.properties[index].req === 'TRUE') {
          const control: FormControl = new FormControl(null, Validators.required);
          this.tempelateRender.addControl(this.formFilter.properties[index].name, control);
        } else {
          const control: FormControl = new FormControl(null);
          this.tempelateRender.addControl(this.formFilter.properties[index].name, control);
        }
      }
      for (let index = 0; index < this.formFilter.properties.length; index++) {
        if (this.formFilter.properties[index].uitype.uitype === 'TEXT') {
          if (this.formFilter.properties[index].value.length !== 0) {
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
          } else {
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value);
          }
        } else if (this.formFilter.properties[index].uitype.uitype === 'LOOKUP') {
          if (this.formFilter.properties[index].value.length !== 0) {
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
          } else {
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].lookups[0].value);
          }
        } else if (this.formFilter.properties[index].uitype.uitype === 'DATE') {
          if (this.formFilter.properties[index].value.length !== 0) {
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
          } else {
            const today = new Date(); // needs to be resolved helplinks https://github.com/kekeh/mydatepicker/blob/master/sampleapp/sample-date-picker-access-modifier/sample-date-picker-access-modifier.html
            let dd = today.getDate();
            let mm = today.getMonth() + 1; // January is 0!
            const yyyy = today.getFullYear();
            if (dd < 10) {
              dd = 0 + dd;
            }
            if (mm < 10) {
              mm = 0 + mm;
            }
            let todays;
            if (this.ddmmyy) {
              todays = dd + '/' + mm + '/' + yyyy;
            } else {
              todays = mm + '/' + dd + '/' + yyyy;
            }
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(todays);
          }
        } else if (this.formFilter.properties[index].uitype.uitype === 'NUMBER') {
          if (this.formFilter.properties[index].value.length !== 0) {
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
          } else {
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value);
          }
        } else if (this.formFilter.properties[index].uitype.uitype === 'TEXTAREA') {
          if (this.formFilter.properties[index].value.length !== 0) {
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
          } else {
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value);
          }
        } else if (this.formFilter.properties[index].uitype.uitype === 'CHECKBOX') {
          if (this.formFilter.properties[index].value.length !== 0) {
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
          } else {
            this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value);
          }
        } else {

        }
      }
      for (let k = 0; k < this.formFilter.properties.length; k++) {
        if (this.formFilter.properties[k].uitype.uitype === 'STEXTTA' || this.formFilter.properties[k].uitype.uitype === 'MTEXTTA' || this.formFilter.properties[k].uitype.uitype === 'DBLOOKUP') {
          this.formFilter.properties[k].dbValue = [];
          this.formFilter.properties[k].dbDummyValue = [];
        }
        if (this.formFilter.properties[k].uitype.uitype === 'DBLOOKUP') {
          this.formFilter.properties[k].lookupOptions = [];
          this.integrationservice.getDBLookup(this.formFilter.properties[k].dblookup).subscribe(res => this.assignDblookupvalue(res, k));
        }
        if (this.formFilter.properties[k].uitype.uitype === 'LOOKUP') {
          this.formFilter.properties[k].lookupOptions = [];
          if (this.formFilter.properties[k].value.length === 0) {
            this.formFilter.properties[k].lookupOptions = [];
            const select = {
              name: this.formFilter.properties[k].lookups[0].name,
              value: this.formFilter.properties[k].lookups[0].value
            };
            this.formFilter.properties[k].value.push(select);
          }
        }

        if (this.formFilter.properties[k].uitype.uitype === 'MTEXTTA') {
          this.formFilter.properties[k].dbDummyValue = [];
          this.formFilter.properties[k].dbValue = [];
          if (this.formFilter.properties[k].value.length === 0) {
            this.formFilter.properties[k].value = [];
          } else {
            for (let l = 0; l < this.formFilter.properties[k].value.length; l++) {
              const element = {
                'value': this.formFilter.properties[k].value[l].value,
                'name': this.formFilter.properties[k].value[l].name
              };
              this.formFilter.properties[k].dbDummyValue.push(element);
            }
          }

        } else if (this.formFilter.properties[k].uitype.uitype === 'STEXTTA') {
          this.formFilter.properties[k].dbDummyValue = [];
          this.formFilter.properties[k].dbValue = [];
          if (this.formFilter.properties[k].value.length === 0) {
            this.formFilter.properties[k].value = [];
          } else {
            for (let l = 0; l < this.formFilter.properties[k].value.length; l++) {
              const element = {
                'value': this.formFilter.properties[k].value[l].value,
                'name': this.formFilter.properties[k].value[l].name
              };
              this.formFilter.properties[k].dbDummyValue = element;
            }
          }

        } else if (this.formFilter.properties[k].uitype.uitype === 'DBLOOKUP') {
          this.formFilter.properties[k].dbDummyValue = [];
          this.formFilter.properties[k].dbValue = [];
          if (this.formFilter.properties[k].value === '') {
            this.formFilter.properties[k].value = [];
          } else {
            for (let l = 0; l < this.formFilter.properties[k].value.length; l++) {
              const element = {
                'id': this.formFilter.properties[k].value[l].value,
                'itemName': this.formFilter.properties[k].value[l].name
              };
              this.formFilter.properties[k].dbDummyValue.push(element);
            }
          }
        } else if (this.formFilter.properties[k].uitype.uitype === 'LOOKUP') {
          this.formFilter.properties[k].lookupOptions = [];
          for (const options of this.formFilter.properties[k].lookups) {
            const lookup = {
              label: options.name,
              value: options.value
            };
            this.formFilter.properties[k].lookupOptions.push(lookup);
          }
        } else if (this.formFilter.properties[k].uitype.uitype === 'CHECKBOX') {
          this.formFilter.properties[k].dbDummyValue = [];
          this.formFilter.properties[k].dbValue = '';
          if (this.formFilter.properties[k].value.length === 0) {
            this.formFilter.properties[k].dbValue = false;
          } else {
            if (this.formFilter.properties[k].value[0].value === 'TRUE') {
              this.formFilter.properties[k].dbValue = true;
            } else {
              this.formFilter.properties[k].dbValue = false;
            }
            // for (let l = 0; l < this.formFilter.properties[k].value.length; l++) {
            //     const element = {
            //         'value': this.formFilter.properties[k].value[l].value,
            //         'name': this.formFilter.properties[k].value[l].name
            //     };
            //     this.formFilter.properties[k].dbDummyValue.push(element);
            // }
          }
        } else if (this.formFilter.properties[k].uitype.uitype === 'TEXTAREA') {
          this.formFilter.properties[k].dbDummyValue = [];
          this.formFilter.properties[k].dbValue = '';
          if (this.formFilter.properties[k].value.length === 0) {
            this.formFilter.properties[k].dbValue = '';
          } else {
            let textstring = '';
            textstring = this.formFilter.properties[k].value[0].value;
            this.formFilter.properties[k].dbValue = textstring.replace(/<\/p><p>/gm, '</p><br><p>');
          }
        }
      }
      this.spinerService.hide();
      this.ngxSmartModalService.getModal('reportFilterModel').open();
    } else {
      // this.selectedReport = {
      //   'name': this.selectedReport.name,
      //   'id': this.selectedReport.id,
      //   'defid': this.selectedReport.defid,
      //   'filterForm': {
      //     'name': this.formFilter.name,
      //     'properties': this.formFilter.properties,
      //     'datatable': this.formFilter.datatable
      //   }
      // };
      this.ngxSmartModalService.getModal('reportFilterModel').close();
      // this.rs.executeHTMLReport(this.selectedReport).subscribe(data => {
      //     this.executedHTMLResports(data);
      // }, error => { this.spinerService.hide(); this.tr.error('', 'Couldn\'t get reports'); });
    }
  }
  formFilterSubmit(event) {
    this.spinerService.show();
    for (const inputField of [].slice.call(event.target)) {
      for (let index = 0; index < this.formFilter.properties.length; index++) {
        if (inputField.id === this.formFilter.properties[index].name) {
          this.formFilter.properties[index].value = [{
            'value': inputField.value
          }];
        }
        delete this.formFilter.properties[index].dbValue;
        delete this.formFilter.properties[index].dbDummyValue;
        delete this.formFilter.properties[index].lookupOptions;
      }
    }
    for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
      for (let index = 0; index < this.formFilter.properties.length; index++) {
        if (inputField.id === this.formFilter.properties[index].name) {
          this.formFilter.properties[index].value = [{
            'value': inputField.children['0'].children['0'].value
          }];
        }
      }
    }
    for (const inputField of [].slice.call(event.target.getElementsByTagName('p-editor'))) {
      for (let index = 0; index < this.formFilter.properties.length; index++) {
        if (inputField.id === this.formFilter.properties[index].name) {
          this.formFilter.properties[index].value = [{
            'value': inputField.children[0].children[1].children[0].innerHTML
          }];
        }
      }
    }
    // for (const inputField of [].slice.call(event.target.getElementsByTagName('p-checkbox'))) {
    //   for (let index = 0; index < this.formFilter.properties.length; index++) {
    //     if (inputField.id === this.formFilter.properties[index].name) {
    //       this.formFilter.properties[index].value = [{
    //         'value': inputField.children[0].children[1].children[0].innerHTML
    //                  }];
    //                }
    //   }
    // }
    const FDataEditor = {
      form: this.formFilter,
      action: 'ADD',
      schema: this.dataSchema
    };
    if (!this.isAdd) {
      FDataEditor.action = 'UPDATE';
    }
    this.dataService.saveData(FDataEditor).subscribe(data => { this.savedData(data); }, error => { this.errorSavedData(error); });
    // this.rs.executeHTMLReport(this.selectedReport).subscribe(data => {
    //   this.executedHTMLResports(data);
    // }, error => { this.spinerService.hide(); this.tr.error('', 'Couldn\'t get reports'); });
  }
  savedData(data) {
    if (!this.isAdd) {
      this.tr.success('', 'Updated successfully');
    } else {
      this.tr.success('', 'Saved successfully');
    }
    this.ngxSmartModalService.getModal('reportFilterModel').close();
    this.spinerService.hide();
    this.ngOnInit();
  }
  errorSavedData(error) {
    this.spinerService.hide();
    this.ngxSmartModalService.getModal('reportFilterModel').close();
  }
  dbLookupDropdown(res, k) {
    this.formFilter.properties[k].value = [];
    this.formFilter.properties[k].value.push({
      name: res.label,
      value: res.value
    });
  }
  assignDblookupvalue(res, k) {
    const value = JSON.parse(res._body);
    for (let val = 0; val < value.length; val++) {
      const elementval = {
        label: value[val].label,
        value: value[val].value
      };
      this.formFilter.properties[k].lookupOptions.push(elementval);
    }
    if (this.formFilter.properties[k].value.length > 0) {
      this.tempelateRender.controls[this.formFilter.properties[k].name].patchValue(this.formFilter.properties[k].value[0].value);
    }
  }
  converToUppercase(string) {
    if (string !== undefined) {
      return string.toUpperCase();
    }
  }
}
