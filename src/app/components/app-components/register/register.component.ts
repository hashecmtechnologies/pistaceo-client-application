import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { ActivatedRoute, Router } from '../../../../../node_modules/@angular/router';
import { SchemaService } from '../../../service/schema.service';
import { WorkService } from '../../../service/work.service';
import { TranslateService } from '../../../../../node_modules/@ngx-translate/core';
import { DocumentService } from '../../../service/document.service';
import { saveAs } from 'file-saver';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
public id:  any;
public name: any;
public registerres = [];
public showDropDown = [];
public optionselect:  any;
public registeredData = [];
public attachmentres = [];
  constructor(private breadcrumbService: BreadcrumbService, private route: ActivatedRoute, public ngxSmartModalService: NgxSmartModalService,
    private ss: SchemaService, private ws: WorkService, private translate: TranslateService, private router: Router, private ds: DocumentService) {
    const browserLang: string = translate.getBrowserLang();
    translate.use('en');
translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
  }

  ngOnInit() {
    this.route.queryParams.subscribe(data => {
this.id = data['id'];
this.name = data['name'];
this.breadcrumbService.setItems([
  {label: this.translate.instant('Register')},
  {label: this.translate.instant(this.name)}
]);
this.ss.getWorkCategories().subscribe(res => {this.getRegisterResult(res);
  this.registerSelectfunction(event); });

    });
  }
  getRegisterResult(data) {
    this.registerres = JSON.parse(data._body);
    this.optionselect = this.registerres[0].id;
    this.showDropDown = [];
    for (const reg of this.registerres) {
    const ele = {
      label: reg.name,
      value: reg.id
    };
    this.showDropDown.push(ele);
  }
  }
  registerSelectfunction(event) {
    this.ws.getWorkRegister(this.id, this.optionselect).subscribe(data => this.showregisterdata(data));
  }
  showregisterdata(data) {
this.registeredData = [];
this.registeredData = JSON.parse(data._body);

  }
  history(data) {
    this.ws.getWorkActivity(data.id).subscribe(res => {this.goToActivity(res); });

  }
  goToActivity(res) {
    let value;
    value = JSON.parse(res._body);
    this.router.navigate(['/register/activity'], {queryParams: {'readworkitem': 'readOnly', 'workId': value}});
  }
  download(primeDoc) {
    this.ds.downloadDocument(primeDoc).subscribe(data => this.downloadCurrentDocument(data) );
  }
  downloadCurrentDocument(data) {
    let filename = '';
    const disposition = data.headers.get('Content-Disposition');
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
      const matches = filenameRegex.exec(disposition);
      if (matches != null && matches[1]) {
        filename = matches[1].replace(/['"]/g, '');
      }
      saveAs(data._body, filename);
  }
  attachment(work) {
    this.ws.getWorkAttachments(work.id).subscribe(data => this.showattachments(data) );
  }
  showattachments(data) {
  this.attachmentres = JSON.parse(data._body);
  this.ngxSmartModalService.getModal('attachmentsModel').open();
  }
  refreshEvent() {
    this.ws.getWorkRegister(this.id, this.optionselect).subscribe(data => this.showregisterdata(data));
  }
}
